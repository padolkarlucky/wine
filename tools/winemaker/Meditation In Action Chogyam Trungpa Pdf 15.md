## Meditation In Action Chogyam Trungpa Pdf 15

 
  
 
**DOWNLOAD ✫✫✫ [https://vittuv.com/2tylxh](https://vittuv.com/2tylxh)**

 
 
 
 
 
# Meditation in Action: A Guide to the Teachings of Chogyam Trungpa
 
Meditation is not just a practice of sitting quietly and focusing on your breath. It is also a way of living with awareness, compassion, and wisdom in every moment. This is what Chogyam Trungpa, a renowned Tibetan Buddhist master, taught in his book *Meditation in Action*.
 
In this article, we will explore the main themes and insights of this classic work, which was first published in 1969 and has inspired generations of meditators. We will also learn how to apply the principles of meditation in action to our daily lives, whether we are at work, at home, or in any situation.
 
## The Life and Example of Buddha
 
The book begins with a brief biography of the Buddha, who was born as a prince named Siddhartha Gautama in ancient India. He had everything he wanted, but he was not satisfied. He saw the suffering of people around him and realized that he too was subject to aging, sickness, and death. He decided to renounce his worldly life and seek the truth for himself.
 
He tried many different methods of meditation and asceticism, but none of them gave him the answer he was looking for. Finally, he sat under a bodhi tree and resolved not to move until he attained enlightenment. He faced many temptations and distractions from his own mind and from external forces, but he did not give up. He realized the true nature of reality and became the Buddha, which means "the awakened one".
 
He then spent the rest of his life teaching others how to achieve the same state of awakening. He did not claim to be a god or a prophet, but a human being who had discovered the truth for himself. He encouraged his followers to do the same, rather than blindly accepting his words. He taught them the four noble truths: the truth of suffering, the truth of the cause of suffering, the truth of the end of suffering, and the truth of the path that leads to the end of suffering.
 
He also taught them the eightfold path: right view, right intention, right speech, right action, right livelihood, right effort, right mindfulness, and right concentration. These are the guidelines for living ethically and wisely, based on understanding reality as it is.
 
## The Manure of Experience and Transmission
 
Chogyam Trungpa explains that meditation is not about escaping from reality or creating a false sense of peace. It is about facing reality as it is, with all its challenges and opportunities. He compares our experience to manure, which can be used as fertilizer to grow flowers or vegetables. Similarly, we can use our experience as a source of learning and growth.
 
He also emphasizes that meditation is not something that can be taught by words alone. It requires transmission from a teacher who has realized the truth for himself or herself. The teacher can only point out the way, but the student has to walk it by himself or herself. The teacher can only give hints and instructions, but the student has to discover the meaning for himself or herself.
 
Therefore, meditation is not a fixed technique or a dogma. It is a personal journey of exploration and discovery. It is a way of relating to ourselves and others with openness and curiosity.
 
## Generosity
 
The first activity associated with meditation in action is generosity. This does not mean only giving material things to others. It also means giving our time, attention, energy, love, and wisdom. It means being willing to share ourselves with others without expecting anything in return.
 
Generosity is based on realizing that we are not separate from others. We are all interconnected and interdependent. We all depend on each other for our well-being and happiness. Therefore, by helping others, we are also helping ourselves.
 
Generosity also helps us overcome our attachment to ourselves and our possessions. We realize that we do not own anything permanently. Everything is impermanent and changing. We cannot hold on to anything forever. Therefore, we can let go of our greed and fear and enjoy what we have without clinging to it.
 
## Patience
 
The second activity associated with meditation in action is patience. This means being able to endure difficulties and hardships
 842aa4382a
 
 
