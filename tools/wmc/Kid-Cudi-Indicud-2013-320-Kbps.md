## Kid Cudi - Indicud (2013 - 320 Kbps)

 
  
 
**LINK ☆☆☆☆☆ [https://www.google.com/url?q=https%3A%2F%2Fshurll.com%2F2tCxHo&sa=D&sntz=1&usg=AOvVaw0DU4eaWchyGrbHJzRN7w1e](https://www.google.com/url?q=https%3A%2F%2Fshurll.com%2F2tCxHo&sa=D&sntz=1&usg=AOvVaw0DU4eaWchyGrbHJzRN7w1e)**

 
 
 
 
 
# Kid Cudi's Indicud: A Bold Experiment in Hip Hop
 
Kid Cudi is one of the most innovative and versatile artists in the hip hop scene. His third studio album, *Indicud*, released on April 16, 2013, is a testament to his creative vision and musical prowess. The album is not a part of his acclaimed *Man on the Moon* series, but rather a standalone project that showcases Cudi's skills as a rapper, singer, songwriter and producer.
 
*Indicud* is a play-on-words on the cannabis strain indica and Cudi's own name. Cudi described the album as his version of Dr. Dre's *The Chronic 2001*, some songs he produced, others he featured and/or played songwriter. The album features guest appearances from King Chip, Kendrick Lamar, Too Short, RZA, ASAP Rocky, Father John Misty, Michael Bolton and Haim. The album also includes four singles: "Just What I Am", "King Wizard", "Immortal" and "Girls".
 
The album received mixed reviews from critics, who praised Cudi's ambition and experimentation, but also criticized his self-indulgence and lack of cohesion. The album debuted at number two on the US Billboard 200 chart, selling 139,000 copies in its first week. It also reached number 32 on the UK Albums Chart and number two on the UK R&B Chart.
 
*Indicud* is a bold experiment in hip hop that showcases Kid Cudi's artistic evolution and diversity. It is an album that challenges the norms and conventions of the genre, while also delivering catchy hooks and memorable verses. Whether you are a fan of Cudi's previous work or not, *Indicud* is an album worth listening to.
  
One of the most notable aspects of *Indicud* is Cudi's production. Cudi handled the production of the entire album by himself, with the exception of one track, "Red Eye", which was co-produced by Hit-Boy. Cudi's production style is eclectic and experimental, incorporating elements of rock, electronic, psychedelic and soul music. He also uses a variety of instruments and sounds, such as guitars, keyboards, drums, synths, samples and vocal effects. Cudi's production creates a rich and diverse sonic landscape that complements his vocals and lyrics.
 
Cudi's vocals and lyrics are also impressive on *Indicud*. Cudi switches between rapping and singing throughout the album, showcasing his versatility and range. He also explores different themes and topics, such as his personal struggles, his aspirations, his love life, his spirituality and his opinions on the music industry. Cudi's vocals and lyrics are honest and expressive, revealing his emotions and thoughts in a raw and authentic way. Cudi also collaborates with various artists on the album, who add their own flavor and style to the songs. Some of the standout features include Kendrick Lamar's verse on "Solo Dolo Part II", RZA's rap on "Beez", Haim's vocals on "Red Eye" and Michael Bolton's chorus on "Afterwards (Bring Yo Friends)".
 
*Indicud* is not a perfect album. It has some flaws and weaknesses that may detract from its overall quality. Some of the songs are too long or repetitive, some of the transitions are abrupt or awkward, some of the lyrics are corny or clichÃ©, and some of the experiments are unsuccessful or unnecessary. The album also lacks a clear direction or concept, making it seem unfocused or disjointed at times. However, these flaws do not overshadow the album's strengths and achievements. *Indicud* is a daring and ambitious album that showcases Kid Cudi's talent and creativity. It is an album that pushes the boundaries and expectations of hip hop, while also staying true to Cudi's artistic vision and identity.
 842aa4382a
 
 
