## Buku Ulumul Hadits Pdf Download

 
  
 
**Buku Ulumul Hadits Pdf Download ->>> [https://lodystiri.blogspot.com/?file=2tAV5m](https://lodystiri.blogspot.com/?file=2tAV5m)**

 
 
 
 
 
# Buku Ulumul Hadits: Sebuah Pengantar untuk Memahami Hadis Nabi
 
Hadis adalah sumber kedua dalam Islam setelah Al-Qur'an yang berisi perkataan, perbuatan, dan persetujuan Nabi Muhammad SAW. Hadis memiliki peran penting dalam menjelaskan dan menguatkan makna ayat-ayat Al-Qur'an, serta memberikan petunjuk praktis bagi umat Islam dalam berbagai aspek kehidupan. Namun, untuk dapat memahami hadis dengan baik dan benar, diperlukan ilmu yang disebut ulumul hadis.
 
Ulumul hadis adalah ilmu yang membahas tentang segala hal yang berkaitan dengan hadis, seperti definisi, sejarah, klasifikasi, kriteria, metodologi, dan lain-lain. Ulumul hadis membantu kita untuk mengetahui keaslian, kualitas, dan makna hadis secara komprehensif dan kritis. Dengan ulumul hadis, kita dapat menghindari kesalahan dalam memahami dan menerapkan hadis.
 
Salah satu buku yang dapat dijadikan referensi untuk mempelajari ulumul hadis adalah buku Ulumul Hadits karya Abdul Majid Khon. Buku ini merupakan buku teks yang digunakan di beberapa perguruan tinggi Islam di Indonesia. Buku ini memaparkan secara sistematis dan lengkap tentang berbagai topik dalam ulumul hadis, seperti:
 
- Kedudukan hadis dan ingkar sunnah
- Sejarah penghimpunan dan pembinaan hadis
- Ilmu hadis dan sejarah perkembangannya
- Istilah-istilah dalam ilmu hadis
- TakhrÃ®j hadis
- Hadis ditinjau dari kuantitas perawi
- Hadis ditinjau dari kualitas sanad dan matan
- Hadis dha'if dan sebab-sebab ke-dha'ifan
- Hadis mawdhÃ»' dan permasalahannya
- Macam-macam hadis dari berbagai sisi
- Biografi singkat beberapa perawi hadis

Buku ini juga dilengkapi dengan daftar pustaka yang mencakup berbagai sumber primer dan sekunder dalam bidang ulumul hadis. Buku ini sangat bermanfaat bagi para mahasiswa, dosen, peneliti, dan siapa saja yang ingin memperdalam ilmu hadis.
 
Jika Anda tertarik untuk membaca buku ini, Anda dapat mendownloadnya dalam format PDF secara gratis dari beberapa situs web berikut:

1. [âUlumul Hadits - UIN Sunan Gunung Djati Bandung](http://digilib.uinsgd.ac.id/29079/1/Ulumul%20Hadits.pdf)
2. [Ulumul Hadis - Abdul Majid Khon - Google Books](https://books.google.com/books/about/Ulumul_Hadis.html?id=tK_xDwAAQBAJ)
3. [Ulumul Hadits Lengkap - Abdul Majid Khon PDF | PDF - Scribd](https://www.scribd.com/document/360203690/Ulumul-Hadits-Lengkap-Abdul-Majid-Khon-pdf)

Semoga buku ini dapat menambah wawasan dan pengetahuan Anda tentang ulumul hadis. Selamat membaca!
  
## Ilmu Hadits Riwayah dan Dirayah
 
Ilmu hadits atau ulumul hadits dibagi menjadi dua, yaitu hadits riwayah dan hadits dirayah. Ilmu hadits riwayah berfokus pada pembahasan mengenai periwayatan suatu hadis. Dengan kata lain, semua proses penukilan atau kutipan yang bersumber dari dari Nabi SAW. merupakan ilmu hadits riwayah. Ilmu ini mencakup aspek-aspek seperti sanad, matan, perawi, dan kitab-kitab hadis[^2^] [^4^].
 
Ilmu hadits dirayah berfokus pada pembahasan mengenai kualitas dan makna suatu hadis. Dengan kata lain, semua proses penilaian dan penafsiran yang berkaitan dengan hadis Nabi SAW. merupakan ilmu hadits dirayah. Ilmu ini mencakup aspek-aspek seperti keshahihan, kelemahan, keanehan, nasikh-mansukh, asbab wurud, dan musthalah[^1^] [^3^].
 
Ilmu hadits riwayah dan dirayah saling berkaitan dan melengkapi satu sama lain. Keduanya diperlukan untuk dapat memahami hadis secara utuh dan mendalam. Tanpa ilmu hadits riwayah, kita tidak dapat mengetahui sumber dan keaslian suatu hadis. Tanpa ilmu hadits dirayah, kita tidak dapat mengetahui kualitas dan makna suatu hadis.
  
## Cabang-Cabang Ulumul Hadits
 
Ulumul hadits memiliki banyak cabang atau sub-bidang yang membahas berbagai topik dan masalah yang berkaitan dengan hadis. Beberapa cabang ulumul hadits yang penting dan populer adalah sebagai berikut:

- Ilmu Rijal al-Hadits: ilmu yang membahas tentang keadaan para perawi hadis, seperti nama, nasab, tempat tinggal, tarikh wafat, kedudukan, kredibilitas, dan lain-lain[^3^].
- Ilmu al-Jarh wa al-Ta'dil: ilmu yang membahas tentang kritik dan pujian terhadap para perawi hadis dari segi keadilan dan kedhabitannya[^3^].
- Ilmu Fannil Mubhamat: ilmu yang membahas tentang penjelasan nama-nama samaran atau julukan yang digunakan oleh para perawi hadis[^3^].
- Ilmu Mukhtalif al-Hadits: ilmu yang membahas tentang perbedaan-perbedaan lafazh atau redaksi antara satu riwayat hadis dengan riwayat lainnya[^3^].
- Ilmu 'Ilal al-Hadits: ilmu yang membahas tentang cacat-cacat atau kelemahan-kelemahan yang terdapat dalam sanad atau matan suatu hadis[^3^].
- Ilmu Gharib al-Hadits: ilmu yang membahas tentang kata-kata atau istilah-istilah yang jarang atau sulit dipahami dalam matan suatu hadis[^3^].
- Ilmu Nasikh wa Mansukh al-Hadits: ilmu yang membahas tentang abrogasi atau penghapusan hukum suatu hadis oleh hadis lain yang lebih kuat atau lebih baru[^3^].
- Ilmu Asbab Wurud al-Hadits: ilmu yang membahas tentang sebab-sebab munculnya atau turunnya suatu hadis, seperti konteks sejarah, sosial, budaya, atau situasi tertentu[^3^].
- Ilmu Musthalah al-Hadits: ilmu yang membahas tentang istilah-istilah teknis yang digunakan dalam ulumul hadits, seperti shahih, hasan, dha'if, 842aa4382a




