## Descargar La Audacia De La Esperanza Pdf To Jpg

 
  
 
**## Links to download files:
[Link 1](https://tiurll.com/2tEGsn)

[Link 2](https://imgfil.com/2tEGsn)

[Link 3](https://blltly.com/2tEGsn)

**

 
 
 
 
 
1. Vive Cristo, esperanza nuestra, y Él es la más hermosa juventud de este mundo. Todo lo que Él toca se vuelve joven, se hace nuevo, se llena de vida. Entonces, las primeras palabras que quiero dirigir a cada uno de los jóvenes cristianos son: ¡Él vive y te quiere vivo!
 
2. Él está en ti, Él está contigo y nunca se va. Por más que te alejes, allí está el Resucitado, llamándote y esperándote para volver a empezar. Cuando te sientas avejentado por la tristeza, los rencores, los miedos, las dudas o los fracasos, Él estará allí para devolverte la fuerza y la esperanza.
 
10. Salomón, cuando tuvo que suceder a su padre, se sintió perdido y dijo a Dios: «Soy un joven muchacho y no sé por dónde empezar y terminar» (*1 R* 3,7). Sin embargo, la audacia de la juventud lo movió a pedir a Dios la sabiduría y se entregó a su misión. Algo semejante le ocurrió al profeta Jeremías, llamado a despertar a su pueblo siendo muy joven. En su temor dijo: «¡Ay Señor! Mira que no sé hablar, porque soy demasiado joven» (*Jr* 1,6). Pero el Señor le pidió que no dijera eso (cf. *Jr* 1,7), y agregó: «No temas delante de ellos, porque yo estoy contigo para librarte» (*Jr* 1,8). La entrega del profeta Jeremías a su misión muestra lo que es posible si se unen la frescura de la juventud y la fuerza de Dios.
 
11. Una muchachita judía, que estaba al servicio del militar extranjero Naamán, intervino con fe para ayudarlo a curarse de su enfermedad (cf. *2 R* 5,2-6). La joven Rut fue un ejemplo de generosidad al quedarse con su suegra caída en desgracia (cf. *Rt*1,1-18), y también mostró su audacia para salir adelante en la vida (cf. *Rt* 4,1-17).
 
15. La Palabra de Dios dice que a los jóvenes hay que tratarlos «como a hermanos» (*1 Tm* 5,1), y recomienda a los padres: «No exasperen a sus hijos, para que no se desanimen» (*Col* 3,21). Un joven no puede estar desanimado, lo suyo es soñar cosas grandes, buscar horizontes amplios, atreverse a más, querer comerse el mundo, ser capaz de aceptar propuestas desafiantes y desear aportar lo mejor de sí para construir algo mejor. Por eso insisto a los jóvenes que no se dejen robar la esperanza, y a cada uno le repito: «que nadie menosprecie tu juventud» (*1 Tm* 4,12).
 
20. Si has perdido el vigor interior, los sueños, el entusiasmo, la esperanza y la generosidad, ante ti se presenta Jesús como se presentó ante el hijo muerto de la viuda, y con toda su potencia de Resucitado el Señor te exhorta: «Joven, a ti te digo, ¡levántate!» (*Lc* 7,14).
 
33. El Señor nos llama a encender estrellas en la noche de otros jóvenes, nos invita a mirar los verdaderos astros, esos signos tan variados que Él nos da para que no nos quedemos quietos, sino que imitemos al sembrador que miraba las estrellas para poder arar el campo. Dios nos enciende estrellas para que sigamos caminando: «Las estrellas brillan alegres en sus puestos de guardia, Él las llama y le responden» (*Ba* 3,34-35). Pero Cristo mismo es para nosotros la gran luz de esperanza y de guía en nuestra noche, porque Él es «la estrella radiante de la mañana» (*Ap* 22,16).
 
48. Aquella muchacha hoy es la Madre que vela por los hijos, estos hijos que caminamos por la vida muchas veces cansados, necesitados, pero queriendo que la luz de la esperanza no se apague. Eso es lo que queremos: que la luz de la esperanza no se apague. Nuestra Madre mira a este pueblo peregrino, pueblo de jóvenes querido por ella, que la busca haciendo silencio en el corazón aunque en el camino haya mucho ruido, conversaciones y distracciones. Pero ante los ojos de la Madre sólo cabe el silencio esperanzado. Y así María ilumina de nuevo nuestra juventud.
 
107. No dejes que te roben la esperanza y la alegría, que te narcoticen para utilizarte como esclavo de sus intereses. Atrévete a ser más, porque tu ser importa más que cualquier cosa. No te sirve tener o aparecer. Puedes llegar a ser lo que Dios, tu Creador, sabe que eres, si reconoces que estás llamado a mucho. Invoca al Espíritu Santo y camina con confianza hacia la gran meta: la santidad. Así no serás una fotocopia. Serás plenamente tú mismo.
 
109. Si eres joven en edad, pero te sientes débil, cansado o desilusionado, pídele a Jesús que te renueve. Con Él no falta la esperanza. Lo mismo puedes hacer si te sientes sumergido en los vicios, las malas costumbres, el egoísmo o la comodidad enfermiza. Jesús, lleno de vida, quiere ayudarte para que ser joven valga la pena. Así no privarás al mundo de ese aporte que sólo tú puedes hacerle, siendo único e irrepetible como eres.
 
135. Dios es el autor de la juventud y Él obra en cada joven. La juventud es un tiempo bendito para el joven y una bendición para la Iglesia y el mundo. Es una alegría, un canto de esperanza y una bienaventuranza. Apreciar la juventud implica ver este tiempo de la vida como un momento valioso y no como una etapa de paso donde la gente joven se siente empujada hacia la edad adulta.
 
139. Tiempo atrás un amigo me preguntó qué veo yo cuando pienso en un joven. Mi respuesta fue que «veo un chico o una chica que busca su propio camino, que quiere volar con los pies, que se asoma al mundo y mira el horizonte con ojos llenos de esperanza, llenos de futuro y también de ilusiones. El joven camina con dos pies como los adultos, pero a diferencia de los adultos, que los tienen paralelos, pone uno delante del otro, dispuesto a irse, a partir. Siempre mirando hacia adelante. Hablar de jóvenes significa hablar de promesas, y significa hablar de alegría. Los jóvenes tienen tanta fuerza, son capaces de mirar con tanta esperanza. Un joven es una promesa de vida que lleva incorporado un cierto grado de tenacidad; tiene la suficiente locura para poderse autoengañar y la suficiente capacidad para poder curarse de la desilusión que pueda derivar de ello»[75].
 
142. Hay que perseverar en el camino de los sueños. Para ello hay que estar atentos a una tentación que suele jugarnos una mala pasada: la ansiedad. Puede ser una gran enemiga cuando nos lleva a bajar los brazos porque descubrimos que los resultados no son instantáneos. Los sueños más bellos se conquistan con esperanza, paciencia y empeño, renunciando a las prisas. Al mismo tiempo, no hay que detenerse por inseguridad, no hay que tener miedo de apostar y de cometer errores. Sí hay que tener miedo a vivir paralizados, como muertos en vida, convertidos en seres que no viven porque no quieren arriesgar, porque no perseveran en sus empeños o porque tienen temor a equivocarse. Aún si te equivocas siempre podrás levantar la cabeza y volver a empezar, porque nadie tiene derecho a robarte la esperanza.
 
153. Es tan importante la amistad que Jesús mismo se presenta como amigo: «Ya no los llamo siervos, los llamo amigos» (*Jn* 15,15). Por la gracia que Él nos regala, somos elevados de tal manera que somos realmente amigos suyos. Con el mismo amor que Él derrama en nosotros podemos amarlo, llevando su amor a los demás, con la esperanza de que también ellos encontrarán su puesto en la comunidad de amistad fundada por Jesucristo[80]. Y si bien Él ya está plenamente feliz resucitado, es posible ser generosos con Él, ayudándole a construir su Reino en este mundo, siendo sus instrumentos para llevar su mensaje y su luz y, sobre todo, su amor a los demás (cf. *Jn* 15,16). Los discípulos escucharon el llamado de Jesús a la amistad con Él. Fue una invitación que no los forzó, sino que se propuso delicadamente a su libertad: «Vengan y vean» les dijo, y «ellos fueron, vieron donde vivía y se quedaron con Él aquel día» (*Jn* 1,39). Después de ese encuentro, íntimo e inesperado, dejaron todo y se fueron con Él.
 
173. Como en el milagro de Jesús, los panes y los peces de los jóvenes pueden multiplicarse (cf. *Jn* 6,4-13). Igual que en la parábola, las pequeñas semillas de los jóvenes se convierten en árbol y cosecha (cf. *Mt* 13,23.31-32). Todo ello desde la fuente viva de la Eucaristía, en la cual nuestro pan y nuestro vino se transfiguran para darnos Vida eterna. Se les pide a los jóvenes una tarea inmensa y difícil. Con la fe en el Resucitado, podrán enfrentarla con creatividad y esperanza, y ubicándose siempre en el lugar del servicio, como los sirvientes de aquella boda, sorprendidos colaboradores del primer signo de Jesús, que sólo siguieron la consigna de su Madre: «Hagan lo que Él les diga» (*Jn* 2,5). Misericordia, creatividad y esperanza hacen crecer la vida.
 
177. «¿Adónde nos envía Jesús? No hay fronteras, no hay límites: nos envía a todos. El Evangelio no es para algunos sino para todos. No es sólo para los que nos parecen más cercanos, más receptivos, más acogedores. Es para todos. No tengan miedo de ir y llevar a Cristo a cualquier ambiente, hasta las periferias existenciales, también a quien parece más lejano, más indiferente. El Señor busca a todos, quiere que todos sientan el calor de su misericordia y de su amor»[94]. Y nos invita a ir sin miedo con el anuncio misionero, allí donde nos encontremos y con quien estemos, en el barrio, en el estudio, en el deporte, en las salidas con los amigos, en el voluntariado o en el trabajo, siempre es bueno y oportuno compartir la alegría del Evangelio. Así es como el Señor se va acercando a todos. Y a ustedes, jóvenes, los quiere como sus instrumentos para derramar luz y esperanza, porque quiere contar con vuestra valentía, frescura y entusiasmo.
 
194. Es lindo encontrar entre lo que nuestros padres conservaron, algún recuerdo que nos permite imaginar lo que soñaron para nosotros nuestros abuelos y nuestras abuelas. Todo ser humano, aun antes de nacer, ha recibido de parte de sus abuelos como regalo, la bendición de un sueño lleno de amor y de esperanza: el de una vida mejor para él. Y si no lo tuvo de ninguno de sus abuelos, se