## Sharing Large Files With iDisk

 
 ![Sharing Large Files With iDisk](https://9to5mac.com/wp-content/uploads/sites/6/2013/08/screen-shot-2013-08-23-at-7-10-34-pm.png)
 
 
**## Downloadable file links:
[Link 1](https://imgfil.com/2tDn82)

[Link 2](https://blltly.com/2tDn82)

[Link 3](https://bltlly.com/2tDn82)

**

 
 
 
 
 
Tap on a folder and you will see a list of the files in the folder. Tap on a file supported by the iPhone such as a Word or Excel document, an iWork document, a picture, a video or a song, and the file will be downloaded from your iDisk to your iPhone so that you can view the file. You can quickly see a list of files that you have recently viewed by tapping on the Recents icon at the bottom. In the Settings, you can decide how much space to devote to iDisk files that you download (the default is 100MB) so that when you return to a file that you recently viewed, the file pops up instantly without having to download it again. In addition to viewing files on your own iDisk, you can tap on the Public Folders icon to view the public files of other MobileMe users.
 
Viewing files works great, and while there are other apps that currently allow you to do the same thing (such as Quickoffice) it is nice to have a free app from Apple. But what makes the iDisk app really powerful is the ability to share files on your iDisk, especially larger files that would be a hassle to attach to an e-mail. I'll walk you through an example to show you how it works.
 
Again, this is great for large files because you avoid dealing with the problems of big attachments such as slow e-mail. Once you are sharing a file, you will see an indication in the form of a green icon that the file is being shared. You can also tap on the Shared Files icon at the bottom of the screen to see all files that you are sharing. At any time, you can change the sharing status for any file by tapping the sharing icon, so you can make the file available for a longer time, or you can stop sharing the file completely.
 
Apple has also posted this video to show you more about the app, but I don't think you really need to read any further; if you use MobileMe, then you will want to get this free app. Not only does it give you an easy way to view your iDisk files, but it also gives you a great solution for sharing larger files, even when you are on the go and only have access to your iPhone but not your computer.
 
As we move into the OS 10.8 Mountain Lion era, any sort of direct replacement for iDisk (from Apple, at least) looks unlikely. But there are other options out there for sending and sharing large files and folders.
 
Dropbox integrates into the Mac Finder at least as smoothly as Apple's iDisk did.To use Dropbox in its most intuitive, iDisk-like way, you have to install an application and associated files on your Mac (OS 10.4 or later). Installers are also available for Windows, Linux and various mobile platforms. When that's done, you'll see a new menu-bar item and a 'Dropbox' folder inside your Mac's home folder. To share something (a file or folder), first copy it into this folder. Dropbox syncs it up, in the background, with the cloud storage, and the menu bar item gives you feedback about progress. Then open the Dropbox folder, right-click on the file or folder you want to share, and choose DropBox/Get Link. A browser window opens, from which you can copy the URL web address to send to someone in an email, for example. For uploaded folders, you can also choose Dropbox/Share This Folder. Again, a browser window opens, but pre-configured with a form to send out an email to multiple collaborators.
 
Again, the most tightly integrated Mac experience is achieved by installing the Google Drive application (good for OS 10.6 and later), though there's currently no equivalent to Dropbox's Finder contextual menu for quickly generating sharing links and invitations. Instead, you go to Google Drive's menu-bar item (yes, it has one of these as well) and choose 'Visit Google Drive on the web'. You can right-click your files and folders there to share them, and everything works in a very clear and straightforward way.
 
But if you find yourself regularly needing to send large files to different people, the problem becomes notably more complicated, thanks to the entirely reasonable tendency of email admins to limit the size of incoming attachments, often to about 5 MB, and to reject or simply drop messages containing Zip archive attachments, the vast majority of which are viruses.
 
In praise of Stuffit: My experience with Mac OS archive/zip is that key attributes of files are sometimes lost, or poorly named (or really long file named) files get mangled. Can anyone confirm that? Anyway I use .SITX when I need reliability wheb sending a group of files to a service provider.
 
Well, it wasn't just the hard sell either. Remember when OS X came out and we had to buy an upgrade in order to get a version that would handle UNIX file attributes? And they told us that the new format was expandable for the future? But it couldn't handle the executable permission bit? So applications compressed and uncompressed with the new version would be broken? So they "fixed" it by setting the executable bit on all files? Then required ANOTHER format change and paid upgrade in order to get the compatibility with OS X that we had been promised with the prior upgrade?
 
Several years ago, another graphic designer friend of mine suggested I try using YouSendIt (dot com) as a FREE means of uploading large files and for me, it works perfectly. I just upload a file or zipped folder to yousendit, put in my recipient's info and an email containing a download link is sent to them. Fast, easy, and free! It sure beats shelling out fifty bucks for an upload service that basically does the same thing.
 
Once you've finished creating your movie, you can store it on your hard drive, but that uses a lot of disk space and the only place you can then show it is on your computer. Since iMovies are usually large, from several gigabytes on up, you can't pass them around on a Zip disk or even on a CD. To **share** your movie with others, you need to export the movie in a format that can be shared. iMovie is able to export a movie in a variety of formats, depending on its final intended use.
 
Send a small movie to someone through email. iMovie will compress the movie to 10 frames per second (digital video is usually 30 frames per second) and 160 x 120 pixels with monaural sound (not stereo). Even a short and highly compressed movie makes a large file, so don't send long movies through email unless absolutely necessary.
 
As a method of sending large files or photos, FTP is less easy to understand, for the beginner, but allows for the sharing large file size information. The disadvantages are that you will need some webspace and an FTP client in order to upload information.
 
Dropbox provides you with 2GB of space for free. There are various incentives whereby you can increase the amount of free space and you can also pay for additional space in order to synchronise files across devices.
 
You can also share specific files with others. Provided that the file is in your Dropbox folder, secondary-click on the file to reveal the context-sensitive menu and select **Share Dropbox Link**. This will generate a long URL which will be saved to your clipboard which allows you to **Command V** paste the URL into an email, an iMessage or another app by which to share the link.
 
As a method of sending large files or photos, Dropbox is relatively easy to understand and allows for the sharing large file size information provided that it does not exceed the space that you have available in Dropbox. That said, it may still be confusing to use for the novice Mac user.
 
As a method of sending large files or photos, Google Drive is not particularly easy to understand, for the novice, though it does allow for the sharing large file size information. That said, it may still be confusing to use for the novice and experienced Mac user alike.
 
There are a number of web-based services, such as ShareFile, WeTransfer and YouSendIt, but these services often have limits on them that are not too dissimilar from email providers. A maximum transfer size of 20GB, or just 10GB, is not uncommon. There is even one service that has a limit of 2GB despite advertising itself as being a method to transfer large files.
 72ea393242
 
 
