## Game Art Institute ? Character Creation By Jason Martin

 
 ![Game Art Institute ? Character Creation By Jason Martin](https://i2.hdslb.com/bfs/archive/50547afae486042e77c43d3b416203621fd6cf7c.png)
 
 
**## Download files here:
[Link 1](https://urluso.com/2tDo7o)

[Link 2](https://urlgoal.com/2tDo7o)

[Link 3](https://urlca.com/2tDo7o)

**

 
 
 
 
 
Week 4: Using Maya I will demonstrate and discuss optimal topology flow for realistic facial deformations for in game and cinematic characters. Several different topology approaches will be discussed and the pros/cons behind each. Furthermore, students will be shown how to approach topology for clothing, with and without, wrinkle maps and to lessen texture distortion from deformation.
 
Week 6: I will go over and demonstrate several methods for painting the head, and then expand on my preferred technique using Mudbox, substance painter and photoshop.
Week 7: Finalize texture creation for heads, and continue on with costume and clothing elements.
Week 8: Review of final textures. Begin discussion of material setup within marmoset and the dreaded video game hair cards. (we will discuss hair card techniques, although the final look depends highly on what game engine and tech is being used.)
 
months working on your reels. This is where most of us got the majority of our education and had many sleepless nights. It was a great time and if it wasn't for VFS I wouldn't have landed at Blur right out of school.  
  
In your time at Blur Studio you seem to have worked on some really interesting and high-profile game titles. Can you name a few and which one is your favorite?  
  
It's hard to say to be honest. I've been fortunate enough to work on several iconic characters and each was such an awesome experience. It's a toss-up between Alpha Series, Big Daddy from Bioshock 2, The Joker from Arkham City, and Darth Vader from The Force Unleashed 2. In the end I would have to say Darth Vader wins. I grew up on Star Wars and I'm pretty sure any modeler would jump at the chance to model Darth Vader. It was pretty awesome I have to say!   
  
I've also worked on a few other less-iconic characters that were really fun to do. Chompy from the Jabberwocky cinematic comes to mind, as well as Grunt from Mass Effect. Creatures are a blast to do. They can be very interpretive and loose. I still love humanoids as well though as they are always challenging. If you can make a realistic face you can do a creature. Realism is the biggest challenge.  
  
Do you have any sources of inspiration or people you look up to?  
  
Many! So many it's not even funny! I find inspiration from all sorts of stuff. It's not restricted to the 3D genre. Walking into Blur I had the opportunity to work with some amazing character artists like Laurent Pierlot and Alessandro Baldasseroni. Both have been big inspirations to me and very helpful - they are very grounded, modest, and good passionate people. I find inspiration outside of Blur as well, of course.  
  
There are countless modelers, sculptors, and painters out there that drive me. I'm a big fan of the creations of Jordu Schell, Rick Baker, and Stan Winston - their contributions to cinema have been nothing short of incredible. I am also a huge fan of fantasy artist Richard Corben, I love his color pallet and paintings. Of course, along with him goes the wonderful Frank Frazetta. Zdzislaw Beksinski has always been a great source of inspiration too - you can get lost in his paintings! Some of these guys I feel are mentioned often, but it's for a good reason. They are amazing at what they do!   
  
I'm a fan of low-brow art as well. There is some brilliant stuff going on there, from Robert Williams to Robert Crumb and everything in between. And some tattooers have made some awesome contributions to that scene too. There is some cool stuff going on there and not just on skin. A few names that come to mind are Timothy Hoyer, Aaron Coleman, Tim Lehi and Watson Atkinson. Also, switching gears back to 3D, it only takes a few minutes at any of the front-runner digital websites like ZBrush Central, 3DTotal, CGHub, and CGTalk to get a good dose of inspiration. I see stuff the younger kids are doing these days and it's incredible. Kinda scary to be honest, but it keeps you on your toes! So much awesome stuff out there, I can go on for days!
 
Could you tell us a bit about your current workflow?  
  
Sure. It's a little more chaotic than most others I would think. The reason being I tend to use XSI and Max in tandem with some ZBrush and Mudbox. I'm a huge fan of XSI's modeling tools; in my opinion no other package that I have tried has come close to the elegance of XSI's polygon tools. The Tweak tool, Proportional tool, and Move Proportional tool are so nice to manipulate polygons. It has a very organic feel to it. XSI also handles dense meshes very well so it can handle my ZBrush meshes with ease. Being that Blur uses two major packages, I get to utilize the best of both. I will poly model everything in XSI, then go to ZBrush or Mudbox, then out to Max to paint and texture. Blur uses XSI to animate, but everything is cached out and run through Max for rendering, so I have to texture in Max.   
  
As you can see, I'm in and out of a bunch of software. It usually runs somewhat smoothly, but when you are in and out so much, sometimes meshes break and point counts change. It can be a pain but it's manageable. I actually really enjoy having the ability to tap into two major packages. Each package has strengths and weaknesses and it's nice to be able to utilize them both for certain things. For the majority of work, I'll poly model in XSI and texture in Max. I use ZBrush for 90% of my sculpting needs, but I'm trying to give Mudbox some love on the side because it's got some good stuff going on there as well.  
  
Usually to kick a character off we are handed a concept or we might have a game asset to either cannibalize or use as a reference. More often than not though the assets we get are works in progress or not fully developed yet. From there I'll build up a base in XSI and get all my poly modeling squared away and unwrapped, then it's into ZBrush for sculpting. From ZBrush we go to Max for texturing and square away all the final technical pipeline stuff to get it ready for rigging. For final rendering we use V-Ray. We switched over last year from MR and it's been a smooth transition for the most part. I like V-Ray, although I'm not sold on the skin shader yet and I miss the MR skin to be honest. The folks over at Chaos Group are very responsive though and we have had some great support.   
  
In general that's my workflow. It's fairly straightforward. It can be wonky at times, but for the most part it works well for me. I do like to experiment with new things when I can. On DC Universe I had the opportunity to change it up a bit and utilize ZBrush much earlier in the modeling process. For Cyborg I actually took a base character into ZBrush and sculpted all his armor up from the base mesh, then retopologized it later in Max. This was a very handy approach to the character; to be able to concept sculpt out the pieces of armor was very fast and effective. I actually did a small walkthrough of that process over at ZBrush Central if anyone is interested.
 
Do you feel that something is missing in today's software or do you have something you wish could be improved to ease the pain in certain areas? (Generally speaking, not software-specific).  
  
Well I find myself in multiple programs all the time and the interaction between them can be frustrating. The more you are in and out of programs, the higher the chance something is likely to break. Not to mention, it's a huge time waster to be exporting and importing files. I know ZBrush has made some steps in the right direction by integrating GoZ, but as always, it's pretty hard to get everything to play well together. I'm not saying everything should be in one package, but I would love it if everything would play nice together. Like I mentioned earlier, I currently use an XSI and Max hybrid workflow and it would be great if I could get them to interact without depending on importing and exporting OBJs. There is the FBX format, but again it doesn't always work out so well.   
  
With that being said I really wish there was something better than the OBJ we can start using as the standard file format for meshes. It would be nice if they would hold more information besides just the mesh and UV coordinates. It would be great if they retained multiple UV sets across channels and better texture information (more than the .mtl). Better formats have come along but nothing ever caught on. The OBJ is really old now and if all the packages would adopt a better file format it would be a step in the right direction.   
  
Where do you see yourself in the next ten years? Also how do you think technology will advance by then?  
  
Wow tough question, hopefully still making great art! I try not to think too much about my career other than to always give 110% and try to challenge myself on a daily basis. As for technology, who  
  
knows? The sky's the limit! Hopefully things will get more seamless and faster. Maybe one day working without any polygon limitation would be good or, even better, possibly the invention of a new way to work altogether.  
  
What is the most fun part of creating a character?  
  
I honestly enjoy all aspects of character creation. It's especially rewarding closer to the end when you are seeing it all come together. I enjoy texturing as much as modeling so it's really awesome when you see the two merge together forming the final result!  
  
What does it take to be a good character artist?  
  
Dedication and a strong foundation! A good humble attitude can't hurt either. I'm not a fan of egos in the industry. It's always good to stay grounded a