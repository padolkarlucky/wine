## 13 mm endometrial thickness after ablation

 
  
 
**## Files available for download:
[Link 1](https://urlin.us/2tHsx8)

[Link 2](https://tlniurl.com/2tHsx8)

[Link 3](https://cinurl.com/2tHsx8)

**

 
 
 
 
 
The next foray into EA came in 1967 when Cahan and Brockunier5 reported the first attempts at cryoendometrial ablation. Droegemueller et al6 described a similar technique using both Freon (DuPont, Deepwater, New Jersey, USA) and nitrous oxide probes. Despite some success, these devices proved to be costly and cumbersome and were abandoned because of the potential for producing painful hematometra.
 
Ten years after surgery, at age 55, she experienced an episode of postmenopausal bleeding and was referred to one of the authors (GAV) for management. The patient's medical problems included hypertension and hypothyroidism, for which she was taking appropriate medications. Her BMI was 24 kg/m2. On pelvic examination, the uterus was normal size, mobile, and without adnexal enlargement. Transvaginal sonography suggested a bicornuate uterus with a 1.4-cm subendometrial leiomyoma abutting the endometrial surface near the left cornua. The endometrial thickness varied from 7 to 8 mm. Both ovaries were identified and described as normal appearing. A Papanicolaou test revealed atypical glandular cells of endometrial origin.
 
In 2013, however, she began experiencing irregular menses associated with pelvic pain. Ultrasonography performed on December 8, 2014, revealed an enlarged uterus measuring 13.3 × 6.2 × 8.0 cm with an inhomogeneous myometrium containing a 4.6 × 4.3-cm area consistent with adenomyosis. The endometrium was described as irregular appearing with a thickness of 1.9 mm. On May 8, 2015, her primary gynecologist performed a diagnostic hysteroscopy and described an irregular uterine cavity with polypoid endometrium and vascular polyps. An endometrial curettage was performed but the specimen did not identify any endometrial tissue, and the patient was subsequently referred to one of the authors (GAV).
 
This cohort of postablation patients will pose at least 2 challenges for physicians. First, we must recognize that AUB may no longer be the primary symptom of PAEC,32,36 in that a significant number of women are likely to present with pelvic pain, whereas others may remain asymptomatic in the earliest stages of the disease. Second, the conventional methods to evaluate suspected cases of EC, transvaginal ultrasonography and endometrial biopsy appear to be inadequate tools after EA. While ultrasonography-guided reoperative hysteroscopic surgery43,44 may provide a useful investigative tool for PAEC, it remains a highly skill-dependent technique and is unlikely to gain widespread acceptance. In lieu of these techniques, it is likely that many women exhibiting signs or symptoms of PAEC will require hysterectomy to properly diagnose and manage the condition.
 
One of the more common causes of changes in endometrial thickness is pregnancy. Women who are having an ectopic pregnancy or who are less than 5 weeks pregnant may show signs of a thickening endometrium.
 
It is also possible for the endometrium to be too thin. Researchers define a thin endometrium as 7 mm or less. Typically, experts associate low readings of endometrial thickness with age. However, they report that 5% of people under 40, and 25% of people over 40 had a thin endometrium.
 
Endometrial ablation has been classically used for treatment of proven menorrhagia. However, a recent ACOG Practice Bulletin has added patient-perceived heavy menstrual bleeding as an added indication.1 Although endometrial ablation is a relatively safe procedure, it is nevertheless not without risk and should be reserved for patients who fail or do not tolerate medical therapy. This procedure has been studied primarily in patients with dysfunctional uterine bleeding. Although some surgeons have used endometrial ablation to treat postmenopausal bleeding resulting from hormone replacement, this approach remains controversial because of the fear of missed endometrial carcinoma. Dysmenorrhea primarily associated with the passage of clots from excessive bleeding can also be treated effectively by endometrial ablation. Other types of pelvic pain and dyspareunia do not usually respond to endometrial ablation.
 
The size of the endometrial cavity significantly affects the success rate. A uterus larger than 12 gestational weeks or a cavity bigger than 12 cm has been shown to reduce success.2, 3 However, patients with small intramural fibroids that do not result in enlargement of the endometrial cavity may be reasonable candidates for ablation. Likewise, the presence of pedunculated serosal fibroids that do not impinge on, or affect the size of, the endometrial cavity probably does not adversely affect the success of endometrial ablation. Since resectoscopic methods allow treatment of an irregular or enlarged cavity, these methods may be more successful in patients with enlarged uteri than other methods. At least one study has shown that, in experienced hands, success rates in women with uterine size greater than 12 weeks was equivalent to that of women with smaller uteri.4 Deep adenomyosis has also been associated with poor success rates after endometrial ablation.
 
The use of ultrasound to evaluate the presence and location of fibroids should be considered in those patients noted on pelvic examination to have an enlarged uteri. Even in women with apparently normal sized uterus, consideration should be given to assessing the uterine cavity by some reliable means in all patients before endometrial ablation. This may be accomplished with ultrasound, sonohysterogram or by office hysteroscopy.
 
Malignant or premalignant disease of the uterus must be ruled out before endometrial ablation. This may be accomplished by office endometrial biopsy or by hysteroscopically directed biopsy. The presence of endometrial hyperplasia at the time of ablation has been associated with the subsequent development of endometrial carcinoma.5 Thus, endometrial hyperplasia is considered a contraindication to endometrial ablation. Cervical cytology should be performed to screen for cervical disease in all patients who do not have recent Papanicolaou (Pap) smears. Patients with abnormal smears should be appropriately evaluated with colposcopy before endometrial ablation.
 
Several methods are available to produce a thin endometrium. The procedure can be performed during the immediate postmenstrual phase when the endometrium is at its thinnest. However, most surgeons prefer methods that are more likely to reliably produce an adequate thinning. A suction or mechanical curettage can be performed immediately before the endometrial ablation. Although cost-effective, this technique may produce uneven thinning. As a result, most surgeons prefer to use some form of exogenous hormones to thin the endometrium. In addition to thinning the endometrium, many hormonal methods also reduce the vascularity of the uterus and may reduce the size of the endometrial cavity.
 
Although the mechanics of the hysteroscopic methods of endometrial ablation are discussed in this chapter, the mechanics of operative hysteroscopy are not. The reader is referred to other chapters for discussion of general hysteroscopic principles, distention media, and technique.
 
The development of electrosurgical methods of endometrial ablation has resulted in a decline in the use of the laser ablation techniques. Many physicians find electrosurgical methods to be easier and to require less operator skill; also the is equipment cheaper to acquire and maintain.
 
Although endometrial resection can resect small submucous fibroids, there is an increased risk of uterine perforation with resection methods. One theoretical benefit with endometrial resection is that pieces of the endometrium are available for examination by the pathologist. With coagulation methods, no material is available for review. However, with appropriate evaluation of patients before ablation procedures, along with the possibility of target biopsy at the time of the procedure, the likelihood of missed carcinoma is quite rare.
 
As with other techniques, as a safety measure, the electrode should be activated only when it is being withdrawn toward the operator. Most surgeons prefer to begin coagulation with the anterior wall because accumulation of bubbles and debris in this area over time makes coagulation later more difficult. However, some surgeons coagulate the tubal ostia initially, with the belief that this decreases fluid loss through the fallopian tubes and, subsequently, the lateral walls and finally the posterior walls (Fig. 4). As the ablation is carried downward, care should be exercised not to treat the cervical canal itself because this could result in sealing off of the endometrial cavity with subsequent development of a hematometrium or pyometrium.
 
The technique for endometrial ablation is essentially the same as that for rollerball coagulation as noted previously (Fig. 5). Because the risk of perforation is higher with this technique, special care should be taken in the thinner area around the tubal ostia.
 
Most surgeons prefer an 8-mm diameter loop for endometrial ablation by resection. With this loop, 4 mm of tissue are resected with each pass. However, some surgeons prefer a 4-mm diameter loop because it is easier to place in restricted areas such as the ostia and because the risk of perforation is less. For this reason, this diameter loop is frequently chosen by those just learning the technique and for teaching purposes. Disadvantages to this size loop include that the loop is more easily damaged and requires more passes to obtain adequate tissue destruction.
 
Potential advantages include complete coverage of the endometrial cavity despite surface irregularities or uterine anomalies such as uterine septa