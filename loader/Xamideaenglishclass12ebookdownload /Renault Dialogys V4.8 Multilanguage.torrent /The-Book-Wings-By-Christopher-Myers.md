## The Book Wings By Christopher Myers

 
 ![The Book Wings By Christopher Myers](https://4.bp.blogspot.com/-zYnin53KkPk/Vqv-LegnVPI/AAAAAAAAAZQ/nSuYUNTO2rI/w1200-h630-p-k-no-nu/WingsByChristopherM.jpg)
 
 
**The Book Wings By Christopher Myers ⚡ [https://byltly.com/2tFiDB](https://byltly.com/2tFiDB)**

 
 
 
 
 
"I wanted to create a book that tells kids never to abandon the things that make them different, to be proud of what makes them unique. Every child has their own beauty, their own talents. Ikarus Jackson can fly through the air; I want kids to find their own set of wings and soar with him."  Christopher Myers
 
BooklistMay, 2000\*Myers, Christopher. Wings. Oct. 2000. 40p. illus. Scholastic, $16.95 (0-590-03377-8).Ages 4-9. Myers retells the myth of Icarus through thestory of Ikarus Jackson, the new boy on the block, who can fly above the rooftops and over the crowd. In this con- temporary version, the winged kid nearly falls from the sky, not because he flies too high and dares to go too near the sun, but because jeering kids in the schoolyard and repressive adults don't like his being different and try to break his soaring spirit. Even more than in Black Cat (1999), Myers' beautiful cut-paper collages are eloquent and open. Some urban scenes are like the elemental sil- houettes in cave paintings. Some are rich and elaborate, with fluid aerial perspectives that change the way we see streets and people. Then there are the images of con- straint and attack: the bullies is like a monstrous Hydra with many heads; the schoolyard like a fiery sun; Ikarus' wings caught in jagged barbed wire near the classroom blackboard. In one view, he is struggling to stay in the air above oceans and continents, and in the corner of the page is a photo of derelict rowboats. The narrator of the spare text is a lonely girl, a golden figure in most of the pic- tures, who is reaching for the boy in flight. When she finally finds the courage to stand up to the bullies, she tells Ikarus he's beautiful and gives him the strength to fly free. The resolution is a little neat, but there's so much to talk about here-the multiple meaning of the pictures, the transformation of the myth, the hero outsider. -Hazel RochmanIkarus Jackson can fly. He swoops above the rooftops and buzzes the gawking passersby, who label him "strange." The narrator of this allegory about being your true self doesn't think he's strange; she thinks he's wonderful ("lkarus Jackson, the fly boy, came to my school last Thursday. His long, strong, proud wings followed wherever he went'). To say Ikarus attracts attention is putting it mildly. Students laugh at Ikarus "useless" wings, even when they see him fly ("nobody likes a show-off"). The narrator, however, is familiar with the judgmental stares of her classmates, and she sympathizes with Ikarus' loneliness. She tells him "what someone should have long ago: 'Your flying is beautiful." Myers' ingenuous story may be a bit purposive, but his collage illustrations are powerful and affecting. Ikarus, a lanky black silhouette with white wings that spread like growing things, captures the eye in every spread; photographic images are taken out of their original context and functionally reimagined in Myers' intriguing compositions. The palette is intense whether hot or cold, and the characters themselves are an eerie amalgam of photographic and textural images. Predictability aside, this is a message worth repcating, and Myers' artistic presentation makes this a book worth having. J MD -Bulletin of the Center for Children's Books, December 2000
 
"I wanted to create a book that tells kids never to abandon the things that make them different, to be proud of what makes them unique. Every child has their own beauty, their own talents. Ikarus Jackson can fly through the air; I want kids to find their own set of wings and soar with him." -- Christopher Myers
 
Are you brave enough to be your true self? Ikarus Jackson is, but it isn't always easy. The people in his neigborhood point at his wings. The kids at school laugh. The teachers call him a distraction. One girl identifies with Ikarus, but she is too shy to speak up for herself, let alone for him. Maybe I should have said something to those mean kids, she thinks, when their taunts send him drifting into the sky.Inspired by Ikarus's own courage, she sets out in search of him and so begins her own journey of self discovery -- leaving both of them transformed.\"I wanted to create a book that tells kids never to abandon the things that make them different, to be proud of what makes them unique. Every child has their own beauty, their own talents. Ikarus Jackson can fly through the air; I want kids to find their own set of wings and soar with him.\" -- Christopher Myers
Paperback.40 pages.Dimensions (cm): 27.2 x 18.3 x 0.5
Weight (kg): 0.12 

","description":"Are you brave enough to be your true self? Ikarus Jackson is, but it isn't always easy. The people in his neigborhood point at his wings. The kids at school laugh. The teachers call him a distraction. One girl identifies with Ikarus, but she is too shy to speak up for herself, let alone for him. Maybe I should have said something to those mean kids, she thinks, when their taunts send him drifting into the sky.Inspired by Ikarus's own courage, she sets out in search of him and so begins her own journey of self discovery -- leaving both of them transformed.\"I wanted to create a book that tells kids never to abandon the things that make them different, to be proud of what makes them unique. Every child has their own beauty, their own talents. Ikarus Jackson can fly through the air; I want kids to find their own set of wings and soar with him.\" -- Christopher Myers
Paperback.40 pages.Dimensions (cm): 27.2 x 18.3 x 0.5
Weight (kg): 0.12 

","meta\_description":"Are you brave enough to be your true self? Ikarus Jackson is, but it isn't always easy. The people in his neigborhood point at his wings. The kids at school laugh. The teachers call him a distraction. One girl identifies with Ikarus, but she is too","brands":[],"tags":[],"vendor":null,"collections":["handle":"age-4","title":"Age 4+","handle":"age-5","title":"Age 5+","handle":"age-6","title":"Age 6+","handle":"age-8","title":"Age 8+","handle":"picture-book","title":"Picture Book","handle":"diversity","title":"Diversity \/ Inclusive","handle":"scholastic","title":"Scholastic"],"promotions":[]}, onVariantSelected: selectCallback, enableHistoryState: false }); // Add label if only one product option and it isn't 'Title'. Could be 'Size'. // Hide selectors if we only have 1 variant and its title contains 'Default'. $('.selector-wrapper').hide(); }); Quick Links - Contact us
 - Printables
 - Subscribe
 - Join Our Drop Ship Program
 - #ReadingTakesYouPlacesChallengeMalaysia
 - #KitaJagaKita
 - The Wise Kids Club
  Follow Us - Facebook
 - Instagram
 - YouTube
 - Vimeo
  - 
 - 
 - 
 - 
 - 
  Copyright © 2020 Muhsin Kids
 
In***Wings*** his newest effort, Myers wanted "to create a book that tells kids never to abandon the things that make them different, to be proud of what makes them unique." He believes that every child has their own beauty and talents. As Myers continues to write and develop stories that combine his message of empowerment with the art of his beautiful illustrations, our children will be well served. Standing firm on the conscious thought guiding his work,***Wings*** offers a thought-provoking story to which all children can relate. Myers introduces Ikarus Jackson, the new boy on the block. Ikarus has wings and can fly above the rooftops. Ikarus displays great pride in his wings until the staring, snickering and giggles spread across the playground. Ikarus' story explores the feelings of loneliness that occur when our differences are pointed out in a negative way and offers a profound lesson about differences and individuality. Share this lesson with your children, and encourage them, as Myers hopes, "to find their own wings and soar with him."
 
source: bush, elizabeth. review of a time to love: stories from the old testament, by walter dean myers, illustrated by christopher myers. bulletin of the center for children's books 57, no. 1 (september 2003): 26.
 72ea393242
 
 
