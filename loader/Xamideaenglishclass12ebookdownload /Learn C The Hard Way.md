## Learn C The Hard Way

 
  
 
**## Files you can download:
[Link 1](https://miimms.com/2tDpJ3)

[Link 2](https://gohhs.com/2tDpJ3)

[Link 3](https://urlcod.com/2tDpJ3)

**

 
 
 
 
 
Across the vastness of the internet, there are many resources for aspiringprogrammers to learn to program. Nowadays you can find the answer to mostcommon questions with a simple Google search. Before this explosion ofpost-modern data availability, information was disseminated in the formof the written word.
 
Recently, I came across an e-book written byZed A. Shaw entitledLearn C The Hard Way, and whileI can commend the author for spending the time and energy to write it,I would NOT recommend it to anyone seriously interested in learning theC programming language. In fact, if you were one of the unlucky souls whohappened to have purchased it. Go right now and at least try to get yourmoney back!
 
The whole point of this article is this: Don't learn C the wrong way.Do your own research, learn as much as you can, read K&R2, digaround and read other people's code, ask questions on IRC, etc. Don'tjust take anyone's word for it, not even mine.
 
It's an ironic quote, considering the author has published two otherworks dedicated to higher-level languages:The C programming language's only failing is giving you access to whatis really there, and telling you the cold hard raw truth. C gives youthe red pill. C pulls the curtain back to show you the wizard. C is truth.
 
When it comes to setup instructions, it doesn't appear that the authorwent to any length to describe how one's environment should be set-up.In my opinion, second only to the importance of learning the languageitself is knowing how to use it within your environment. For Mac OS X,the author writes:... search online for "installing xcode" for instructions on how to do it.
 
If I were a novice programmer and had purchased this book, I'd have apretty good case of the red ass about right now if I didn't know howto set-up my own environment. The author goes on to lament that:An IDE, or "Integrated Development Environment" will turn you stupid.They are the worst tools if you want to be a good programmer becausethey hide what's going on from you, and your job is to know what's goingon. They are useful if you're trying to get something done and theplatform is designed around a particular IDE, but for learning to code C(and many other languages) they are pointless.
 
At this point, the only thing I can think is, "I'd just love for youto show me a damn working Makefile!" A novice will be thinking, "Whatthe hell's a Makefile?" as the concept of a Makefile has not yetbeen introduced. Not to mention the fact that all the examples thusfar have been broken, presumably to force the user to make a commonmistake so that they learn from it, but this book's "benefit of thedoubt" allowance is starting to run out.
 
K&R2 Chapter 2 covers this quite nicely:Each compiler is free to choose appropriate sizes for its own hardware,subject only to the restriction that shorts and ints are at least 16bits, longs are at least 32 bits, and short is no longer than int, whichis no longer than long.
 
Finally in "Exercise 26" you're supposed to write your own program. Eventhough you have no Makefile, you haven't found Jimmy Hoffa yet, andthere's been hardly any explanation of how to invoke the compiler. So,I'm just supposed to poke in some code, go to my terminal, type thismagic 'make' word, and expect a built and working program, right?
 
Sound familiar? There are more holes in the author's logic than in halfof all the termite infested homes world-wide. If, as the author statesin the introduction C is "telling you the cold, hard truth", whydoes he now complain that the "truth" wasn't sugar-coated enough?
 
I love the words, "empirical evidence," especially when not a shred ofevidence has been offered to accompany them. Then, he seems todo a 180 and say that he agrees with his critics, then goes on to dodgethe issue entirely by saying we should "just use something else" because"I don't want to have to check my work -- because it's too hard."
 
Interestingly enough, just a couple weeks ago, on the 4th of January,2015; the author removed the original version of this section fromhis book, and replaced it with this statement presumably "admitting defeat" in his questto sack the temple of K&R as it were; proclaiming that "C is dead" and"curse all those pedantic C developers, I'm going to go learn Go!"
 
Well, if that's really what you want to do, then go for it. But, beforeyou rewrite your C book, perhaps you should take the time to actuallydig into it and learn C first (inside and out.) Then determine whetheryou're actually qualified to write a book on the subject. I'd say thatjudging by what you've written both in your book, and this particularpost, that you have a long way to go before you can master how to usea "error prone shitty language like C" as you put it. If you hold Cin such low regard, why the hell would you want to write a book toencourage people to learn it, anyway? It makes no sense to me. Perhapsat the time you sat down to write your book, you had a differentopinion, and that opinion changed when you just couldn't be right.
 
Learn how to accept criticism, and admit to yourself that you're notalways right. Go out and have your book peer reviewed by people who areactually writing code in C, and don't assume that they're just a bunch ofpedantic assholes just because they point out that you are wrong inyour assumptions. Approach the community with an eagerness to learn anddo, and learn from our experience, instead of positioning yourself assomeone who has to prove that the world is wrong and you are right.That won't get you anywhere in any situation. Furthermore, you willnever win an argument with pedantic people by arguing from emotion.Period.
 
If you don't plan on using a programming language yourself, how willyou ever learn it to the extent that you should, in order to teachothers? I implore you, try coming to grips with the subject matterbefore writing a book, and save us all a lot of headache. If you wantto teach others, you have to teach yourself first.
 
Here is the original email, from Zed, which began the thread way back onFebruary 3rd:Hey Tim, this is me Zed. Just dropping you a quick email before I actuallysleep. I liked what you wrote about my book, and I actually think you maylearn some things from me. I'd like you to be a technical reviewer for mybook, and I'd pay you for it too.
 
I'll write more about it tomorrow, but basically you'd take exercises as Iwrite them and point out technical flaws or definition flaws. The book'sdesign is actually going to change in target audience away from total newbsand more to people who need to learn secure coding and C.
 
"Learn C the hard way" is definitely a great book for modern C. Other than that, I find that like with almost any other programming learning challenge, the best thing to do is find something you want to build and do so in C!
 
I absolutely agree. However, as a self-trained developer in several languages, I have learned there is significant value to following books and courses. **When you learn via a project, you only absorb those parts of the language you needed.**
 
Case in point, although I first started using Python in 2011, it wasn't until 2018 that I learned several tools in Python, including decorators and list comprehensions. Why? I'd never needed them, or else, never realized I could use them in place of some approaches I was more used to. Despite nearly seven years of regular Python development, I knew I had some massive gaps in my knowledge, so I started going through a comprehensive Udemy course for the language, and I'm so glad I did!
 
This isn't to say that projects aren't important -- you won't ever truly master a language until you build something meaningful in it -- rather, **one should pair project-based learning with a good quality book or self-paced course.**
 
If you want a good grounding in Antarctic exploration, view Shackleton and all its bonus material. If you want a good lesson in the importance of openness to learning in project management, view the **Antarctic: a Frozen History**, a History Channel documentary. That program makes the point that the British explorers -- Robert Falcon Scott and Ernest Shackelton -- were not inclined to learn from native peoples who knew how to live and work in cold regions. In contrast, Roald Amunsen, a Norwegan, lived with natives in northern Canada and learned from them. Amunsen adopted clothing and other technology from those people. Also, he used dog sleds and recruited experienced drivers for his dog teams. And finally he recruited team members who were skilled at cross country skiing. While Amunsen was able to get to the South Pole and back with relative ease, the British explorers were doing it the hard way with inadequate clothing and equipment. Scott and his companions did make it to the pole but died on the return trip. Shackleton's expedition never got off to a good start because his ship was caught in a ice flow and crushed. The **Shackleton** miniseries shows the leadership skills employed by Shackleton himself during his heroic and successful efforts to save his crew.
 72ea393242
 
 
