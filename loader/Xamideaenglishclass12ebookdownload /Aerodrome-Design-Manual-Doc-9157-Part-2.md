## Aerodrome Design Manual Doc 9157 Part 2

 
  
 
**## Files you can download:
[Link 1](https://gohhs.com/2tCs32)

[Link 2](https://urlcod.com/2tCs32)

[Link 3](https://tinourl.com/2tCs32)

**

 
 
 
 
 
# Aerodrome Design Manual Doc 9157 Part 2: A Guide for Taxiways, Aprons and Holding Bays
 
If you are involved in the design, construction or maintenance of aerodromes, you may be interested in the Aerodrome Design Manual Doc 9157 Part 2, published by the International Civil Aviation Organization (ICAO). This document provides guidance on the implementation of the specifications and recommendations in Annex 14, Volume I, of the Convention on International Civil Aviation, which covers aerodrome design and operations.
 
The Aerodrome Design Manual Doc 9157 Part 2 focuses on taxiways, aprons and holding bays, which are essential components of an aerodrome infrastructure. Taxiways are the paths that connect runways with aprons, terminals and other facilities. Aprons are the areas where aircraft are parked, loaded, unloaded and serviced. Holding bays are the areas where aircraft wait for clearance to enter or cross a runway.
 
The purpose of this manual is to assist States in designing taxiways, aprons and holding bays that ensure a smooth, safe and efficient flow of aircraft ground traffic. The manual covers topics such as:
 
- The general layout and configuration of taxiways, including rapid exit taxiways, dual or multiple taxiways, parallel taxiways and fillets.
- The design criteria for taxiway physical characteristics, such as width, gradient, surface, markings, lighting and signs.
- The design criteria for taxiway shoulder and strip dimensions, which provide clearance and protection for aircraft manoeuvring on taxiways.
- The design criteria for apron layout and dimensions, taking into account the types and sizes of aircraft that use the aerodrome.
- The design criteria for apron markings, lighting and signs, which facilitate the identification and guidance of aircraft on aprons.
- The design criteria for holding bay layout and dimensions, which provide adequate space and separation for aircraft waiting to take off or cross a runway.
- The design criteria for holding bay markings, lighting and signs, which indicate the location and boundaries of holding bays.

The manual also provides illustrations of diagrams and charts that show the impact of newer generation, larger aircraft on existing aerodromes. Additionally, the manual includes appendices that contain detailed information on fillet design methods, wing tip clearance charts for wide body aircraft and a corrigendum that updates some parts of the manual.
 
The Aerodrome Design Manual Doc 9157 Part 2 is a valuable resource for anyone who wants to learn more about the best practices and standards for taxiways, aprons and holding bays at aerodromes. You can purchase or download the manual from the ICAO Store[^1^] or access it online from other sources[^2^] [^3^].
  
Why is it important to follow the Aerodrome Design Manual Doc 9157 Part 2? By following the guidance and recommendations in this manual, you can ensure that your aerodrome meets the international standards and best practices for safety, efficiency and compatibility. This can help you to:

- Reduce the risk of accidents and incidents involving aircraft, vehicles and personnel on the ground.
- Improve the operational capacity and performance of your aerodrome.
- Enhance the customer satisfaction and comfort of passengers, airlines and other users of your aerodrome.
- Facilitate the integration and interoperability of your aerodrome with other aerodromes and air traffic management systems.
- Comply with the legal obligations and responsibilities of your State under the Convention on International Civil Aviation.

How can you apply the Aerodrome Design Manual Doc 9157 Part 2 to your aerodrome? The manual is intended to provide general guidance and recommendations that can be adapted to suit the specific needs and conditions of your aerodrome. You should consider factors such as:

- The type and volume of traffic that your aerodrome handles or expects to handle in the future.
- The size and configuration of your aerodrome site and its surrounding environment.
- The meteorological and climatic conditions that affect your aerodrome operations.
- The availability and feasibility of resources and technology for your aerodrome development and maintenance.
- The coordination and consultation with relevant stakeholders and authorities for your aerodrome planning and design.

The manual is not a substitute for professional judgement and expertise. You should always consult with qualified engineers, architects, planners and other specialists who can assist you in applying the manual to your aerodrome. You should also keep abreast of the latest developments and innovations in the field of aerodrome design and management.
 842aa4382a
 
 
