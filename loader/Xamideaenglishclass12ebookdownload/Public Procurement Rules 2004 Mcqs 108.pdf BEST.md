## Public Procurement Rules 2004 Mcqs 108.pdf

 
  
 
**DOWNLOAD ››› [https://tweeat.com/2tyEAB](https://tweeat.com/2tyEAB)**

 
 
 
 
 
# Public Procurement Rules 2004 MCQs: A Comprehensive Guide
 
If you are preparing for a test or an interview that requires knowledge of the Public Procurement Rules 2004 (PPRA Rules), you might want to check out this guide. In this article, we will provide you with multiple choice questions (MCQs) and answers on various topics related to the PPRA Rules. These MCQs are based on the official PDF document of the PPRA Rules, which you can download from [here](https://www.scribd.com/document/521331696/Public-Procurement-Rules-2004-Mcqs-Solved-Download).[^1^]
 
The PPRA Rules are a set of rules and regulations that govern the procurement of goods, services and works by the federal government and its agencies in Pakistan. The PPRA Rules aim to ensure transparency, accountability, efficiency, economy and fair competition in public procurement. The PPRA Rules were framed in the year 2004 under section 26 of Public Procurement Regulatory Authority (PPRA) Ordinance, 2002, and the Regulations are issued from time to time under Section 27 of PPRA Ordinance, 2002.[^3^]
 
The PPRA Rules cover various aspects of public procurement, such as methods of procurement, bidding procedures, bid evaluation criteria, contract award and management, complaint redressal mechanism, debarment of bidders, etc. The PPRA Rules also prescribe the roles and responsibilities of the procuring agencies, the bidders, the PPRA and other stakeholders involved in public procurement.
 
In order to test your understanding and knowledge of the PPRA Rules, we have compiled a list of MCQs with answers below. These MCQs cover some of the important topics and concepts related to the PPRA Rules. You can use these MCQs as a self-assessment tool or as a revision material before your test or interview. We hope you find this guide helpful and informative.
 
## Public Procurement Rules 2004 MCQs with Answers
 
1. What does "bid" mean according to the PPRA Rules?

a) A tender, or an offer, in response to an invitation, by a person, consultant, firm, company or an organization expressing his or its willingness to undertake a specified task at a price.

b) A person who submits a bid.

c) A procedure leading to the award of a contract whereby all the interested persons, firms, companies, or organizations may bid for the contract.

d) None of the above.

**Answer: a)**
2. What is the principal method of procurement according to the PPRA Rules?

a) Open competitive bidding.

b) Closed competitive bidding.

c) First-in first-out (FIFO).

d) Last-in first-out (LIFO).

**Answer: a)**
3. What does "contract" mean according to the PPRA Rules?

a) An agreement enforceable by law.

b) An order with general terms and pricing under closed framework agreement.

c) An arrangement which allows repeated procurement.

d) None of the above.

**Answer: a)**
4. All procurement opportunities over \_\_\_\_\_\_\_ rupees should be advertised in the newspaper according to the PPRA Rules.

a) 0.5 million

b) 1.5 million

c) 2.0 million

d) 2.5 million

**Answer: c)**
5. The procurement opportunities over two million rupees should be advertised in at least \_\_\_\_\_\_\_ newspaper(s) according to the PPRA Rules.

a) One

b) Two

c) Three

d) Four

**Answer: b)**
<!-- Truncated due to character limit -->842aa4382a




