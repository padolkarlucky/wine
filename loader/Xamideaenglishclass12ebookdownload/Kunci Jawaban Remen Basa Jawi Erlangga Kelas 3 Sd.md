## Kunci Jawaban Remen Basa Jawi Erlangga Kelas 3 Sd

 
  
 
**Kunci Jawaban Remen Basa Jawi Erlangga Kelas 3 Sd >>>>> [https://urevulad.blogspot.com/?d=2tyu0X](https://urevulad.blogspot.com/?d=2tyu0X)**

 
 
 
 
 ```html 
# Kunci Jawaban Remen Basa Jawi Erlangga Kelas 3 SD: Tema Pengalaman
 
Apakah Anda sedang mencari kunci jawaban buku Remen Basa Jawi Erlangga kelas 3 SD? Jika ya, maka Anda berada di tempat yang tepat. Artikel ini akan memberikan kunci jawaban lengkap untuk tema pengalaman yang terdiri dari empat piwulang. Tema pengalaman adalah tema keenam dalam buku Remen Basa Jawi Erlangga kelas 3 SD. Tema ini bertujuan untuk mengembangkan kemampuan siswa dalam menceritakan pengalaman menarik dalam ragam krama dan ngoko.
 
Kunci jawaban yang disajikan dalam artikel ini bersumber dari buku Remen Basa Jawi Erlangga kelas 3 SD yang diterbitkan oleh Penerbit Erlangga. Kunci jawaban ini dapat digunakan sebagai bahan referensi atau bantuan belajar bagi siswa, orang tua, atau guru. Namun, sebaiknya siswa mencoba mengerjakan soal-soal terlebih dahulu sebelum melihat kunci jawaban ini. Selain itu, kunci jawaban ini tidak bersifat mutlak dan dapat berbeda dengan jawaban yang diberikan oleh guru atau sekolah.
 
Berikut adalah kunci jawaban Remen Basa Jawi Erlangga kelas 3 SD tema pengalaman:
 
## Piwulang 1: Maca Batin Crita Pandawa
 
Pada piwulang pertama ini, siswa diminta untuk membaca dan memahami cerita tentang pengalaman Pandawa saat berada di hutan Dandaka. Cerita ini diambil dari kitab Mahabharata yang merupakan salah satu karya sastra Jawa klasik. Siswa juga diminta untuk mengartikan kata-kata sulit dan menjawab pertanyaan tentang isi cerita.
 
Berikut adalah kunci jawaban untuk piwulang 1:
 
1. Kata-kata sulit dan artinya:
    - Ngungkuli: menutupi
    - Ngayomi: melindungi
    - Ngungsi: mengungsi
    - Laras: senjata
    - Ngayun: mengayunkan
    - Ngandel: memotong
    - Nyilih: meminjam
    - Nyambut gawe: bekerja
    - Nyekel: menggenggam
    - Nyilih: meminjam
2. Pertanyaan dan jawaban:
    - Sapa sing ngungkuli Pandawa lan Drupadi? 
Jawab: Ratu Salya lan Ratu Sudesna sing ngungkuli Pandawa lan Drupadi.
    - Apa sing diayomi Pandawa nalika ana ing alas Dandaka? 
Jawab: Pandawa diayomi dening para rsi lan para dewa nalika ana ing alas Dandaka.
    - Kepiye carane Pandawa bisa ngalahake Kalayawana? 
Jawab: Pandawa bisa ngalahake Kalayawana kanthi cara nyilih laras gada saka Batara Kresna lan ngandel sirah Kalayawana kanthi laras kuwi.
    - Apa sing digawe Pandawa nalika ana ing Kerajaan Wirata? 
Jawab: Pandawa digawe nyambut gawe kanthi cara mbiyantu Ratu Wirata ing saben bidange. Yudistira dadi guru, Bima dadi juru masak, Arjuna dadi guru tari, Nakula dadi pawang kuda, lan Sadewa dadi pawang sapi.
    - Apa

```html

## Piwulang 2: Nggagas Crita Pengalaman

        Pada piwulang kedua ini, siswa diminta untuk menggagas cerita pengalaman sendiri atau orang lain dalam ragam ngoko. Siswa harus memperhatikan unsur-unsur cerita seperti tokoh, latar, alur, konflik, dan penyelesaian. Siswa juga harus menggunakan kata-kata yang sesuai dengan kaidah bahasa Jawa.

        Berikut adalah contoh kunci jawaban untuk piwulang 2:

        Cerita Pengalaman: Ngunduh Mangga

        Aku lan kancaku Dika isih dadi siswa SD kelas 3. Dina iku, aku lan Dika pengin ngunduh mangga ing kebon Pak Slamet sing ana ing pinggir desa. Kebon Pak Slamet terkenal duwe pohon mangga sing gedhe lan buahnya sing gedhe lan manis. Aku lan Dika ora duwe dhuwit kanggo tuku mangga, mula arep ngunduh wae.

        Aku lan Dika nyiapake tangga bambu lan karung kanggo ngunduh mangga. Kita banjur lunga menyang kebon Pak Slamet nalika jam istirahat sekolah. Kita milih pohon mangga sing paling gedhe lan akeh buahnya. Aku njukuk tangga bambu neng ngisor pohon mangga, Dika njaluk tangga lan nglebokake neng dhuwur pohon. Dika mulai ngunduh mangga sing gedhe-gedhe lan njeblosake neng karung sing tak pegat.

        Nalika aku lan Dika lagi asyik ngunduh mangga, tiba-tiba ana suara seng ndelikake kita. Suara kuwi saka Pak Slamet sing lagi lunga menyang kebon. Pak Slamet ndelok aku lan Dika sing lagi ngunduh mangga tanpa idin. Pak Slamet marah banget lan ngguyu kita. Aku wedi banget lan arep lunga ninggalake tangga lan karung. Nanging Dika isih neng dhuwur pohon mangga lan ora bisa turu.

        Aku banjur njaluk Pak Slamet supaya ora marah lan maringi Dika turu saka pohon mangga. Aku uga njaluk pangapunten kanggo ngunduh mangga tanpa idin. Pak Slamet awalane ora setuju lan arep ngomong karo guru kita neng sekolah. Nanging sawise aku njelasake yen aku lan Dika pengin nyobake buah mangga Pak Slamet sing terkenal manis, Pak Slamet akhire setuju maringi kita turu saka pohon mangga.

        Pak Slamet uga maringi kita duwe sawetara buah mangga sing wis tak unduh. Nanging Pak Slamet njaluk kita ora ngunduh mangga maneh tanpa idin. Aku lan Dika njaluk pangapunten maneh lan janji ora bakal ngunduh mangga tanpa idin maneh. Aku lan Dika banjur lunga ninggalake kebon Pak Slamet kanthi gembira. Kita seneng banget wis duwe buah mangga sing manis saka kebon Pak Slamet.

## Piwulang 3: Maca Batin Crita Pengalaman

        Pada piwulang ketiga ini, siswa diminta untuk membaca dan memahami cerita tentang pengalaman Arjuna saat berada di Kerajaan Wirata. Cerita ini juga diambil dari kitab Mahabharata yang merupakan salah satu karya sastra Jawa klasik. Siswa juga diminta untuk mengartikan kata-kata sulit dan menjawab pertanyaan tentang isi cerita.

        Berikut adalah kunci jawaban untuk piwulang 3:

< 842aa4382a




