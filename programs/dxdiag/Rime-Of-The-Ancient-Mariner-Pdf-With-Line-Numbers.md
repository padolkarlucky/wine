## Rime Of The Ancient Mariner Pdf With Line Numbers

 
  
 
**DOWNLOAD >>> [https://www.google.com/url?q=https%3A%2F%2Ftinurll.com%2F2tDtBe&sa=D&sntz=1&usg=AOvVaw0RI70qQrZFAsuHTiCiBSq-](https://www.google.com/url?q=https%3A%2F%2Ftinurll.com%2F2tDtBe&sa=D&sntz=1&usg=AOvVaw0RI70qQrZFAsuHTiCiBSq-)**

 
 
 
 
 
Download the free Coleridge ebook:
**epub
 mobi (for Kindle)
 pdf**
(If pdf fails to download, please try **here**.)

There is also a separate pdf of **The Rime of the Ancient Mariner** (full text with line numbers).
 
For more classic poetry, try these websites:
Project Gutenberg for free ebooks.
Or to read online:
The Poetry Foundation (a clear and informative website)
Bartleby (lots of volumes of online verse)
Kalliope (a Danish site, but with works by many British poets)
Luminarium (medieval to 18th Century poetry)


 
\* For the two last lines of this stanza, I am indebted to Mr. Wordsworth. It was on a delightful walk from Nether Stowey to Dulverton, with him and his sister, in the Autumn of 1797, that that [sic] this Poem was planned, and in part composed.
 
"Explain, clearly, this line from "The Rime of the Ancient Mariner": "He prayeth best, who loveth best."" eNotes Editorial, 27 June 2019, -help/explain-clearly-this-line-from-the-rime-of-the-200249.Accessed 9 Feb. 2023.
 
The line "He prayeth best, who loveth best" is the moral the Mariner learns from his supernatural journey. It echoes the theology of Augustine in Confessions, which Samuel Taylor Coleridge was very likely intentionally referencing in writing this poem. (Coleridge was a religious man very concerned with theology, even changing denominations of Christianity a few times in his life. An exploration of his collected notes also shows he was continuously reflecting upon his evolving belief system.)
 
As always with any question that focuses on specific quotations, it is incredibly important to focus on the context of the quote, or what comes both before and after it to help understand the meaning of the specific quote you have extracted from the text. If we have a look at the quote you refer to in this brilliant poem, we can see that this famous line is actually part of a stanza that comes towards the very end of the poem, and is used to sum up the moral of the story:
 
Coleridge adds a very poignant quality to the tale with these lines. Perhaps more than any other in the poem, they serve to remind the reader of the normal world to which the Mariner once belonged. It is ironic that he is seen as having a family only after he is forced to work next to his nephew, with whom there is no longer any possibility of love or communication.
 
The contrast continues in these lines. When the dead first rose, they groaned (a typical Gothic detail). As the day dawns, the spirits become music, creating a marvelous mixture of sounds and notes. In these lines, sound and music are used to create a sense of peace. Another interesting contrast is with the spirits that gather around the mast at dawn: before its death the albatross perched there every evening.
 
When the iambs above are read aloud, the emphasis falls on every second syllable. The meter of the first line is in iambic tetrameter because it contains four iambic units in each line (totalling eight syllables), whereas the meter of the second line is in iambic trimeter because it contains only three iambic units (totalling six syllables).
 
If the albatross is anything, it is a synecdoche, a concept or figure of speech by which a part is used to stand for the whole. In this case the albatross stands for all animals. By coupling the absence of described color with the unexplained murder, the killing can be viewed as a synecdoche for all killing of animals apart from that of killing to eat. That is, killing an animal apart from the purpose of survival, Coleridge seems to say, is a crime, i.e., murder, not only against the animal but against all of nature, especially one policed by spirits.
 
***The Rime of the Ancient Mariner*** is a long poem written by **Samuel Taylor Coleridge**. What is *The Rime of the Ancient Mariner* about? The story presented in the poem is narrated by a sailor, or mariner, who is compelled to tell his tale to certain people he encounters. The ship on which the mariner works experiences good fortune until the mariner shoots a friendly albatross with his crossbow. After this event, the ship and its crew seem to be cursed.
 
As the poem begins, three men are about to go to a wedding. An old man waits outside, however, and draws one of the men aside. The old man, the ancient mariner, tells the wedding guest that he feels compelled to tell him a story about an event that happened while he was at sea. The guest wants to go to the wedding, but bewitched by the old man's "glittering eye," he stays to listen to the tale.
 
The mariner says the ship he worked on had experienced good fortune as it sailed toward the equator. As the guest hears the music coming from the wedding hall he has a strong desire to go to the wedding, yet he cannot stop himself from listening to the old man's story. The ship encounters nasty weather, the mariner says, first encountering a massive storm and then mist, snow, and ice. The mariner describes the dangerous ice as "mast-high." Then an albatross appears in the mist. The sailor begin to feed the albatross as it flies around the ship. Suddenly, the ice begins to crack, a south wind blows, and the ship is able to move again. The albatross continues to follow the ship, and the mariner calls to it and feeds it every day. One day, the mariner shoots the albatross with his crossbow.
 
All aboard the ship rejoice when a ship's sail is spotted in the distance. Something is strange about the ship, however. Its sails are tattered and thin, and the only passengers are Death and Life-in-Death. The sailors have encountered an abandoned ghost ship. As the ship comes closer, Death and Life-in-Death are casting dice to determine who will decide the fate of the ancient mariner and the crew. Life-in-Death wins and the men begin to die. No such fate awaits the ancient mariner, and he watches in horror as the dead seem to accuse him with their unclosed eyes.
 
The burden of the albatross now removed, the sailor sleeps at last. He dreams the buckets on the ships deck are filled with dew and he wakes to rain. The mariner drinks until his thirst is quenched. A squall blows up, but the wind never touches the ship. The dead arise and noiselessly return to their posts on the ship. The mariner's nephew stands near him as they work, but the boy never speaks.
 
The wedding guest again admits that he is afraid, but the mariner assures him the bodies were animated by spirits, not the men themselves. The spirits sing like angels, while the boat plows ahead without benefit of the wind. The dead sailors now steer the ship toward home. Eventually, the boat begins to speed through the water, which causes the mariner to faint from the sudden acceleration. While he is unconscious, the mariner hears two voices discussing the matter of the albatross. One voice says, "The man hath penance done,/And penance more will do."
 
*The Rime of the Ancient Mariner* analysis or close reading indicates that meaning comes from the cycle of sin, penance, and absolution of the mariner. When he sins by killing the innocent albatross, he is separated from God to such an extent that he cannot pray. His penance takes two forms. First, the mariner experiences the terrifying events while he is still aboard the ship, but then his penance continues after his rescue. He must continually tell his story to certain people he encounters. The next step in the cycle, absolution, seems to evade him, however. If he experiences any feeling of forgiveness or relief from the sin, it is brief. The mariner will soon repeat the penance step again as he has done with the wedding guest. He is doomed to the relentless pursuit of absolution that never comes.
 
*The Rime of the Ancient Mariner* albatross is the most important symbol in the poem. The white bird represents innocence. It plays with the sailors as they feed it. It flies around the boat, providing entertainment from the drudgery of their work. It does no harm. When the ancient mariner shoots it with his crossbow, he sins by killing the innocent bird. The sailors then hang the dead albatross from the mariner's neck, and the bird becomes both a literal and figurative burden.
 
Coleridge explores several themes in *The Rime of the Ancient Mariner.* Sin and restoration are central concerns in the poem. The mariner kills the albatross, not for food or survival, but for sport, an indication that he has no respect for the animal's life. Only one line of the poem is devoted to this act, as if his disregard for the creature is so much a part of the man that he does not consider his action before or after he fires his crossbow.
 
The other sailors, because they associate the albatross with the good winds they experience, are horrified by the mariner's thoughtless act. The mariner must realize and acknowledge his sin before the natural and supernatural forces that besiege him finally relent. Even then, he must continually repent by sharing his story with others.
 
*The Rime of the Ancient Mariner* was first published in *Lyrical Ballads* in 1798. It was the first poem in the collection of poems by Coleridge and William Wordsworth. In 1817, Coleridge republished the poem in *Sibylline Leaves* along with annotations he wrote to accompany the poem.
 
***The Rime of the Ancient Mariner*** was written by **Samuel Taylor Coleridge** and first published in 1798. The poem is narrated by the mariner, who is compelled to repeat his story to certain people he encounters. The narrative poem describes the ancient mariner's experience aboard a ship after he kills an albatross. The bird has brought joy and good fortune, and on