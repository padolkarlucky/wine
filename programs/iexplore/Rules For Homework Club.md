## Rules For Homework Club

 
 ![Rules For Homework Club](https://2.bp.blogspot.com/-6zDJv_la33A/UdeWPzhxlpI/AAAAAAAAFjY/rYwrbE06Aus/s1600/homework+club_preview_Page_1.jpg)
 
 
**DOWNLOAD 🗸🗸🗸 [https://www.google.com/url?q=https%3A%2F%2Furloso.com%2F2tF5aG&sa=D&sntz=1&usg=AOvVaw3tRCDRPlZWH6WbvvWZ6ezC](https://www.google.com/url?q=https%3A%2F%2Furloso.com%2F2tF5aG&sa=D&sntz=1&usg=AOvVaw3tRCDRPlZWH6WbvvWZ6ezC)**

 
 
 
 
 
A homework club is a **safe, supportive**and**productive** student meetup out of school hours to assist students in completing their homework. Homework clubs provide vital support for children who do not otherwise have the help that they need.
 
Having a **solid team** of parent helpers is crucial to setting up a homework club. Remember, this is not all on you. **Seek out help** and support from your principal, teaching colleagues, parents and school community.
 
Our Active Learning Resource Pack is a brilliant resource pack that includes 15 active games to play as the perfect brain break before or during homework club. Active games promote fun, active ways of revising learning and are perfect for small groups.
 
Take the time to consider the possibility of initiating a homework club. Perhaps, all you have to do is suggest the idea, work out the who, what, where, when and why and find the right team to run the show.
 
Students who go to Homework Club should be staying for the entire hour. Please do not pick up your students early from Homework Club, as it they may not receive a stamp (credit) if they are picked up early. Students who check in to Homework Club and leave of their own accord to 'hang out' elsewhere will receive a consequence. Students must be picked up from Homework Club by 3:30pm at the latest.

Students should bring homework to Homework Club. Students without homework or a library book to read should not be in Homework Club. Students who are not working are a distraction to others and will be removed and may receive a consequence. Repeat offenders may lose the privilege of attending.

Only students who attend Homework Club, or stay under the tutelage of a teacher, may ride the late bus home. Students without a Homework Club stamp will be denied entrance to the late bus and may receive a consequence.
 
**Attendance at homework clubs soon could rival the turnout for more traditional afterschool offerings. The clubs give students the help and structure they need to complete assignments. Included: Descriptions of how homework clubs are organized.**
 
Often when youngsters get around to starting their homework, distractions from television, computers, friends, and family make studying a challenge and help is not available. So more students are doing their homework in places other than home, such as school-, library-, and community-sponsored homework clubs.
 
After-school homework clubs are growing in many communities, with most of them focused on elementary and middle school students. Clubs meet at least one day a week after school and are supervised by teachers or faculty members and volunteers. Students receive general academic help and/or homework assistance.
 
While some homework clubs are more formal than others, most allow students to "attend" whenever they feel the need. The programs are geared not only to children having trouble with schoolwork, but those who find it hard to concentrate at home or have no one to provide homework assistance.
 
"We try to re-enforce concepts and the instruction from class," said Damiano Russo, assistant principal of Dearborn Street Elementary School in Northridge, California. "We have a diverse population, and many parents working two jobs. We want to be available for the students. Sometimes teachers will recommend the club to parents if their children are having trouble concentrating at home."
 
The Dearborn club meets for 90 minutes four days a week, and is staffed by a teaching assistant and a volunteer, who familiarize themselves with classroom assignments. About 25 children attend each day. "It's goal-directed, we try to minimize distractions," Russo added.
 
Suzanne Piotrowski, a learning disabled specialist at Beech Street Elementary School, in Manchester, New Hampshire, said her school's homework club for third through fifth graders is equally popular. The club meets for an hour after school, three days a week, and draws about 60 students each week.
 
"Students get tutoring and help completing homework," Piotrowski told Education World. "Some just do homework and some need re-explanation." School officials had hoped more special education students would attend, but the club has been more popular with mainstream students.
 
The school started the club because a fifth-grade teacher started helping some students after school who she knew had trouble doing homework at home because of noise or other difficulties, according to Piotrowski.
 
The move to middle school also can mean a big change on the homework scene, and the homework club at Granite Mountain Middle School in Prescott, Arizona, has been so heavily attended that the school had to find another teacher for seventh graders, said Marilyn McCready, the school's library media specialist, who oversees the homework club. "It's very popular and more popular after report cards come out," McCready told Education World.
 
Granite's club also meets for an hour after school two days a week, and has a drop-in policy. The only requirement is that once students show up, they must stay for the whole hour unless a parent comes to pick them up. "We've made it as easy as we can."
 
Teams at Bennet Middle School in Manchester, Connecticut, also organize homework clubs, and set up a schedule for staffing them, said language arts teacher Jenna Brohinsky, team leader for the Royal 7's, a seventh grade team. Students can come for an hour of help after school on Tuesdays, Wednesdays, and Thursdays, and some get a chance to work in the computer lab, Brohinsky added.
 
The Toronto Public Library operates the Leading to Reading program to help youngsters improve reading skills and homework clubs in 33 of its 99 branches. The Toronto clubs are more formal than some of the after-school programs in the U.S. Students in second through sixth grade sign up for the program, and the library arranges for a volunteer to meet with the student at the library once a week at a specific time for between 60 and 90 minutes.
 
Last year about 347 children participated in the homework clubs and Leading to Reading programs, said Cathy Thompson, east region coordinator for the Leading to Reading and Homework Help programs of the Toronto Public Library.
 
This year, the library started a homework program for teenagers, because so many who had participated as elementary students came back seeking help, said Joanne Hawthorne a specialist in children and teen services for the Toronto Public Library.
 
Teen clubs started this year in six branches, and also involve volunteers doing one-on-one tutoring, Hawthorne told Education World. While originally aimed at high school students, some clubs have been opened up to seventh and eighth graders, she said.
 
"Anecdotally, we've heard positive things, but we haven't done any follow-up studies," said Russo. "Classroom teachers report that more homework assignments are being done when kids go to homework club. So far it has been a very positive experience, and well-worth the investment."
 
Toronto library staff members have seen homework club students make big gains, Kondo said. "In some cases, the results have been spectacular," he told Education World. "Certainly, a student could go from a C to a B. The fact that the volunteer sees the same child week-to-week means they get used to each other. And any time a child can get individual help, it is great."
 
Starting a homework club can be beneficial for all involved. The children who attend the club can work together and learn from each other, while the adults (usually parents) who are involved can see where the children are struggling and learn more about the kind of learner each child is. Some schools already offer a homework club to help children study after school, but parents can also start a homework club in their community. If you are interested in starting a homework club for your children, first set some rules and regulations. Select a time and place to hold the club and then let others know about it.
 
Starting a homework club is one way that parents can help their children study. Parents who are considering starting a homework club should set initial rules and regulations for the club, select a time and place for the club and then let others know about the club.
 72ea393242
 
 
