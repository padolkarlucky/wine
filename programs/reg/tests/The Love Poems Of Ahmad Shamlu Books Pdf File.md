## The Love Poems Of Ahmad Shamlu Books Pdf File

 
  
 
**CLICK HERE ✔ [https://kneedacexbrew.blogspot.com/?d=2tCuOF](https://kneedacexbrew.blogspot.com/?d=2tCuOF)**

 
 
 
 
 
# The Love Poems Of Ahmad Shamlu: A Treasure Of Persian Poetry
  
If you are looking for a collection of beautiful and inspiring poems in Persian, you may want to check out **The Love Poems Of Ahmad Shamlu**, a book that features some of the best works of one of Iran's most influential poets. Ahmad Shamlu (1925-2000) was a prominent figure in the modern Persian literature, who wrote poems that touched on various themes such as love, freedom, justice, and resistance. He was also a translator, journalist, and editor, who contributed to the cultural and intellectual life of Iran.
  
In this book, you will find a selection of Shamlu's love poems, translated into English by Firoozeh Papan-Matin, a professor of Persian language and literature. The poems are arranged in chronological order, from his early works in the 1940s to his later ones in the 1990s. You will be able to witness the evolution of his poetic style and voice, as well as his personal and political experiences. The poems are accompanied by an introduction that provides some background information on Shamlu's life and work, as well as a glossary that explains some of the cultural and historical references in the poems.
  
The Love Poems Of Ahmad Shamlu is a book that will appeal to anyone who loves poetry, especially those who are interested in exploring the rich and diverse tradition of Persian poetry. You will be moved by Shamlu's lyrical and expressive language, his vivid imagery, his emotional depth, and his universal message of love. You will also discover a new perspective on Iran's history and culture, through the eyes of one of its most celebrated poets.
  
You can download **The Love Poems Of Ahmad Shamlu** as a PDF file from various online sources[^2^] [^3^], or you can order a paperback copy from Amazon[^2^]. Whether you read it on your computer or on paper, you will surely enjoy this book of poetry that showcases the beauty and power of Persian language and literature.
  
One of the most remarkable features of Shamlu's love poems is his use of imagery and symbolism. He often draws inspiration from nature, such as the sun, the moon, the stars, the sea, the wind, the flowers, and the birds. He also uses colors and their variations to create different moods and atmospheres. For example, in his poem "Tell Your Most Beautiful Words", he writes:

> Tell your most beautiful words
 Release your pain of silence
 Don't be scared if they say
 Your words are useless
 Because our words
 Are not useless melodies

In this poem, he encourages his beloved to express her feelings and thoughts, and not to be afraid of criticism or rejection. He assures her that their words are meaningful and valuable, and that they can create a harmony between them.
  
Another characteristic of Shamlu's love poems is his use of repetition and variation. He often repeats certain words or phrases, with slight changes or additions, to emphasize his point or to create a rhythm. For example, in his poem "In This Dead-End", he writes:

> They smell your mouth
 To find out if you have told someone: I love you!
 They smell your heart!
 Such a strange time it is, my darling.
 And they punish love
 By flogging at the road-cross,
 We must hide our love in dark closets.

In this poem, he denounces the oppression and violence that he and his beloved face in their society. He repeats the word "they" to show the contrast between them and the authorities who try to control their lives. He also repeats the phrase "smell your" to show how intrusive and absurd their actions are. He then adds different words after each repetition, such as "mouth", "heart", and "love", to show how they target different aspects of their identity and emotions.
  
Shamlu's love poems are not only expressions of his personal feelings, but also reflections of his social and political views. He often criticizes the injustice and corruption that he witnesses in his country, and calls for freedom and democracy. He also shows solidarity with the oppressed and marginalized people, such as the workers, the peasants, the women, and the minorities. He believes that love is a powerful force that can inspire people to resist tyranny and to create a better world.
 842aa4382a
 
 
