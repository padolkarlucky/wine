## Starcraft And Broodwar Cheat Codes

 
 ![Starcraft And Broodwar Cheat Codes](https://3.bp.blogspot.com/_tYzITWQO60w/TFbi2P-689I/AAAAAAAAAAw/H0N8ddD6PaI/s1600/cheats.PNG)
 
 
**Download &gt;&gt;&gt; [https://lodystiri.blogspot.com/?file=2tAW3Q](https://lodystiri.blogspot.com/?file=2tAW3Q)**

 
 
 
 
 
# Starcraft and Broodwar Cheat Codes: A Complete Guide
 
Starcraft and Broodwar are classic real-time strategy games that have captivated millions of players around the world. Whether you want to relive the epic campaigns, challenge your friends online, or just have some fun with the game mechanics, you might be interested in using some cheat codes to spice things up.
 
Cheat codes are special commands that you can enter during gameplay to activate various effects, such as getting more resources, upgrading your units, revealing the map, or making your units invincible. However, using cheat codes will disable achievements and prevent you from saving your progress, so use them at your own risk.
 
In this article, we will show you how to use cheat codes in Starcraft and Broodwar, and provide you with a complete list of all the available codes for both games. Let's get started!
 
## How to Use Cheat Codes in Starcraft and Broodwar
 
Using cheat codes in Starcraft and Broodwar is very simple. All you need to do is press the Enter key during gameplay to bring up a message prompt. Then, type in the cheat code of your choice and press Enter again to activate it. You will see a confirmation message on the screen if the code is valid.
 
You can use cheat codes as many times as you want, but some of them will affect both you and your enemies, so be careful. You can also deactivate some cheat codes by typing them again. To disable all cheat codes at once, type "power overwhelming" (without quotation marks) and press Enter.
 
Note that cheat codes only work in single-player mode, and not in multiplayer or custom games. Also, cheat codes are case-sensitive, so make sure you type them exactly as they appear.
 
## List of Cheat Codes for Starcraft and Broodwar
 
Here is a complete list of all the cheat codes for Starcraft and Broodwar, along with their effects and sources. Some cheat codes are exclusive to either Starcraft or Broodwar, while others work for both games.

| Cheat Code | Effect | Source |
| --- | --- | --- |

| show me the money | Gives you 10,000 minerals and gas | [^1^] [^2^] |

| whats mine is mine | Gives you 500 minerals | [^1^] [^2^] |

| breathe deep | Gives you 500 gas | [^1^] [^2^] |

| something for nothing | Gives you free upgrades (type three times for level 3) | [^1^] [^2^] |

| medieval man | Gives you all research abilities | [^1^] [^2^] |

| modify the phase variance | Lets you build anything without prerequisites | [^1^] [^2^] |

| food for thought | Lets you build over the supply limit | [^1^] [^2^] |

| operation cwal | Makes building and upgrading faster (also affects enemies) | [^1^] [^2^] |

| the gathering | Gives infinite energy to your units (also affects enemies) | [^1^] [^2^] |

| power overwhelming | Makes your units invincible (also affects enemies) | [^1^] [^2^] |

| noglues | Disables enemy psionic abilities (only works in Broodwar) | [^1^] [^2^] |

| black sheep wall | 842aa4382a
<br>
<br>
 |