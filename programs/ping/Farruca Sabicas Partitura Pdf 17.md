## Farruca Sabicas Partitura Pdf 17

 
  
 
**Download File 🆓 [https://vercupalo.blogspot.com/?d=2tCwN6](https://vercupalo.blogspot.com/?d=2tCwN6)**

 
 
 
 
 
# How to Play Farruca by Sabicas - A Guide for Flamenco Guitarists
  
Farruca is a traditional Spanish folk dance that originated in Galicia and was adopted by flamenco artists. It is characterized by a slow tempo, a minor key, and a rhythmic pattern of 4/4. Farruca is often played as a solo guitar piece or as a duet with another guitarist or a dancer.
  
One of the most famous and influential flamenco guitarists of all time, Sabicas, composed and recorded several versions of Farruca, showcasing his virtuosity and creativity. His Farruca is considered a masterpiece of flamenco guitar music, with its flashy music and snappy riffs and its mournful melodies and beautiful harmonies.
  
In this article, we will show you how to play Farruca by Sabicas, based on his original version from 1925. We will provide you with the sheet music, the tablature, and some tips and tricks to help you master this challenging but rewarding piece.
  
## What You Need to Play Farruca by Sabicas
  
To play Farruca by Sabicas, you will need:
  
- A flamenco guitar or a classical guitar with nylon strings.
- A capo on the second fret.
- A metronome or a backing track to practice with.
- A good understanding of flamenco techniques such as rasgueado, picado, alzapÃºa, tremolo, and golpe.
- A lot of patience and perseverance.

## How to Play Farruca by Sabicas - Step by Step
  
Farruca by Sabicas consists of four main sections: an introduction, a theme, a variation, and an ending. Each section has its own melody and chord progression, but they are all based on the same scale: the E Phrygian mode. The E Phrygian mode is similar to the E minor scale, but with a flattened second degree. It gives the piece a dark and exotic sound.
  
The introduction is a short and simple section that sets the mood and introduces the rhythm of the piece. It starts with a descending chromatic run on the bass strings, followed by some chords and arpeggios. The introduction ends with a cadence that leads to the theme.
  
The theme is the main melody of the piece. It is played twice, with some variations in the second time. The theme is composed of four phrases, each one ending with a different chord. The first phrase ends with an E minor chord, the second phrase ends with an A minor chord, the third phrase ends with a B major chord, and the fourth phrase ends with an E major chord. The theme is played mostly on the treble strings, using picado and alzapÃºa techniques.
  
The variation is a more complex and elaborate section that develops the theme. It is also played twice, with some variations in the second time. The variation is composed of eight phrases, each one ending with a different chord. The first four phrases are similar to the theme, but with more notes and ornaments. The fifth phrase ends with an F major chord, the sixth phrase ends with an A minor chord, the seventh phrase ends with an E major chord, and the eighth phrase ends with an E minor chord. The variation is played mostly on the bass strings, using rasgueado and tremolo techniques.
  
The ending is a short and fast section that concludes the piece. It starts with a rapid ascending run on the treble strings, followed by some chords and arpeggios. The ending ends with a final E major chord that is accented with a golpe.
  
## Farruca by Sabicas - Sheet Music and Tablature
  
Here is the sheet music and tablature for Farruca by Sabicas. You can download it as a PDF file from this link: [Farruca Sabicas Partitura Pdf 17](https://example.com/farruca_sabicas_partitura_pdf_17.pdf).
  ![Farruca Sabicas Partitura Pdf 17](https://example.com/farruca_sabicas_partitura_pdf_17.png)  
## Farruca by Sabicas 842aa4382a




