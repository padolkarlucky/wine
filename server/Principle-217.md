## Principle 2.1.7

 
 ![Principle 2.1.7](https://johnmbizz.weebly.com/uploads/3/7/1/5/37156819/8983441_orig.jpg)
 
 
**## Files you can download:
[Link 1](https://vittuv.com/2tDRN3)

[Link 2](https://miimms.com/2tDRN3)

[Link 3](https://gohhs.com/2tDRN3)

**

 
 
 
 
 
On January 1st, 2022, the new Chapter 2.1.7, published by the European Pharmacopoeia (Ph.Eur.) and pertaining to balances used for analytical purposes, became effective. One of the highlighted features describes requirements for checking instruments between calibrations (during routine use). Performance checks include verification of repeatability and sensitivity, two critical parameters for ensuring proper functioning of analytical balances.
 
The Ph.Eur. is the legally binding reference for all companies placing pharmaceutical products in European member states. As such, the new chapter applies to any producers of medicines and/or substances for pharmaceutical use exported into the European market, regardless of location.  
  
Please check our FAQs about Chapter 2.1.7 and learn how you can ensure adherence to the updated European Pharmacopoeia guidelines. Be sure to download the full set of FAQs for an in-depth discussion of Chapter 2.1.7.
 
Our USP Advanced QApp is used to check the performance of balances and offers a guided workflow to determine minimum sample weight acc. to USP Chapter 41 or the new Ph.Eur. Chapter 2.1.7. Our Minimum Weight application monitors weight determined by the USP Advanced QApp and checks it against permitted ranges as per different guidelines. These applications can be paired with add-on advanced software programs for determining and monitoring the measurement uncertainty.
 
On July 1, 2021, the new European Pharmacopoeia (Ph.Eur.) Chapter 2.1.7 "Balances for Analytical Purposes" will be published and, after a transition period of six months, become mandatory on January 1, 2022 for pharmaceutical companies manufacturing or exporting into the European market.  
  
The new chapter describes principles for the use of balances used in analytical procedures including calibration and performance checks to maintain performance.  
  
Our new white paper summarizes the concepts of the well-known USP Chapters and and the new European Pharmacopoeia (Ph.Eur.) Chapter 2.1.7.
 
Since earliest times, human societies have pondered the nature of ethics and its requirements and have sought illumination on ethical questions in the writings of philosophers, novelists, poets and sages, in the teaching of religions, and in everyday individual thinking. Reflection on the ethical dimensions of medical research, in particular, has a long history, reaching back to classical Greece and beyond. Practitioners of human research in many other fields have also long reflected upon the ethical questions raised by what they do. There has, however, been increased attention to ethical reflection about human research since the Second World War. The judgment of the Nuremberg military tribunal included ten principles about permissible medical experiments, since referred to as the Nuremberg Code. Discussion of these principles led the World Medical Assembly in 1964 to adopt what came to be known as the Helsinki Declaration, revised several times since then. The various international human rights instruments that have also emerged since the Second World War emphasise the importance of protecting human beings in many spheres of community life. During this period, written ethical guidelines have also been generated in many areas of research practice as an expression of professional responsibility.
 
In addition to this National Statement, the Australian code for the responsible conduct of research, 2018 (the 'Research Code') has an essential role in promoting good research governance. The Research Code sets down the broad principles of responsible and accountable research practice, and identifies the responsibilities of institutions and researchers in areas such as data and record management, publication of findings, authorship, conflict of interest, supervision of students and research trainees, and the handling of allegations of research misconduct.
 
However, the values of respect, research merit and integrity, justice, and beneficence have become prominent in the ethics of human research in the past six decades, and they provide a substantial and flexible framework for principles to guide the design, review and conduct of such research. This National Statement is organised around these values, and the principles set out in paragraphs 1.1 to 1.13 give them practical expression.
 
These ethical guidelines are not simply a set of rules. Their application should not be mechanical. It always requires, from each individual, deliberation on the values and principles, exercise of judgement, and an appreciation of context.
 
2.1.7 Research is 'negligible risk ' where there is no foreseeable risk of harm or discomfort; and any foreseeable risk is no more than inconvenience. Where the risk, even if unlikely, is more than inconvenience, the research is not negligible risk.
 
2.2.1 The guiding principle for researchers is that a person's decision to participate in research is to be voluntary, and based on sufficient information and adequate understanding of both the proposed research and the implications of participation in it. For qualifications of this principle, see Chapter 2.3: Qualifying or waiving conditions for consent.
 
a) involvement in the research carries no more than low risk (see paragraphs 2.1.6 and 2.1.7, page 18) to participants b) the benefits from the research justify any risks of harm associated with not seeking consent c) it is impracticable to obtain consent (for example, due to the quantity, age or accessibility of records) d) there is no known or likely reason for thinking that participants would not have consented if they had been asked e) there is sufficient protection of their privacy f) there is an adequate plan to protect the confidentiality of data g) in case the results have significance for the participants' welfare there is, where practicable, a plan for making information arising from the research available to them (for example, via a disease-specific website or regional news media) h) the possibility of commercial exploitation of derivatives of the data or tissue will not deprive the participants of any financial benefits to which they would be entitled i) the waiver is not prohibited by State, federal, or international law.
 
The guidance in this section identifies common ethical issues that arise in the various phases of research. It is up to each researcher and HREC to apply the guidance to each project, taking account of the four principles of research merit and integrity, justice, beneficence and respect. This guidance facilitates consideration of the risks and benefits of the research and the level of ethical oversight required.
 
Human research projects must adhere to the core ethical principles described in Section 1 of this National Statement. These principles apply at all stages of a research project from inception to post-completion.
 
This chapter discusses the manner in which the core principles of this National Statement should be reflected in the elements of research project design. The chapter should be considered as a whole, however, the order in which these elements are discussed does not imply a hierarchy or a sequence, or that all of these elements will have equal relevance in every design. The elements are:
 
A single project may employ more than one recruitment strategy, especially where discrete cohorts are required to meet the objectives of the research. For some research designs, the recruitment and consent strategies occur concurrently; for others, they are separate. It is essential that recruitment strategies adhere to the ethical principles of justice and respect.
 
Human research projects incorporate one or more methods to generate, collect, or access data or information so as to achieve the objectives of the research. Collection, use and management of data and information must be in accordance with the ethical principles discussed in Section 1 of this National Statement.
 
Research may involve access to large volumes of data or information not explicitly generated for research purposes. The size and accessibility of such sources make them attractive for some research designs, the use of which may raise difficult privacy and consent questions. However, because research using populationwide datasets is inclusive of all members of the population in question, it promotes the core principle of justice. In addition, benefits and burdens may be spread more evenly than research based on selected participants.
 
Data or information available on the internet can range from information that is fully in the public domain (such as books, newspapers and journal articles), to information that is public, but where individuals who have made it public may consider it to be private, to information that is fully private in character. The guiding principle for researchers is that, although data or information may be publicly available, this does not automatically mean that the individuals with whom this data or information is associated have necessarily granted permission for its use in research. Therefore, use of such information will need to be considered in the context of the need for consent or the waiver of the requirement for consent by a reviewing body and the risks associated with the use of this information.
 
Providing research findings or results to participants can be a benefit, but it can also be a source of risk (for example, psychological, social, legal). The approach taken to communicating findings and results should reflect principles of good science and adhere to the ethical principles of justice, respect and beneficence discussed in Sectio