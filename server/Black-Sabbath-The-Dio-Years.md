## Black Sabbath - The Dio Years

 
 ![Black Sabbath - The Dio Years](https://cdn.shopify.com/s/files/1/0447/6964/0599/products/MyPost_6_600x.jpg?v=1635553435)
 
 
**CLICK HERE ↔ [https://urllio.com/2tH8nE](https://urllio.com/2tH8nE)**

 
 
 
 
 
The original lineup of Black Sabbath possesses such a mythic quality that it's easy to overlook how far they slid by the time Ozzy Osbourne up and left the band...or how far they rebounded after they hired Rainbow singer Ronnie James Dio as his replacement. Countless compilations over the years have preserved the initial part of the story line -- celebrating the innovations of the first four albums with a near fetishistic quality -- but there has never been a good retrospective concerning the Dio years until Rhino released the aptly titled The Dio Years in early 2007. True, the Dio years didn't last all that long -- the singer joined in 1980 for Heaven & Hell, then lasted through one more studio album, the following year's Mob Rules, before departing under a shroud of controversy after 1982's botched live album Live Evil -- but Dio had a powerful impact upon the band and its legacy; these were the last years that Sabbath exerted pull as an active band, and after his departure they stumbled through various singers over the next decade before intermittently reuniting with Ozzy in the '90s. The Dio Years proves that during his brief time with the band, Dio did help Sabbath make music that could hold its own with some of the classic lineup's finest moments. With Dio as a frontman, the band was harder, nastier, and a little faster than the slow sludge of the early Sabbath records, but it fit in nicely with the New Wave of British Heavy Metal at the beginning of the '80s and it's aged very well. Some of it can sound silly -- Dio's lyrical obsessions always do -- but this is harder, heavier, better music than either Technical Ecstasy or Never Say Die! Anybody who's refused to give this latter-day incarnation of the band the time of day might find this compilation revelatory.
 
Tony Iommi is a talkative mood these days. With Black Sabbath set to revisit the band's Dio's years with deluxe reissues of the albums **Heaven and Hell** and **Mob Rules**, the left-handed guitar legend had a few things to say about those heavy metal times.
 
Few debates make me as nauseated as the one concerning the worthiness of the **Ozzy Osbourne** versus **Ronnie Jame Dio BLACK SABBATH** lineups. While it is true (in my mind) that the **Ozzy**-era lineup cannot be touched from the standpoint of musical trailblazing and the writing of legendary tunes, to say that the **Dio** years (specifically 1981-1983) were somehow sub par or unremarkable is just silly. The music created by each lineup should be evaluated on its own merits. The main point of commonality is that both lineups can boast producing some of the greatest heavy metal songs of all time.
 
Song-wise, the **"Heaven and Hell"** material commences with the up-tempo **"Neon Knights"**, an absolute killer! And who doesn't love **Geezer Butler**'s opening bass line on **"Lady Evil"**? The explosion of speed and fury that occurs after **Geoff Nichols**' keyboard intro subsides on **"Die Young"** is amazingly powerful. The tune rages with speed and fury until the sparking keyboards and **Dio**'s soft vocal section on the chorus hits. The tune then blasts off again and **Iommi** slays with a solo. The recognizable riff on the title track approaches the legendary status of **"Iron Man"** and **"Smoke on the Water"** (I know, not quite, but you get the gist) and the epic song is an all-time great, hands down. **"Lonely is the Word"** closes the **"Heaven and Hell"** album with grace and beauty, a different kind of heaviness defining the slow tune. The version of **"Children of the Sea"** from **"Live Evil"** was the best choice for a live cut, as it comes off particularly well and stands as another of the **Dio** years' greatest contributions to metal. The mid-section bass playing against **Iommi**'s melodic leads (and eventually brief and ripping solo) and **Nichols**' choral keyboard effect is stunning.
 
BLABBERMOUTH.NET uses the Facebook Comments plugin to let people comment on content on the site using their Facebook account. The comments reside on Facebook servers and are not stored on BLABBERMOUTH.NET. To comment on a BLABBERMOUTH.NET story or review, you must be logged in to an active personal account on Facebook. Once you're logged in, you will be able to comment. User comments or postings do not reflect the viewpoint of BLABBERMOUTH.NET and BLABBERMOUTH.NET does not endorse, or guarantee the accuracy of, any user comment. To report spam or any abusive, obscene, defamatory, racist, homophobic or threatening comments, or anything that may violate any applicable laws, use the **"Report to Facebook"** and **"Mark as spam"** links that appear next to the comments themselves. To do so, click the downward arrow on the top-right corner of the Facebook comment (the arrow is invisible until you roll over it) and select the appropriate action. You can also send an e-mail to blabbermouthinbox(@)gmail.com with pertinent details. BLABBERMOUTH.NET reserves the right to "hide" comments that may be considered offensive, illegal or inappropriate and to "ban" users that violate the site's Terms Of Service. Hidden comments will still appear to the user and to the user's Facebook friends. If a new comment is published from a "banned" user or contains a blacklisted word, this comment will automatically have limited visibility (the "banned" user's comments will only be visible to the user and the user's Facebook friends).
 
"Putting to bed the longstanding rumors of a reunion tour, **Heaven and Hell** have announced their first tour dates for 2007. The tour will feature 11 Canadian shows, along with an exciting one-night-only performance at one of the most prestigious venues in the world, New York's Radio City Music Hall. Heaven and Hell reunites **Ronnie James Dio**, **Tony Iommi**, **Geezer Butler** and **Vinny Appice** for the first time in 15 years." Presale here. Regular sale here. Tour Dates below...

**Heaven & Hell | 2007 Tour DAtes**
Mar 11 Pacific Coliseum (Vancouver, BC Canada)
Mar 13 Rexall Place (Edmonton, AB)
Mar 14 Pengrowth Saddledome (Calgary, AB)
Mar 16 Brandt Centre (Regina, SK)
Mar 18 MTS Centre (Winnipeg, MB)
Mar 20 Steelback Centre (Sault Ste. Marie, ON)
Mar 22 Air Canada Centre (Toronto, ON)
Mar 24 John Labatt Centre (London, ON)
Mar 26 Bell Centre (Montreal, QC)
Mar 27 Colisee Pepsi Arena (Québec, QC)
Mar 28 Civic Centre (Ottawa, ON)
Mar 30 Radio City Music Hall (New York, NY)
 
Available in three amazing editions including two personally signed by **Tony Iommi, Geezer Butler, Bill Ward, Vinnie Appice and Wendy Dio.**

**The Standard Edition**
Measuring 300mm square with a printed hardback cover and supplied in a black cloth slipcase. £99 each.
 
Though Dio is thrilled to be able to revisit his past with Sabbath, he hasn't lain awake at night longing for a reunion. And while Iommi and Butler have wondered over the years what it would be like to play again with Dio, it was Warner Music subsidiary Rhino Records that first brought up the idea by making plans to release *The Dio Years.* The label contacted Iommi's manager and said it would like to include some previously unreleased songs. Since there was nothing worthwhile in the vault that hadn't been used, Iommi contacted Dio and suggested they consider getting together to write some new material.
 
"It was wonderful to work with Vinnie again," Dio said. "Even though I hadn't seen him in many years, we got along as if we'd never been apart because we know each other so well from all those years of being together."
 72ea393242
 
 
