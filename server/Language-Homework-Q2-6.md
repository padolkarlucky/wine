## Language Homework Q2 6

 
  
 
**LINK ->>> [https://shurll.com/2tDNyG](https://shurll.com/2tDNyG)**

 
 
 
 
 
Homework must be submitted when due. You loose 10%, one grade, the first day homework is late. Then 10% more per week, each week homework is late. Max of 50% off. A ZERO really hurts your final points. Do and turn in all homework, even if it is late. See HW1 for submit details. (no late penalty for online courses)
 
**Official summary (sightly modified):**Presents a comparative approach to object-orientedprogramming and design, with an emphasis on techniques for programminglarge-scale systems. Discusses the concepts of object, class,meta-class, message, method, inheritance, and genericity. Reviewsforms of polymorphism in object-oriented languages. Contrasts the useof inheritance and composition as dual techniques for software reuse:forwarding versus delegation and sub-classing versussub-typing. Studies main programming language tools and techniques to facilitate the creation of applications: modularity, encapsulation,code reuse techniques, design patterns. Basic concepts inobject-oriented design are illustrated by writing programs in one ormore object-oriented languages.
 
**Fri, 17 Dec 2010:** Oops. Bug in my implementation of rooms for homework 6. You only ever notice it if an AutonomousPerson moves into a room with no exits. The check for an empty list of exits when picking an exit at random from a room is broken. Method randomExit() in Room should read: def randomExit ():Option[Exit] =  if (theExits.isEmpty()) Option.none() else Option.some(theExits.nth(Util.random(theExits.length())))
 
**Mon, 13 Dec 2010:** I've uploaded the homework 5 grades on Blackboard. Please have a look at your full set of grades and let me know if anything doesn't agree with your records. In particular, if you received a 0 (usually indicating no submission) for a homework that you're sure you submitted, please inquire. There are a couple of homeworksthat I have received out of band and that are in Nick's hands,so those grades should be updated in the coming days.
 
**Fri, 10 Dec 2010:** The submission siteis open for homeworks 6.As I mentioned multiple times, I will accept submissions until Dec 17. Please name your submission adventure.zip, and don't forget to include a text file called README.txt describing your team, your extensions, and the classes you modified and how. Please *please* don't put it in a Word file (or anything else annoyingly proprietary.)
 
**Thu, 02 Dec 2010:** Homework 6 is out. I will accept submissions for this homework until Friday, December 17th, the day after the exam. (Extra day because the homework came out a day after the days I said I would make it available. Only fair.) Source code for the homework is in the Homeworks section of the web site, as usual. Look at it, and familiarize yourself with the code. There's a lot in there for you to look at.
 
**Mon, 08 Nov 2010:** A few of you have requested a sample solution for the last question of homework 3, where I asked you to add a stream() operation to atlases that returns a stream of the rooms in the atlases. Here is a sample solution, where I add stream() to the implementation of atlases from homework 2, as opposed to a binary-search-tree implementation. The subtlety for this question was to realize that I wasn't asking you to make atlases themselves support the stream operations, but rather require you to invoke stream() on an atlas to get a stream out of it. That's an alternative way to provide a stream interface that has some advantages. (Of course, there are ways of answering the question correctly and *still* have atlases directly support the stream operations... Think about it if you want.) The way I implemented it, I added two new (private) classes to the implementation to support the stream operations.
 
**Fri, 29 Oct 2010:** In an uncharacteristic display of efficiency and proactiveness, the university cleaning crew quite literally garbage collected the box of homeworks 1 & 2 I left outside my office overnight. Meaning that if you have not picked up your homework 1 or 2 and would like to do so, you'll have to drop me an email...
 
**Thu, 28 Oct 2010:** Made a couple more corrections on homework 3 following feedback frompeople trying to test their Atlas implementation with thetester from homework 2. (Which you should do too.) Basically added theroomInFocus() method back in the ADT. Also changed theStream trait from Question 3 to match what we saw inclass. Sorry I didn't see the discrepancy earlier:
 
**Mon, 25 Oct 2010:** I've had a few questions on homework 3 at office hours earlier, theanswer to which may be helpful to everyone. The main one if that forQuestion 2, I'm asking you to change the representation of atlases youimplemented in homework 2. There, you had to use the representation ofatlases that came out of the specification, thus, three concreteimplementation classes corresponding to the three creators. For this homework, youcan ditch those three concrete classes, and replaced them by somethingelse. What that something else is is completely up to you, as you longas it implements something like a binary search tree. You still needto have those three creators for Atlas, of course, since thecreators are part of the signature I'm asking you to implement, butthose creators will need to instantiate and/or do something with yournew concrete implementation e classes. You may also benefit from having helper methodsin your concrete implementation classes (I had two in my implementation).
 
For question 3, I've added a sample solution to Atlas fromhomework 2 to the Homeworkssections of the website that you can use if you get stuck on question2. I've also tossed in solutions to OString andRoom, even though most of you got those right.
 
**Sat, 16 Oct 2010:** Homework 3 is out. Due Wednesday 27 Oct, at 22h00. Helper files are provided in the Homeworks section of the web site. I will add a solution to homework 2 that you can use when you get to Question 2 and 3, but you can use yours too. Testers will come later.
 
**Thu, 14 Oct 2010:** Lecture notes from last week's lecture on information hiding are now available. They may come in handy if you want to hide helper functions in your homework and forgot how I did it in class and could not reconstruct it.
 
**Wed, 06 Oct 2010:** Some of you have spotted the inconsistency in addExit() for question 2 of homework 2. The signature is correct, it's the specification that's wrong! I've updated the homework to the correct specification, so please make sure you read the new version.
 
**Wed, 06 Oct 2010:** I've added a copy of my sample solution Direction.scala for homework1 to the Homeworks section of the web site, if you want to use it for homework2. Feel free to use yours, though -- as long is passed all the tests.
 
**Sun, 03 Oct 2010:** Homework 2 is out. Because it came out two days late, it will be due two days later than originally planned, so Thursday 14 Oct, at 22h00. I will give you a pristine copy of Direction.scala implementing the code from homework 1, as well as some testing code in the coming days.
 
**Wed, 29 Sep 2010:** Submission instructions have been emailed to your husky.neu.edu accounts, including a unique token that you can use to set your initial password. Once you have your password, you can submit homeworks. The submission system can be found at ~riccardo/cs3500/. Please get in touch with me if there are problems. You can submit multiple times, and we will generally only look at the last thing you submitted by the submission deadline. Note that the submission system will gsend you back an email confirmation of submission, including the file(s) you submitted. Please look at them to make sure that what you submitted was what you thought you submitted. In particular, submittiing .class files instead of .scala files is a common error that will probably get you zero.
 
**Mon, 27 Sep 2010:** Okay, automatic extension for homework 1 until *Wednesday, September 29, 2010* -- so one extra day -- just so I can get my act together and work out the kinks out of the submission system.
 
**Sun, 26 Sep 2010:** A common question: do you have to provide any code for Q2 of homework 1? Answer: NO. The only thing I want you to do for Q2 is to give the equations you need to add to the specification to account for the new creators and operations. Also: I'm still in the process of figuring out submission instructions. Stay tuned...
 
**Sun, 26 Sep 2010:** I'm providing a tester for your Direction ADT in homework 1, DirectionTester.scala. Just compile it with your Direction.scala file, and run it. (It has a main() method.) Let me know if anything's wrong.
 
**Sun, 26 Sep 2010:** I was reminded that I did not cover equality on Friday, making answering the question of how to implement equals() on homework 1 a bit of a problem. Here's the template for writing equals() in Scala: override def equals (other: Any) = other match  case that : Direction => /\* case where 'other' is a Direction, and you use 'that' to access the argument of equals() \*as\* a Direction, e.g., use that.isNorth()... \*/ case \_ => /\* case where 'other' is not a Direction \*/
 
**Textbooks:** No textbook for the course. I will post lecture notes here, and give you pointers to online documents. You may find the following book useful, although it is *not* required, and we won't actually follow it:

- C. Hostermann, *Object-Oriented Design and Patterns*, 2nd edition, John Wiley and Sons, 2006

We will use the Scala programming language, which is a variant of Java developed in Switzerland. There are a few books on Scala available, and a fair amount of information online, that I will pass along to you. We will also use and refer to Java at times. The quality of a programming book is usually inversely proportional to its size, and my favorite Java book is a slim volume des