## Guillermo Floris Margadant El Derecho Privado Romano Pdf 27

 
 ![Guillermo Floris Margadant El Derecho Privado Romano Pdf 27](https://bibliotecas.uchile.cl/documentos/2005716-919c495184.jpg)
 
 
**Click Here ……… [https://vittuv.com/2tylvE](https://vittuv.com/2tylvE)**

 
 
 
 
 
# Guillermo Floris Margadant: A Renowned Scholar of Roman Private Law
 
Guillermo Floris Margadant was a Dutch-Mexican jurist, historian and professor who dedicated his life to the study of Roman law and its influence on modern legal systems. He was born in The Hague in 1924 and moved to Mexico in 1949, where he became a naturalized citizen and a distinguished member of the National Autonomous University of Mexico (UNAM). He wrote several books and articles on Roman law, especially on private law, which is the branch of law that regulates the relations between individuals and legal entities.
 
One of his most notable works is *Derecho Romano: El Derecho Privado Romano* (Roman Law: Roman Private Law), published in 2010 by Editorial Esfinge. In this book, he offers a comprehensive and systematic analysis of the main aspects of Roman private law, such as persons, family, property, obligations, contracts, succession and procedure. He also examines the historical development of Roman law from its origins to its reception in medieval and modern Europe and Latin America. He illustrates his exposition with examples, cases and references to classical sources and modern scholarship.
 
The book is divided into 27 chapters, each covering a specific topic of Roman private law. The chapters are organized in four parts: Part I deals with the general principles and sources of Roman law; Part II covers the law of persons and family; Part III focuses on the law of property and obligations; and Part IV discusses the law of succession and procedure. The book also includes an introduction, a bibliography, an index of names and an index of subjects.
 
Guillermo Floris Margadant was a respected authority on Roman law and a prolific writer who contributed to the diffusion and understanding of this ancient legal system. He died in Mexico City in 2002 at the age of 78. His legacy remains as an inspiration for students and scholars of Roman law and legal history.
  
One of the main features of Roman private law is its flexibility and adaptability to different social and historical contexts. Roman law was not a rigid and static system, but a dynamic and evolving one that responded to the needs and challenges of a diverse and complex society. Roman law was also influenced by other legal traditions, such as the Greek, the Oriental and the Germanic ones, creating a rich and varied legal culture.
 
Another characteristic of Roman private law is its rationality and coherence. Roman law was based on logical and consistent principles that aimed to achieve justice and equity in human relations. Roman law also developed a sophisticated and refined legal technique that allowed for the creation and interpretation of legal norms and institutions. Roman law was not only a practical tool for solving disputes, but also a source of legal science and philosophy.
 
A third aspect of Roman private law is its universality and permanence. Roman law was not limited to the territory and time of the Roman Empire, but extended to other regions and periods through its reception and transmission by different legal systems. Roman law also provided the foundations and models for many modern legal concepts and institutions, such as human rights, civil law, contract law, property law and procedural law. Roman law is still relevant and influential in today's world, as it represents a common heritage and a shared language for lawyers and jurists.
 842aa4382a
 
 
