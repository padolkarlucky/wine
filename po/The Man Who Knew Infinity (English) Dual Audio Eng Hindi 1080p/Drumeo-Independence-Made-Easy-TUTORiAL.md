## Drumeo Independence Made Easy TUTORiAL

 
  
 
**## Links to get files:
[Link 1](https://urlca.com/2tDZ4F)

[Link 2](https://urluso.com/2tDZ4F)

[Link 3](https://tweeat.com/2tDZ4F)

**

 
 
 
 
 
**Getting Started**
The 'Getting Started' Module is the perfect introduction to BDS 2.0. You will get to know your three instructors: Jared, Sean, and Dave - while learning essential bass drum fundamentals. Lessons include: choosing a pedal, drum tuning and muffling, bass drum pedal settings, bass drum pedal adjustments, seat height & posture, bass drum setup tips, heel up technique, heel down technique, flat foot technique, and a few other great tips to help get you started.

**Bass Drum Independence**
Bass Drum Independence is something that a lot of drummers struggle with. Separating your hands from your feet and your right foot from your left foot is no easy task. That's where this module hits the mark. It includes beginner, intermediate, and advanced hand patterns that are designed specifically to take your bass drum independence to the next level. Then, once you have completed the module, you can apply your new skills with some of the included drum play-along songs.

**Heel-Toe Technique**
The heel-toe technique has always been somewhat of a mystery to many drummers. When taught correctly, it can be learned in a few short minutes. However, without proper instruction, it can easily take days or even weeks of trial-and-error to get it working. This module simplifies the process and teaches you how to play the heel-toe technique with step-by-step examples. You will learn both how to master the technique, and how to apply it within your drumming.

**Heel-Toe Exercises**
Once you have developed the basic heel-toe technique, you'll want to start incorporating it within practical beats and fills. This fourth module covers beginner, intermediate, and advanced drumming patterns that make specific use of this unique technique. These examples will help you get started with incorporating double strokes in beats, and will leave you well prepared for creating your own unique heel-toe-based beats and fills in the future!

**Slide Technique**
This module covers the slide technique - a popular alternative to the heel-toe method. Dave Atkinson walks you through the basic fundamentals, including: correct footwear and pedal settings, how to develop the technique, how to develop the technique for your left foot, and how to apply the technique with drum beats and drum fills. The slide technique can be used in many different styles of music and is a natural way to play quick double strokes!

**Slide Technique Exercises**
In Module 6, you'll get a chance to apply the slide technique within practical beats and fills. It includes a collection of beginner, intermediate, and advanced patterns that progressively increase in difficulty. Each of the drum beats include ten unique variations, and the drum fills include five unique variations. Put your new found slide technique skills to work using these fun exercises, and be prepared to see a dramatic boost in the way you creatively incorporate the bass drum within music!

**Double Pedal Single Strokes 1**
Here we take a look at double pedal single strokes with Sean Lang. Sean walks you through a couple different lessons in this module, including: the element of groove, developing balance and control, basic 8th and 16th note patterns, bass drum triplets, foot assignment, single stroke beats, single stroke fills, note value exercises, and the herta lick. Playing quick single strokes is a valuable part of playing both single and double bass, so make sure you check out this fun module.

**Double Pedal Single Strokes 2**
In Module 8 we take a closer look at double pedal single strokes with Dave Atkinson. Dave takes you through a variety of topics, including: double bass warm ups, developing the weaker foot, double bass slide triplets, developing slow speeds, developing speed with single strokes, how to develop endurance, the swivel technique, practicing along to music, developing hand and foot independence, and ending with odd numbers. It's all broken down in step-by-step detail.

**Double Pedal Double Strokes**
Next, you get a chance to look at double pedal single strokes with Jared Falk. He walks you through basic 16th note patterns, 16th note triplets with double strokes, and hertas with double strokes. This really is something that can take your drumming to the next level and give you an edge over other drummers at the same time. By combining the tips and tricks from all three drummers, you'll have an opportunity to create your own original drumming style.

**Broken Double Bass Beats**
Module 10 features Sean Lang teaching broken double bass beats. These patterns are powerful for creating tight grooves with some added double bass flair. Sean teaches eleven beginner, intermediate, and advanced examples in a progressive way. You can start by learning these example patterns. Then, when you feel comfortable with the concepts, you can start creating your own original patterns to spice things up and boost your creativity on the drum set.

**Double Bass Exercises**
Module 11 covers some really fun double bass exercises that you can implement into your drumming immediately. Dave Atkinson walks you through beginner double bass beats and fills, intermediate double bass beats and fills, as well as advanced double bass beats and fills. If you have been working through the modules, then this is a great place to put some of those new techniques to work, and really start having some fun applying everything you have learned.

**Metal & Blast Beats**
Module 12 features Sean Lang teaching metal and blast beats. Even if you're not into metal drumming, these lessons will still teach you some very important speed and coordination concepts that every drummer should learn. Sean Lang walks you through: skank blasts, hammer blasts, bomb blasts, gravity blasts, traditional blasts, one foot vs. two feet, a hybrid hammer blast, developing speed with one foot, and creating groove within blasts.

**Rudiments With The Feet**
Playing rudiments with the feet is a massively under-rated concept of bass drumming. A lot of drummers assume that rudiments are only for the hands, but this module opens up new possibilities by utilizing the rudiments with your feet. Jared Falk takes you through playing 5, 6, 7, 9, and 11 stroke rolls - as well as playing swiss army triplets all with your feet. With this module you can create thousands of new drum beats and fills, all by using the rudiments with your feet.

**Hand To Feet Combinations**
This module is one of the coolest modules within the Bass Drum Secrets 2.0 pack. Here, Jared, Dave and Sean all give their take on unique hand to feet combinations. By the end of this module you will be able to create your own hand to feet combinations for use within beats, fills, and creative drum solos! This module will open new doors for your drumming and really start to expand your creativity when you sit down at the drums.

**Drum Play-Alongs**
Okay, you made it! We are at the first play-along module of the Bass Drum Secrets 2.0. Here you get to put all of your new found bass drum techniques, beats, fills, concepts, ideas, and creativity to use. This module features Dave Atkinson's band 'Yuca' as well as a ton of cool jam tracks that you can download and take back to your drum kit. So, have some fun and remember to mix things up with the new techniques you have been developing!

**Drum Play-Alongs**
Sean Lang and his band 'First Reign' are featured in the second play-along module of Bass Drum Secrets 2.0. Sean plays along to a couple songs from First Reign's album, then plays along to some really cool jam tracks all of which you can download and take back to your kit. The musical styles include: fast rock, half time rock, metal, shuffle rock, punk, and straight rock. As with all the play-alongs, the video footage includes educational view and a dedicated foot-cam view.

**Drum Play-Alongs**
In this third play-along module, Jared Falk plays to some cool jam tracks, including: half time rock, fast rock, metal, straight rock, and drum and bass. You can start by watching the educational view to get an overall feel of the track, and then skip to the foot-cam view to see exactly what techniques are being used. Then, take the drum-less audio tracks to your kit to start jamming along with them. You can use his beats and fills, or come up with your own!

**Slow Motion**
Module 18 features all of the slow motion footage from Bass Drum Secrets 2.0 compiled for easy viewing. The module includes slow motion footage for the slide technique, double pedal slide technique, swivel technique, heel-toe technique, double stroke hertas, feathering the bass drum, swiss army triplets, one handed roll, break beat, gravity blast, double pedal single strokes and much more. This footage shows you exactly how each of these techniques are played!

**Pedal Reviews**
Here you can watch video reviews of nine popular bass drum pedals. If you are aren't sure what pedal would best suit your playing style, watch this module to learn more. Finding the right pedal can dramatically improve your bass drumming ability. Pedals included are: Gibraltar Intruder Direct Drive, Axis Longboard, Tama Speed Cobra, Tama Iron Cobra, Gibraltar 3311, Pearl Eliminator, Yamaha Flying Dragons, Pearl P900, and Stomp Direct Drive Retrofit.

**Bonuses**
In this 'bonuses' module, we have included some fun and creative behind-the-scenes footage! Some of the content includes: all three instructors playing in unison, plenty of drum solo footage, how to play the flat-foot technique, how to use triggers and what they are, gear talk, interviews of all the instructors, behind the scenes footage from the making of BDS 2.0, and much more. Nothing got held back - this section includes ever