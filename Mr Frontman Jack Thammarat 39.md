## Mr Frontman Jack Thammarat 39

 
  
 
**## Files you can download:
[Link 1](https://vittuv.com/2tCcV7)

[Link 2](https://miimms.com/2tCcV7)

[Link 3](https://gohhs.com/2tCcV7)

**

 
 
 
 
 
# Mr Frontman Jack Thammarat 39: A Guitar Legend's Birthday Celebration
 
Jack Thammarat is a Thai guitarist who has won international acclaim for his virtuosic skills and original compositions. He is best known for his song "Mr Frontman", which he composed and performed for the Guitar Idol III contest in 2009, where he emerged as the winner. Since then, he has released several albums, collaborated with other artists, and toured around the world.
 
On May 23, 2020, Jack Thammarat celebrated his 39th birthday by releasing a new version of "Mr Frontman" with a backing track from his latest album "Still On The Way". He also shared his custom preset for the Line 6 Helix guitar processor, which he created with Live Ready Sound. The preset, called "Ultimate Tones: Jack Thammarat Volume 1", is available for purchase on his website.
 
In this article, we will take a closer look at Jack Thammarat's musical journey, his influences, his gear, and his tips for aspiring guitarists. We will also review his new rendition of "Mr Frontman" and his Helix preset, and see why he is one of the most respected and admired guitarists in the world today.
 
## Jack Thammarat's Musical Journey
 
Jack Thammarat was born on May 23, 1981, in Udon Thani, Thailand. He started playing guitar at the age of 13, inspired by his father who was also a musician. He learned by ear and by watching videos of his favorite guitarists, such as Joe Satriani, Steve Vai, Eric Johnson, and John Petrucci. He also studied music theory and classical guitar at a local music school.
 
Jack Thammarat began his professional career as a session guitarist and a guitar teacher in Bangkok. He played in various bands and genres, from pop to rock to jazz to metal. He also participated in several guitar competitions, such as the Yamaha Guitar Festival and the Thailand Guitar King. He gained recognition for his versatility, creativity, and technical ability.
 
In 2009, Jack Thammarat entered the Guitar Idol III contest, an online competition that attracted thousands of entries from around the world. He submitted his original song "Mr Frontman", which showcased his melodic sense, harmonic knowledge, rhythmic skills, and expressive tone. He received rave reviews from the judges and the audience, and was voted as one of the finalists to perform live in London. There, he impressed everyone with his flawless performance and charismatic stage presence, and was crowned as the Guitar Idol III winner.
 
The success of "Mr Frontman" opened many doors for Jack Thammarat. He signed a deal with Jam Track Central, a website that offers backing tracks and instructional videos from top guitarists. He also became an endorser of several brands, such as Suhr Guitars, Seymour Duncan Pickups, Boss Pedals, Palmer Amps, Elixir Strings, and Line 6 Helix. He started touring internationally as a solo artist and as a guest performer with other musicians. He also released more albums of his original songs, such as "Melodic Freeway", "The Inspiration", and "Still On The Way".
 
## Jack Thammarat's Influences
 
Jack Thammarat has a wide range of musical influences that reflect his diverse background and taste. He cites Joe Satriani as his biggest idol and inspiration, especially for his melodic approach and use of effects. He also admires Steve Vai for his innovation and expression, Eric Johnson for his tone and phrasing, John Petrucci for his technique and composition, Andy Timmons for his feel and groove, Greg Howe for his fusion style and improvisation, Guthrie Govan for his versatility and mastery.
 
Besides these guitar heroes, Jack Thammarat also draws inspiration from other genres and instruments. He listens to jazz legends like Pat Metheny, Mike Stern, John Scofield, and Scott Henderson. He studies classical composers like Bach, Beethoven, Mozart, and Chopin.
 842aa4382a
 
 
