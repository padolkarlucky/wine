## essay underline or italicize book title

 
  
 
**Download File ⇒ [https://www.google.com/url?q=https%3A%2F%2Ftiurll.com%2F2tItjr&sa=D&sntz=1&usg=AOvVaw2Js3-0sVXz2kIgYh2IQXAR](https://www.google.com/url?q=https%3A%2F%2Ftiurll.com%2F2tItjr&sa=D&sntz=1&usg=AOvVaw2Js3-0sVXz2kIgYh2IQXAR)**

 
 
 
 
 
There are certain rules to follow when deciding what we should do to ensure that we're using a book's title correctly. The **book title** is the name of the book, which is found on the front cover and the side of most books. Some people believe that book titles should be underlined, while others think that they should be written in italics.
 
In many classrooms, written communication takes place using the traditional method of pen or pencil on paper. We must underline the titles of books when writing this way. Underlining clearly shows that special attention should be given to the underlined information (in this case the book title). Underlining titles of books is not used nearly as often as italics. However, when handwriting information, we do not have the benefit of keyboard shortcuts and enhancements.
 
Another common way that people write is on a computer, which allows them the choice of using different keyboard options. When writing using a computer, we should type the title of the book in italics. This is the most accepted and common practice for writing titles of books.
 
The **italics** option is usually found within text programs like Microsoft Word and Google Docs. The italics mark is a slanted capital letter I at the top of the open document. This allows the lettering to slant slightly to the side. Using italics sets the title of the book apart from other words in the written text.
 
Another context in which book titles are used would be for a works-cited page. This is also called a 'bibliography.' A **bibliography** is a list of referenced materials that have been used within works such as research papers. When writers are referring to a book's title for a bibliography, they should write the title in italics. A few examples of what this should look like are shown here.
 
When referring to a **book title** in writing, deciding whether to underline or italicize the title can be confusing. At a time when technology is so popular, we most often use computers and other electronic devices for writing. When this is the case, we must remember to write a book's title in **italics**. If we're taking notes or completing an assignment the traditional way, using pen and paper, we should underline a book title. We most often reference books in sentences and bibliographies. When using a computer, we should always write a book's title in italics.
 
Back in the day, before the internet and blue underlined words meant links to other websites, students were taught to underline the titles of books, magazines, plays, songs, movies, and other titled works. Nowadays, people expect underlined words to be links that take them to even more informative content, so the rules have changed.
 
I thought the chapter, "Why Mornings Matter (more than you Think)," in The Miracle Morning for Writers was the most powerful. (Chapter titles are set off by quotation marks while book titles are italicized.)
 
"If you are writing an essay do you underline, use quotation marks or italicize the book title?" eNotes Editorial, 10 Apr. 2014, -help/you-writing-an-essay-do-you-underline-use-354926.Accessed 10 Feb. 2023.
 
If you are using a word processor you can and should italicize book titles. However, if you are using a typewriter, I don't see how you can use italics. Before word processors came into common usage, it used to be the standard practice to underline book titles when typing. This indicated that these titles should be in italics if the manuscript was published in a book, magazine, or newspaper. If you are writing something in longhand you should also underline book titles, but I don't see how you could have any other choice, unless you happen to be artistically gifted and can make your handwriting look like italics when you want to. There may be some typewriters which allow you to switch from regular typeface to italics, but I have never seen one. No doubt the IBM Selectrics could be used to type book titles in italics if you switched from one ball to another and then back again, but that seems awfully time-consuming.
 
Book titles are italicized. If you are using a typewriter and can't write in italics, then it is customary to underline the title. Same applies to plays. Titles of poems, short stories, essays, and other short pieces are set off in double quotes. George Orwell's "Shooting an Elephant" would be in quotes, since it is only an essay. Commas and periods go inside the closing quotation mark, as for example with Orwell's "Shooting an Elephant." The English do it differently and this can create some confusion, but observe the usage in American publications.
 
It occurs to me that you might be referring to a collection of essays by Orwell in a book that just has the title Shooting an Elephant derived from the title of the essay. If it is a book title it should be italicized.
 
The title of the periodical (journal, magazine, or newspaper) is italicized. The title of the article or work is enclosed in quotations. Omit any introductory article in the newspaper title for English-language newspapers (Palm Beach Post, not The Palm Beach Post). Retain the article in non-English language newspapers (Le monde).
 
The title of the work is italicized if the work is independent. The title of the work is enclosed in quotation marks if it is part of a larger work. The title of the overall Web site is italicized if distinct from the the title of the work.
 
If you happen across some older documents, you might see book titles underlined, as this used to be the standard. But underlining fell out of favour as computer technology improved and formatting and style options became more varied.
 
In practice, this means that if you have a comma or full stop after your book title, you should turn italics off before that piece of punctuation. The same is true for dashes, question marks and any other punctuation.
 
A title is placed in **quotation marks** if the source is part of a larger work. A title is **italicized** (or underlined if italics are unavailable) if the source is self-contained independent.
 
If you have an **untitled source**, provide a generic description of it, neither italicized nor enclosed in quotation marks, in place of a title. Capitalize the first word of the description and any proper nouns in it. (pp. 121-124)
 
**Abbreviations**. The titles of the books of the Bible are often abbreviated in parenthetical citations. Refer to this list of abbreviations from Grove City College to see the recommended abbreviations.
 
But, titles of individual editions of the Bible are italicized in the Works Cited entry and in your essay. For example, The English Standard Version Bible: Containing the Old and New Testaments with Apocrypha.
 
Parenthetical references to books of the Bible include the edition in the first citation (especially if you use more than one edition in your essay). They always include chapter (abbreviated) and verse. Underline or italicize the title of the version you're using:
 
Titles are part of a research paper and their proper usage can make the paper more presentable. As such, titles can be underlined, italicized, typed in bold, or put into quotation marks to emphasize particular words. This article will discuss the issue of underlining the titles of a research paper.
 
**You can underline research paper or essay titles if that is what your instructor wants you to do or there are guidelines to be followed. This is because formatting styles like APA and MLA do not allow underlining of titles. Therefore, you cannot underline the titles of your paper without considering the purpose of the titles, what they are used to refer to, and so on.**
 
As we noted earlier, the essence of underlining or italicizing titles is to create emphasis. The same case applies to titles of longer works. However, for longer works, it is advisable to italicize it because underlining a title that is too long may look unpresentable.
 
For level 5 headings, the title should not be italicized. However, it should be indented from the left side of the page. The unique thing about this level of heading is that instead of the text or the paragraph that follows starting on a new line below the title, it will start within the same line as the level 5 title.
 
If a title does not meet such requirements, then it should not be italicized. You should always keep in mind that any academic work should have consistency. If you decide to italicize the aforementioned, then you should maintain it. If you decide to underline whatever was supposed to be italicized, maintain that too.
 
Longer works like books, journals, etc. should be italicized and shorter works like poems, articles, etc. should be put in quotations. For example, a book title would be placed in italics but an article title would be placed in quotation marks.
 
The titles of major works like books, journals, etc. should be italicized (this also includes legal cases and some other special names) and subsections of larger works like book chapters, articles, etc. should be put in quotations. For example, the title of a legal case would be placed in italics but a book chapter would be placed in quotation marks.
 
If a novella or novelette has been published both independently and in a collection, I would err towards italicizing the title. Some anthologies, like The Norton Anthology of English Literature, include works of all lengths, even entire novels and plays. So if a novella has been published separately, I would italicize it, even if it appears elsewhere in a collection.
 
You should underline longer works if you are using Chic