## Lautaro Un Joven Libertador De Arauco Pdf 11

 
  
 
**DOWNLOAD »»» [https://www.google.com/url?q=https%3A%2F%2Fbltlly.com%2F2tzD5K&sa=D&sntz=1&usg=AOvVaw0gwTOlG53FFuL\_r-ha9Ykk](https://www.google.com/url?q=https%3A%2F%2Fbltlly.com%2F2tzD5K&sa=D&sntz=1&usg=AOvVaw0gwTOlG53FFuL_r-ha9Ykk)**

 
 
 
 
 
# Lautaro: el hÃ©roe mapuche que desafiÃ³ a los conquistadores espaÃ±oles
 
Lautaro fue uno de los mÃ¡s valientes y destacados lÃ­deres del pueblo mapuche, que resistiÃ³ la invasiÃ³n espaÃ±ola en el territorio que hoy es Chile. Su vida y hazaÃ±as fueron narradas por el escritor chileno Fernando AlegrÃ­a en su novela *Lautaro, joven libertador de Arauco*, publicada en 1943 y reeditada en 2018.
 
La novela cuenta la historia de Lautaro desde su infancia, cuando fue capturado por el gobernador espaÃ±ol Pedro de Valdivia y se convirtiÃ³ en su paje. Lautaro aprendiÃ³ el idioma, las costumbres y las tÃ¡cticas militares de los espaÃ±oles, pero nunca olvidÃ³ su origen ni su lealtad a su pueblo. Un dÃ­a, aprovechando una oportunidad, escapÃ³ y regresÃ³ a su tierra natal, donde se uniÃ³ a la rebeliÃ³n mapuche contra los invasores.
 
Lautaro se convirtiÃ³ en toqui (jefe militar) y organizÃ³ a los guerreros mapuches para enfrentar a los espaÃ±oles con una estrategia audaz y novedosa. LogrÃ³ derrotar a Valdivia en la batalla de Tucapel (1553) y lo matÃ³ con sus propias manos. Luego, liderÃ³ una serie de victorias contra las fuerzas espaÃ±olas en Marihuenu (1554), AndaliÃ©n (1556) y ConcepciÃ³n (1556), llegando hasta las puertas de Santiago, la capital del reino.
 
Sin embargo, su sueÃ±o de liberar a Arauco de la dominaciÃ³n espaÃ±ola se vio truncado por la traiciÃ³n de algunos caciques aliados y la superioridad numÃ©rica y armamentÃ­stica de los espaÃ±oles. Lautaro muriÃ³ en combate en Peteroa (1557), cuando tenÃ­a apenas 23 aÃ±os. Su muerte fue llorada por su pueblo, que lo considerÃ³ un hÃ©roe y un mito. Su nombre y su ejemplo siguen inspirando a las generaciones posteriores de mapuches y chilenos que luchan por la justicia y la libertad.
 
La novela de AlegrÃ­a es una biografÃ­a novelada que mezcla hechos histÃ³ricos con elementos literarios y fantÃ¡sticos. EstÃ¡ escrita con un lenguaje sencillo y emotivo, que busca acercar al lector a la figura de Lautaro y a la cultura mapuche. La obra estÃ¡ ilustrada por Mario Silva Ossa, mÃ¡s conocido como CorÃ©, un destacado dibujante e ilustrador chileno. La portada del libro muestra a Lautaro montado en un caballo blanco, con una lanza en la mano y una mirada desafiante.
 
*Lautaro, joven libertador de Arauco* es una obra clÃ¡sica de la literatura chilena y latinoamericana, que ha sido traducida a varios idiomas y adaptada al cine, al teatro y al cÃ³mic. Es una lectura recomendada para todos los que quieran conocer mÃ¡s sobre la historia y la identidad de Chile y de los pueblos originarios.
 
Fuente: [^1^] [^2^] [^3^] [^4^]
  
Lautaro no solo fue un gran guerrero, sino tambiÃ©n un gran lÃ­der que supo unir a los mapuches en torno a un objetivo comÃºn: la defensa de su territorio y su cultura frente a la invasiÃ³n espaÃ±ola. Su figura trascendiÃ³ las fronteras de Arauco y se convirtiÃ³ en un sÃ­mbolo de la resistencia indÃ­gena en toda AmÃ©rica. Su nombre fue adoptado por otros pueblos originarios que lucharon contra el colonialismo, como los tehuelches, los ranqueles y los pehuenches.
 
Lautaro tambiÃ©n dejÃ³ una huella profunda en la literatura y el arte chilenos y universales. Su vida y sus hazaÃ±as fueron inmortalizadas por el poeta espaÃ±ol Alonso de Ercilla en su obra maestra *La Araucana*, publicada entre 1569 y 1589. En este poema Ã©pico, Ercilla reconoce la valentÃ­a y el heroÃ­smo de Lautaro y lo presenta como un personaje digno de admiraciÃ³n y respeto. Lautaro tambiÃ©n ha sido protagonista de otras obras literarias, como las novelas *Lautaro* de Vicente PÃ©rez Rosales (1886), *Lautaro, joven libertador de Arauco* de Fernando AlegrÃ­a (1943) y *Lautaro: el cruce de los sueÃ±os* de Jorge Baradit (2010).
 
Asimismo, Lautaro ha inspirado a diversos artistas visuales, como pintores, escultores y muralistas, que han plasmado su imagen en lienzos, monumentos y murales. Algunos ejemplos son el busto de Lautaro realizado por Nicanor Plaza en 1882, el mural *Lautaro* pintado por Gregorio de la Fuente en 1945 y el mural *La muerte de Lautaro* creado por Pedro Subercaseaux en 1957. Lautaro tambiÃ©n ha sido representado en el cine, el teatro y el cÃ³mic, como en la pelÃ­cula *Lautaro* dirigida por JosÃ© Bohr en 1940, la obra teatral *Lautaro* escrita por Isidora Aguirre en 1969 y la historieta *Lautaro: hijo del trueno* ilustrada por Claudio Romo en 2012.
 
Lautaro es hoy en dÃ­a una figura emblemÃ¡tica para los mapuches y los chilenos, que lo reconocen como un hÃ©roe nacional y un defensor de los derechos humanos. Su nombre se ha dado a varias calles, plazas, colegios e instituciones del paÃ­s. Su legado sigue vivo en la memoria colectiva y en el espÃ­ritu de lucha por la justicia y la libertad.
 
Fuente: [^1^] [^2^] [^3^]
 842aa4382a
 
 
