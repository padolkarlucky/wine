## Project: Snowblind Game

 
  
 
**Click Here ––– [https://byltly.com/2tFhlS](https://byltly.com/2tFhlS)**

 
 
 
 
 
***Project: Snowblind*** is a first-person shooter video game developed by Crystal Dynamics and published by Eidos Interactive for PlayStation 2, Xbox and Microsoft Windows. The game follows soldier Nathan Frost, who is enhanced with nanotechnology following injuries on a mission and sent against a military regime known as the Republic. Players control Frost through a series of linear levels, using enhancements both in combat and to manipulate security devices such as cameras. The online multiplayer allows up to sixteen players to take part in modes ranging from team-based to solo battles.
 
Beginning development in 2004, the game was Crystal Dynamics' first attempt at a first-person shooter and originally planned as part of the *Deus Ex* series with consultation from original developer Ion Storm. The game eventually evolved into its own product, but retained gameplay elements from its *Deus Ex* roots. Reception of the game was generally positive.
 
Similar to the *Deus Ex* series, the focus of *Project: Snowblind*'s gameplay is giving the player a variety of choices on how to approach any given situation. Although the game is generally linear, most levels feature multiple paths through any given area, allowing players to either rush in guns blazing or attempt to find a more stealthy side-path. Unlike *Deus Ex*, the game is entirely centered around pure combat, but nonetheless provides the player with multiple options regarding every battle. Every weapon in the game has a secondary fire mode, several of which create exotic effects such as a swarm of drones that will actively seek out and attack enemies. The player can also throw a variety of grenades with different effects, including a riot shield that creates a temporary stationary energy wall for the player to take cover behind. The player can also use a special "Icepick" device to hack enemy cameras, turrets, and robots and use them against enemy forces. The game also features several driveable vehicles. Finally, the player's character possesses a variety of nano-technology augmentations that can be used to grant them various powers.[2][3]
 
One of the main focuses of *Project: Snowblind*'s gameplay is Nathan Frost's nano-technology augmentations. Although most of Frost's augmentations are inactive at the beginning of the game, they become activated as the game progresses, granting Frost additional powers.[4]
 
*Project: Snowblind'*s multiplayer mode features several of the gameplay elements found in the game's single-player campaign, including drivable vehicles, the ability to operate and hack cameras and turrets on the battlefield, and the ability to use augmentation powers.[2]
 
In 2003, following the release of *Deus Ex: Invisible War*, multiple attempts were made by series developer Ion Storm and publisher Eidos Interactive to create further entries in the *Deus Ex* series. One of these projects, planned as the third series entry following *Invisible War*, was titled *Deus Ex: Clan Wars*.[5][6] As development progressed, the game changed into its own identity and was rebranded as *Project: Snowblind*.[6] Preproduction for *Project: Snowblind* began in early 2004, being the first FPS produced by Crystal Dynamics.[7] During its early development, Crystal Dynamics had advice from Ion Storm and series creator Warren Spector. As development progressed, the game evolved into its own entity and took an original name.[7][8]
 
The music was composed by Troels Brun Folmann, who became involved with the game after joining the company to complete research for a PhD thesis. *Project Snowblind* was Folmann's second score after *Robin Hood: Defender of the Crown*. He was invited to score the game to further his research into video game music. The game's directors wanted Folmann to create an "epic orchestral score with eastern/ethnic elements".[9]
 
The game received "favorable" reviews on all platforms according to video game review aggregator Metacritic.[25][26][27] It was criticized for its short length and inactive multiplayer, but was praised for its surprisingly entertaining gameplay.
 
**Minimum:**IBM PC or 100% compatible, Microsoft Windows 2000/XP, Pentium IV, 1.5 GHz (Or AMD Athlon XP equivalent) processor, 100% DirectX 64 MB 3D Accelerated video card with Pixel Shader v1.1 Capability, 256 MB System RAM, 100% DirectX 9 Compatible Sound Card, 3GB free uncompressed hard drive space (additional space may be necessary for saved games), 100% Windows 2000/XP compatible Mouse and Keyboard
 
As stated on the Project: Snowblind box, laptops and integrated video chips may work with Project: Snowblind, but are not supported. Should you incur problems while running the game on an integrated video chip, Eidos Tech Support will not be able to help you.
 
A most enjoyable, easy to play FPS with a perfectly balanced concept of difficulty in both, action and problem solving. Quite possibly, the marketing for the game (or lack of, more likely) maybe the reason why it has not caught on popularly (though it might) I do wish their developer would reconsider about shelving the sequel, market the game for a long awaited release. It most definitely deserves a chance for marketing.
 
Here are some games you can buy on Amazon: when we use affiliate links on Unseen64 we may receive a commission for the sale. **For you is free, you don't pay anything more**, but for us it helps a lot! Currently this is working for Amazon.com, Amazon.it and Amazon.co.uk :)
 
In May, Eidos brought a game called Snowblind to E3. This tactical squad-based shooter was set in a dark, dystopian future and featured a special ops commando packing an arsenal of high-tech weaponry and nano-augmentations that granted him superhuman abilities. It didn't take long for people to figure out that Snowblind was, in fact, Clan Wars, the game Eidos teased in April. For reasons that have never been fully revealed, the publisher decided to distance this new project from Deus Ex entirely. A strange choice given that Invisible War, despite bad reviews, sold over a million copies. According to an anonymous source quoted in a Gamespot report from June 17, 2004, "Snowblind did begin as Clan Wars, but then it began to take a direction of its own."
 
Project Snowblind has been largely forgotten about, which is a shame because it's quietly one of the best shooters of the PS2 era. It reviewed well, with 8/10s across the board, but ultimately failed to make much of an impact. No follow-up was ever revealed or even hinted at. It makes me wonder what would have happened if Eidos stuck to its original plan and the game was released as Deus Ex: Clan Wars, and was more directly connected to that series. It might not have helped, because the Deus Ex name was cursed after Invisible War's negative reception. Either way, I admire Crystal Dynamics for trying to make a deeper, smarter FPS. Project Snowblind might have faded into obscurity, but it remains one of the studio's most interesting, ambitious games.
 
Project: Snowblind started its life as a Deus Ex spin-off, with the purpose of capitalizing on that universe's rich fiction and distinct style by pairing it with a more visceral gameplay experience. Despite the fact that Project: Snowblind bears no official Deus Ex ties now, the game brandishes an almost identical near-future cyber-thriller feel, right down to the eerie blue circuit-board highlights on the hero's skin. Crystal Dynamics already delivered Snowblind to the Xbox and PlayStation 2 nearly a month before it arrived on the PC, and it will be apparent to PC shooter fans that the game was built specifically with these consoles in mind. It's not the prettiest shooter on the PC, but it's got a good amount of style, and the run-and-gun action stands up pretty well, regardless of the platform.
 
Snowblind borrows liberally from highly recognizable sources, an ethos which might as well be written in the design documents for most modern first-person shooters. As mentioned before, the game's look owes a lot to Deus Ex, though because the action's set in a war-torn near-futuristic Hong Kong, there's a bit more overt Asian influence, both ancient and modern. Still, the game gets a lot of mileage out of contrasting the old and the new, creating a look that's a bit more steely and streamlined than what typified *Blade Runner*'s grimy cyberpunk aesthetic. Expect to see a lot of near-futuristic military hardware couched in dilapidated industrial complexes, blown-out urban centers, and--in one of the game's more visually stunning pieces--in a massive domed opera house that's been hastily converted in to a POW camp.
 
The game's unwavering devotion to this specific aesthetic goes far in establishing Snowblind's overall vibe, as does an almost fetish-level concentration of filtered lighting effects. There's a soft-glow effect at work for virtually the entire run of the game, which cleverly smoothes out the hard edges while creating a tangible atmosphere. There's a unique effect associated with every one of your special abilities, each of which can change the entire look of the game. The titular "snowblind" effect is especially well done, though considering the harrowing situations when it usually comes up, you'll likely be too freaked-out to really sit back and appreciate it.
 72ea393242
 
 
