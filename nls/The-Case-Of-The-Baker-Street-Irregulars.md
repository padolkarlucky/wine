## The Case Of The Baker Street Irregulars

 
  
 
**Download · [https://shoxet.com/2tESTf](https://shoxet.com/2tESTf)**

 
 
 
 
 
Sherlock Holmes Consulting Detective is a cooperative game where the players try to solve mysterious cases by interviewing suspects, searching through newspapers and walking the streets for clues. Here, players are walking in the steps of the Irregulars, the young London kids helping the most famous detective ever during his investigations. Once you have completed your investigation, compare your deduction skills to those of the master detective himself, Sherlock Holmes!
 
**Gameplay:** Sherlock Holmes Consulting Detective is a hybrid cooperative game with elements of storytelling and logical deduction. In this set, the players are members of the Baker Street Irregulars, the unofficial gang of street children that aids Sherlock Holmes towards solving his mysterious cases. There are ten cases in the game ranging from 16 to 44 pages, with the vast majority of them being around the 20 to 24 page mark. The first five cases have only one relevant newspaper per case and are independent from one another. From case 6 onwards, all cases relate to case 10 and should be played before it, preferably in chronological order. All 1989 newspapers can be used when tackling case 10. Cases are presented in chronological order. They start on the 19th of September 1885, up to the last case starting on the 12th of September 1889.
 
In this cooperative game, you and your fellow players try to solve mysterious cases by walking the streets in search of clues. Interrogate suspects, carefully read the newspapers, and go in search of clues throughout the London streets. When you believe you have solved the case, compare your findings to those of Sherlock Holmes.
 
Dear little chap! said Holmes strategically.What a rosy-cheeked young rascal! Now, Jack, is there anything you would like?
The youth pondered for a moment.
Id like a shillin, said he.
Nothing you would like better?
Id like two shillin better, theprodigy answered after some thought.
Here you are, then! Catch!A fine child, Mrs.Smith!
Lor bless you, sir, he is that, and forward. Hegets amost too much for me to manage, specially when my man is away days at atime.
Away, is he? said Holmes in a disappointed voice.I am sorry for that, for I wanted to speak to Mr. Smith.
Hes been away since yesterday mornin, sir,and, truth to tell, I am beginnin to feel frightened about him. But if it was abouta boat, sir, maybe I could serve as well.
I wanted to hire his steam launch.
Why, bless you, sir, it is in the steam launch that hehas gone. Thats what puzzles me; for I know there aint more coals in her thanwould take her to about Woolwich and back. If hes been away in the barge Idha thought nothin; for many a time a job has taken him as far as Gravesend,and then if there was much doin there he might ha stayed over. But what goodis a steam launch without coals?
[124]He might have bought some at a wharf down the river.
He might, sir, but it werent his way. Many a timeIve heard him call out at the prices they charge for a few odd bags. Besides, Idont like that wooden-legged man, wi his ugly face and outlandish talk. Whatdid he want always knockin about here for?
A wooden-legged man? said Holmes with blandsurprise.
Yes, sir, a brown, monkey-faced chap thats calledmoren once for my old man. It was him that roused him up yesternight, and,whats more, my man knew he was comin, for he had steam up in the launch. Itell you straight, sir, I dont feel easy in my mind about it.
But, my dear Mrs. Smith, said Holmes, shrugginghis shoulders, you are frightening yourself about nothing. How could you possiblytell that it was the wooden-legged man who came in the night? I dont quiteunderstand how you can be so sure.
His voice, sir. I knew his voice, which is kind othick and foggy. He tapped at the winderabout three it would be. Show a leg,matey, says he: time to turn out guard. My old man woke upJimthats my eldestand away they went without so much as a word to me. Icould hear the wooden leg clackin on the stones.
And was this wooden-legged man alone?
Couldnt say, I am sure, sir. I didnt hear noone else.
I am sorry, Mrs. Smith, for I wanted a steam launch, andI have heard good reports of the  Let me see, what is her name?
The Aurora, sir.
Ah! Shes not that old green launch with a yellowline, very broad in the beam?
No, indeed. Shes as trim a little thing as any onthe river. Shes been fresh painted, black with two red streaks.
Thanks. I hope that you will hear soon from Mr. Smith. Iam going down the river, and if I should see anything of the Aurora I shall lethim know that you are uneasy. A black funnel, you say?
No, sir. Black with a white band.
Ah, of course. It was the sides which were black.Good-morning, Mrs. Smith. There is a boatman here with a wherry, Watson. We shall take itand cross the river.
The main thing with people of that sort, saidHolmes as we sat in the sheets of the wherry, is never to let them think that theirinformation can be of the slightest importance to you. If you do they will instantly shutup like an oyster. If you listen to them under protest, as it were, you are very likely toget what you want.
Our course now seems pretty clear, said I.
What would you do, then?
I would engage a launch and go down the river on thetrack of the Aurora.
My dear fellow, it would be a colossal task. She mayhave touched at any wharf on either side of the stream between here and Greenwich. Belowthe bridge there is a perfect labyrinth of landing-places for miles. It would take youdays and days to exhaust them if you set about it alone.
Employ the police, then.
No. I shall probably call Athelney Jones in at the lastmoment. He is not a bad fellow, and I should not like to do anything which would injurehim professionally. [125]But I have a fancy for working it out myself, now that we have gone so far.
Could we advertise, then, asking for information fromwharfingers?
Worse and worse! Our men would know that the chase washot at their heels, and they would be off out of the country. As it is, they are likelyenough to leave, but as long as they think they are perfectly safe they will be in nohurry. Joness energy will be of use to us there, for his view of the case is sure topush itself into the daily press, and the runaways will think that everyone is off on thewrong scent.
What are we to do, then? I asked as we landed nearMillbank Penitentiary.
Take this hansom, drive home, have some breakfast, andget an hours sleep. It is quite on the cards that we may be afoot to-night again.Stop at a telegraph office, cabby! We will keep Toby, for he may be of use to usyet.
We pulled up at the Great Peter Street Post-Office, and Holmesdispatched his wire.
Whom do you think that is to? he asked as weresumed our journey.
I am sure I dont know.
You remember the Baker Street division of the detectivepolice force whom I employed in the Jefferson Hope case?
Well, said I, laughing.
This is just the case where they might be invaluable. Ifthey fail I have other resources, but I shall try them first. That wire was to my dirtylittle lieutenant, Wiggins, and I expect that he and his gang will be with us before wehave finished our breakfast.
It was between eight and nine oclock now, and I wasconscious of a strong reaction after the successive excitements of the night. I was limpand weary, befogged in mind and fatigued in body. I had not the professional enthusiasmwhich carried my companion on, nor could I look at the matter as a mere abstractintellectual problem. As far as the death of Bartholomew Sholto went, I had heard littlegood of him and could feel no intense antipathy to his murderers. The treasure, however,was a different matter. That, or part of it, belonged rightfully to Miss Morstan. Whilethere was a chance of recovering it I was ready to devote my life to the one object. True,if I found it, it would probably put her forever beyond my reach. Yet it would be a pettyand selfish love which would be influenced by such a thought as that. If Holmes could workto find the criminals, I had a tenfold stronger reason to urge me on to find the treasure.
A bath at Baker Street and a complete change freshened me upwonderfully. When I came down to our room I found the breakfast laid and Holmes pouringout the coffee.
Here it is, said he, laughing and pointing to anopen newspaper. The energetic Jones and the ubiquitous reporter have fixed it upbetween them. But you have had enough of the case. Better have your ham and eggsfirst.
I took the paper from him and read the short notice, which washeaded Mysterious Business at Upper Norwood.About twelve oclock last night [said the Standard]Mr. Bartholomew Sholto, of Pondicherry Lodge, Upper Norwood, was found dead in his roomunder circumstances which point to foul play. As far as we can learn, no actual traces ofviolence were found upon Mr. Sholtos person, but a valuable collection of Indiangems which the deceased gentleman had inherited [126] from his father has been carried off. The discovery wasfirst made by Mr. Sherlock Holmes and Dr. Watson, who had called at the house with Mr.Thaddeus Sholto, brother of the deceased. By a singular piece of good fortune, Mr.Athelney Jones, the well-known member of the detective police force, happened to be at theNorwood police station and was on the ground within half an hour of the first alarm. Histrained and experienced faculties were at once directed towards the detection of the criminals, with the gratifying result that the brother, Thaddeus Sholto, has already beenarrested, together with the housekeeper, Mrs. Bernstone, an Indian butler named Lal Rao,and a porter, or gatekeeper, named McMurdo. It is quite certain that the thi