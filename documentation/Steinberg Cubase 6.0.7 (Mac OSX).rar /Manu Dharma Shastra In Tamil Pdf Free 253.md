## manu dharma shastra in tamil pdf free 253

 
  
 
**## Downloadable file links:
[Link 1](https://blltly.com/2tFnIF)

[Link 2](https://bltlly.com/2tFnIF)

[Link 3](https://urlin.us/2tFnIF)

**

 
 
 
 
 
**Vishnu Sahasra Namam**
*Translated By P. R. Ramachander*

**Introduction**

Stotras are devotional hymns of Hinduism sung in praise of God. This word is derived from Sthuthi which means praise. Though post puranic age has given rise to large number of popular mellifluous Stotras composed by sages like Sankara, Ramanuja, Vedatha Desika, Appayya Deeksithar, Mooka Kavi and poets like Kalidasa, there are several great Stotras which trace their origin to Puranas. Of these the greatest is possibly the Vishnu Sahasra Namam found in Mahabharatha. Literally translated this means thousand names of Vishnu. This is found in the Anushasanika Parvam (chapter relating to orders or rules to the kings) of Mahabharatha.

Bheeshma Pitamaha was defeated and grievously wounded by Arjuna. But since he could choose the time of his death as per the boons received by him, he chose to die in Uttarayana and was waiting for the auspicious time. Meanwhile the war was over, leading to death of all those male members in his family except the Pancha Pandavas and the unborn child of Abhimanyu. Yudishtra, the eldest of the Pandavas, became the King of Hasthinapura and whom else would he go for advise other than Bheeshma the great. Anushasanika Parva is in the form of questions and answers between Yudishtra and Bheeshma Pitamaha. To a question as to what is the best possible Stotra, Bheeshma answers that it is the Vishnu Sahasra Nama and teaches it to Yudishtra.

Though it describes one thousand aspects and praises of the Lord, it is not simple and easy to understand. Many great Acharyas, lead by Sankara Bhagavat Pada, felt a need to give its meaning in crystal clear terms so that the devotee can not only sing but meditate on the aspect of the God that he is praising. But this again was done in Sanskrit. With the modern civilization, several translations of this Stotra is available in English and in almost all major languages of India.

I have attempted yet another translation in English. My aim is to make the translation simple to understand and easy to meditate. Naturally I have leaned heavily on many translations, which are already available, especially the one in Tamil by Anna published by the Sri Ramakrishna Math, Mylapore, Madras.

Though it is called Sahasra Nama in fact it contains only 901 distinct sounding names. Eight hundred and fifteen names are repeated once, Seventy-five of these names are repeated twice, nine of these names thrice and two of these names four times. The acharyas have attempted to give different meanings to the same word in different places successfully.

In the afterward to the Stotra, Goddess Parvathi asks Siva, the cosmic god, for an easy way to sing this Stotra for the learned. He replies that it is sufficient that the learned one repeats the name of Rama and this is equivalent to his singing of Sahasranama. It is important to note that this is easy method that is prescribed to the very learned who do not have time to recite it daily and not for everybody. It is also very important to meditate on the meaning of each word while it is sung.

**Invocation**

Shuklam Baradaram Vishnum, Sasi Varnam Chatur Bhujam,
Prasanna Vadanan Dyayet, Sarva Vignoba Sandaye

Dressed in white you are,
Oh, all pervading one,
And glowing with the colour of moon.
With four arms, you are, the all knowing one
I meditate on your ever-smiling face,
And pray, Remove all obstacles on my way.

Yasya dviradavaktradyah parisadyah parah satam,
Vighnam nighnanti satatam visvakasenam tamasaraye.

The elephant faced one along with his innumerable attendants,
Would always remove obstacles as we depend on Vishvaksena.

Vyasam Vasishtanaptharam, Sakthe Poutramakalmasham,
Parasarathmajam vande, Shukathatham Taponidhim.

I bow before you Vyasa,
The treasure house of penance,
The great grand son of Vasishta.
The grand son of Shakthi,
The son of Parasara.
And the father of Shuka,

Vyasaya Vishnu Roopaya, Vyasa Roopaya Vishnave,
Namo Vai Brahma Vidaya, Vasishtaya Namo Nama.

Bow I before,
Vyasa who is Vishnu,
Vishnu who is Vyasa,
And again and again bow before,
He, who is born,
In the family of Vasishta.

Avikaraya Shuddhaya, Nityaya Paramatmane,
Sadaika Roopa Roopaya, Vishnave Sarva Jishnave.

Bow I before Vishnu
Who is pure,
Who is not affected,
Who is permanent,
Who is the ultimate truth.
And He who wins over,
All the mortals in this world.

Yasya smarana Mathrena, Janma Samsara bandhanath.
Vimuchayate Nama Tasmai, Vishnave Prabha Vishnave
OM Namo Vishnave Prabha Vishnave

Bow I before Him,
The all-powerful Vishnu,
The mere thought of whom.
Releases one forever,
Of the ties of birth and life.
Bow I before the all powerful Vishnu

Shri Vaisampayana Uvacha:
Shrutva dharmaneshena, Pavanani cha Sarvasha,
Yudishtra santhanavam Puneravabhya Bhashata

Sri Vaisampayana said:
After hearing a lot,
About Dharma that carries life,
And of those methods great,
That removes sins from ones life,
For ever and to cleanse,
Yudhishtra asked again,
Bheeshma, the abode of everlasting peace.

Yudishtra Uvacha:
Kimekam Daivatham Loke, Kim Vapyegam Parayanam,
Sthuvantha Kam Kamarchanda Prapnyur Manava Shubham,
Ko Dharma sarva Dharmanam Paramo Matha
Kim Japan Muchyathe Jandur Janma Samsara Bhandanat

Yudishtra asked:
In this wide world, Oh Grandpa,
Which is that one God,
Who is the only shelter?
Who is He whom,
Beings worship and pray,
And get salvation great?
Who is He who should oft,
Be worshipped with love?
Which Dharma is so great,
There is none greater?
And which is to be oft chanted,
To get free.
From these bondage of life, cruel?

Bheeshma Uvacha:
Jagat Prabhum devadevam Anantham Purushottamam,
Stuvan nama Sahasrena, Purusha Sathathothida,
Tameva charchayan nityam, Bhaktya purushamavyayam,
Dhyayan sthuvan namasyancha yajamanasthameva cha,
Anadi nidhanam vishnum sarva loka Maheswaram
Lokadyaksham stuvannityam Sarva dukkhago bhaved,
Brahmanyam sarva dharmagnam Lokanam keerthi vardhanam,
Lokanatham Mahadbhootham Sarva Bhootha bhavodbhavam
Aeshame sarva dharmanam dharmadhika tamo matha,
Yad bhaktyo pundarikaksham Stuvyr-archanayr-nara sada,
Paramam yo mahatteja, paramam yo mahattapa
Paramam yo mahad brahma paramam ya parayanam
Pavithranam Pavithram yo mangalanam cha mangalam,
Dhaivatham devathanam cha bhootanam yo vya pitha
Yatha sarvani bhoothani bhavandyathi yugagame
Yasmincha pralayam yanthi punareve yuga kshaye
Tasya Loka pradhanasya Jagannatathasya bhoopathe
Vishno nama sahasram me Srunu papa bhayapaham

Bheeshma Replied:
That Purusha with endless devotion,
Who chants the thousand names,
Of He who is the lord of the Universe,
Of He who is the God of Gods,
Of He who is limitless,
Would get free,
From these bondage of life, cruel
He who also worships and prays,
Daily without break,
That Purusha who does not change,
That Vishnu who does not end or begin,
That God who is the lord of all worlds,
And Him, who presides over the universe,
Would loose without fail,
All the miseries in this life.
Chanting the praises,
Worshipping and singing,
With devotion great,
Of the lotus eyed one,
Who is partial to the Vedas,
Who is the only one, who knows the dharma,
Who increases the fame,
Of those who live in this world,
Who is the master of the universe,
Who is the truth among all those who has life,
And who decides the life of all living,
Is the dharma that is great.
That which is the greatest light,
That which is the greatest penance,
That which is the greatest Brahman,
Is the greatest shelter that I know.
Please hear from me,
The thousand holy names,
Which wash away all sins,
Of Him who is purest of the pure,
Of That which is holiest of holies,
Of Him who is God among Gods,
Of That father who lives Without death,
Among all that lives in this world,
Of Him whom all the souls,
Were born at the start of the world,
Of Him in whom, all that lives,
Will disappear at the end of the world,
And of that the chief of all this world,
Who bears the burden of this world.
I would teach you without fail,
Of those names with fame.
Which deal of His qualities great,
And which the sages sing,
So that beings of this wide world,
Become happy and great.

Rishir Namnam Sahsrasya Veda Vyaso Maha Muni
Chando aunustup stada devo bhagawan devaki sutha
Amruthamsu Bhavo Bhhejam Shakthir devaki nandana
Trisama hridayam tasya santhyarthe viniyujyade
Vishnum Jishnum Mahavishnum Prabha vishnun Maheswaram
Aneka Roopa Daityantham Namami purushottamam

These thousand names Yudishtra
Are Sung for peace,
And has Vyasa as the sage,
And is composed in Anusthup metre,
And has its God the son of Devaki,
And its root is Amrutamsudbhava
And its strength the baby of Devaki,
And its heart is Trissama
Bow I before Him,
Who is everywhere,
Who is ever victorious,
Who is in every being,
Who is God of Gods,
Who is the killer of asuras,
And who is the greatest,
Among all purushas.

**Dhyanam**

Ksheerodanvath pradese suchimani vilasad saikathe Maukthikanam
Malaklupthasanastha Spatikamani nibhai maukthiker mandithanga
Shubrai-rabrai-rathabrai ruparivirachitai muktha peeyusha varshai
Anandi na puniyadari nalina Gadha sankapanir Mukunda

Let that Mukunda makes us all holy,
Who wears all over his body
Beads made of crystal,
Who sits on the throne of garland of pearls,
Located in the sand of precious stones,
By the side of the sea of milk,
Who gets happy of the white cloud,
Sprayed of drops of nectar,
And who has the mace, the wheel and the lotus in His hands.

Bhoo padau yasya nabhir viyadasu ranila schandra suryaau cha nether
Karnavasasiro dhaumugamabhi dhahano yasya vasteyamabhdhi
Anthasth