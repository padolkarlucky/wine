## BRS Physiology (Board Review Series) Linda S. Costanzo PhD

 
 ![BRS Physiology (Board Review Series) Linda S. Costanzo PhD](https://libribook.com/Images/brs-physiology-board-review-series-7th-edition-pdf.jpg)
 
 
**## File links for downloading:
[Link 1](https://cinurl.com/2tEV5t)

[Link 2](https://byltly.com/2tEV5t)

[Link 3](https://tlniurl.com/2tEV5t)

**

 
 
 
 
 
**Reviewer:** David R Bell, BS, MS, PhD (Indiana University School of Medicine-Fort Wayne) 
**Description:** This is the seventh edition of a descriptive review of physiology presented in subdiscipline chapters. Chapters are composed of multilevel bulleted outlines with a limited number of color figures and sets of multiple-choice review questions. A review exam ends the book. The previous edition was published in 2015.
**Purpose:** The author's stated purpose is to aid students in their preparation for the USMLE Step 1 exam in the context of the physiology learned in their first two years of medical school. As a descriptive review of physiology, the book meets this goal, but it provides few explanations of processes and does not mimic true high-yield USMLE Step 1 prep tools.
**Audience:** The book is written exclusively for year 1 and 2 medical students. It meets the needs of students trying to crosscheck their fund of knowledge in physiology, but it is less useful as an explanatory book of physiological processes. The author is a professor of physiology and has been an established author of physiology books for over 20 years.
**Features:** The book is clear and well organized in outline form. Its content, however, is composed of short descriptive statements, facts, and definitions with occasional calculations, rather than explanatory text. 
**Assessment:** This is primarily a reconfiguration of the author's textbook, ***Physiology***, 6th edition (Elsevier, 2018) into an outline format. The text and review questions are predominantly unchanged from the first edition of this review, which was published in 1995. The book is miscast as a USMLE board review. It is more of a collection of stated facts, definitions, and descriptions rather than explanations of physiological cause and effect, which is a large component of USMLE type physiology questions. There is little clinical integration like that found in Step 1 and many of the review questions are simplistic, with occasional errors. Students are likely to use higher yield board exam prep tools rather than this book, although they may find it useful as a quick check and review of their basic understanding of facts in physiology.
 72ea393242
 
 
