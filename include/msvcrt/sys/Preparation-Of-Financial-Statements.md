## Preparation Of Financial Statements

 
 ![Preparation Of Financial Statements](https://hm.cpa/wp-content/uploads/FSreviewgraphic.png)
 
 
**Preparation Of Financial Statements ✦✦✦ [https://www.google.com/url?q=https%3A%2F%2Furloso.com%2F2tFkLi&sa=D&sntz=1&usg=AOvVaw1RpHG9CjPIK9zkUwl7xGIr](https://www.google.com/url?q=https%3A%2F%2Furloso.com%2F2tFkLi&sa=D&sntz=1&usg=AOvVaw1RpHG9CjPIK9zkUwl7xGIr)**

 
 
 
 
 
Are you aware of the option in the SSARS titled **Preparation of Financial Statements** (AR-C 70)? Many CPAs still believe the lowest level of service in the SSARS is a compilation, but this is not true. CPAs can and do issue financial statements without a compilation report. Today I provide an in-depth look at AR-70, Preparation of Financial Statements.
 
Are there other times when AR-C 70 is not applicable? Yes. The preparation guidance does not apply when the accountant is merely assisting in the preparation of financial statements; such services are considered bookkeeping.
 
A: No. The accountant is only required to perform the preparation engagement in accordance with section 70 of SSARS No. 21 **when engaged**to prepare financial statements. Therefore, because the accountant was not engaged to prepare the financial statements, there is no requirement to include a statement on each page of the financial statements indicating that no assurance is provided on the financial statements.
 
If the client does not request the preparation of financial statements and the accountant creates the statements as a byproduct of another service (e.g., tax return), he is not subject to the requirements of AR-C 70.
 
The accountant can prepare financial statements as directed by management or those charged with governance. The financials should be prepared using an acceptable reporting framework such as the following:
 
In preparing the financial statement, the accountant may need to assist management with judgements regarding amounts or disclosures. The accountant should discuss these judgments with management. Why? So management can understand and accept responsibility for the financial statements.
 
Is an engagement letter required for a preparation service? Yes. Moreover, the letter should be signed by the accountant or the firm and management or those charged with governance. A verbal understanding is not sufficient. Though AR-C 70 does not specify how often the engagement letter should be updated, it is best to do so annually.
 
As noted above, no compilation report will be issued for a preparation service. The preparation service is considered a nonattest, nonassurance service, and no compilation, review, or audit procedures are required.
 
The accompanying financial statements of XYZ Company as of and for the year ended December 31, 20XX, were not subjected to an audit, review, or compilation engagement by me (us) and I (we) do not express an opinion, a conclusion, nor provide any assurance on them.
 
Some accountants prefer to provide a disclaimer on letterhead. Why? Any third party reader can see that the accounting firm is involved in the preparation of the statements and that no assurance is provided.
 
If an accountant signs client checks and performs bookkeeping services, independence is not required. Moreover, if the accountant prepares financial statements for the same client, independence is not required. Signing checks, bookkeeping, and the preparation of financial statements are all nonattest services.
 
Suppose an accountant issues monthly financial statements for January through November with no compilation report (using the preparation option), but in December issues financial statements with a compilation report. Providing the monthly preparation services and the December compilation service triggers a requirement to consider independence.
 
Can the accountant omit all disclosures (notes to the financial statements) in a preparation engagement? Yes. Alternatively, the accountant can provide selected disclosures or if needed, full disclosure. In short, the accountant can do any of the following:
 
How should a departure from the applicable financial reporting framework be reported? Discuss the departure with management to see if it can be corrected. If it is not corrected, disclose the departure. How?
 
When a bank, credit union, regulatory or governmental agency, or other similar entity designs a prescribed form to meet its needs, there is a presumption that the required information is sufficient. What should be done if the prescribed form conflicts with the applicable basis of accounting? For example, what if the prescribed form requires all numbers to be in compliance with GAAP with the exception of receivables? Follow the form. In effect, the prescribed form is the reporting framework. Report departures from the prescribed form and its related instructions on the face of the financial statements (the form) or in a note.
 
Ralph, thank you for your comment. I am a long-time follower of your work, so I know your opinion is well worth consideration. The purpose of my post is not to promote its use but to educate those who might desire to use it. No doubt the preparation service can open the door to additional litigation risk. The compilation report does a better job of explaining what was done (or maybe I should say what was not done). Again, I appreciate your thoughts.
 
The late Eli Mason, a longtime leader and critic of the profession was quoted by the New York CPA Journal as cited below. I humbly concur with Mr. Mason. The preparation service unnecessarily exposes the CPA to risks that third parties will rely on the financial statements and the accountant will not have attached a compilation report containing protective language. Since a compilation involves no verification and no expression of assurance, it should have remained the lowest level of service that CPAs are permitted to perform.
 
The Guide is a financial statement preparers' manual tailored especially for preparers of financial statements for nonpublic companies. It takes you from the trial balance to the completed financial statements (including notes) in statement-by-statement, account-by-account sequence, regardless of whether the statements are audited, reviewed, compiled, or prepared by accountants in industry. You can use the information, including a disclosure checklist, to prepare financial statements and disclosures in accordance with generally accepted accounting principles or a special purpose framework.
 
When you encounter a new reporting issue, it is beneficial to see how other financial statement preparers may have reported the same or a similar issue. The Trends volume presents numerous sets of illustrative real-life financial statements and notes from a variety of industries using various reporting bases, referenced by an easy-to-use finding list and index to help you quickly find helpful illustrations.
 
Preparation of your financial statements is one of the last steps in the accounting cycle, using information from the previous statements to develop the current financial statement. Additionally, based on your needs, we can provide a financial statement analysis and file quarterly and year-end statements.
 
Statement on Standards for Accounting and Review Services (SSARS) No. 21, *Statements on Standards for Accounting and Review Services: Clarification and Recodification*, is the result of efforts by the AICPA Accounting and Review Services Committee (ARSC) to clarify and revise the standards for reviews, compilations, and engagements to prepare financial statements.
 
SSARS No. 21 supersedes all existing AR sections with the exception of AR Section 120,*Compilation of Pro Forma Financial Information*. Proposed standards regarding compilation of pro forma information and compilation of prospective financial information are expected to be exposed for public comment in 2015.
 
An accountant engaged to perform a review, a compilation, or an engagement to prepare financial statements is required to adhere to the requirements in Section 60 as well as the requirements in the appropriate engagement section.
 
The accountant may prepare financial statements that omit substantially all disclosures required by the applicable financial reporting framework. In such instances, the accountant is required to disclose such an omission in the financial statements. The disclosure of the omission of substantially all disclosures required by the applicable financial reporting framework may be made on the financial statements or in a selected note to the financial statements (see Exhibit 1).
 
The accountant is precluded from preparing financial statements that omit substantially all disclosures if the omission was undertaken with the intent of misleading users of such financial statements (see Exhibit 1).
 
If the accountant becomes aware that the financial statements include a departure or departures from the applicable financial reporting framework, he or she is required to either correct the departure or, after discussions with management, disclose the material misstatement or misstatements in the financial statements. The disclosure of the material misstatement or misstatements may be made on the face of the financial statements or in a note to the financial statements.
 
Section 80 retains the requirement that the accountant determine whether he or she is independent of the entity. The accountant can still perform a compilation engagement on financial statements that omit substantially all disclosures.
 72ea393242
 
 
