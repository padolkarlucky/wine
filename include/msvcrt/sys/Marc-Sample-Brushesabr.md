## Marc Sample Brushes.abr

 
  
 
**LINK ↔ [https://www.google.com/url?q=https%3A%2F%2Fgeags.com%2F2tDIb6&sa=D&sntz=1&usg=AOvVaw27YJ32BZzMRzjofXe6X7w-](https://www.google.com/url?q=https%3A%2F%2Fgeags.com%2F2tDIb6&sa=D&sntz=1&usg=AOvVaw27YJ32BZzMRzjofXe6X7w-)**

 
 
 
 
 
Alpha-diversity was calculated based on observed OTUs. Between 83 and 256 different species were observed per sample and Shannon index of the samples ranged from 3.45 to 7.06. However, non-parametric Kruskal-Wallis test revealed no significant differences of alpha diversity based on user age or period of use. To compare the different samples, principal component analysis (PCA) with weighted UniFrac distance analysis of the microbiomes was performed using ClustVis (Figure 2) [31]. Samples are coloured based on period of use and the different age is presented as shapes. The majority of the samples with short distances revealed strong variations of age and period of use. However, TB17 and TB19 clustered closely together and revealed a rather short period of use with two to four weeks and shorter than two weeks, respectively. Most of the samples TB3, TB4, TB8, TB12, TB13, TB14, TB15, and TB16 were used for a short period (
 
Figure 4 shows the microbiological contamination of toothbrushes in relation to the age of users and the period of use. Although the aerobic mesophilic TVC of toothbrushes of older users was much higher compared to younger users, the means did not differ significantly (p = 0.9). In contrast, the other media tested revealed only minor differences between the age groups (Figure 4b). The anaerobic mesophilic TVC was highest in samples of the age group between 20 and 60 years and the microbial count of Streptococcus spp. was higher in samples belonging to the group of >60 years.
 
The significant differences between different age groups might at least be partially related to changes of the oral microbiota with increasing age [42,43]. Xu et al. (2015) [44] showed that the abundance of Firmicutes was higher at younger age, which might explain that Lactobacillales dominated on toothbrushes of younger age groups. Furthermore, their study showed that the relative abundance of Actinobacteria increased steadily from the age 15 to 76 and this trend was observed for Micrococcales in the toothbrush samples as well. Clustering of samples based on weighted Unifrac revealed at least partially small distances between samples of the same user age/period of use, indicating that these microbiomes had many similarities. However, other factors might be responsible for the differences as well since samples without the same user age/period of use clustered closely together as well and thus further investigations are needed.
 
Space vector modulation is a PWM control algorithm for multi-phase AC generation, in which the reference signal is sampled regularly; after each sample, non-zero active switching vectors adjacent to the reference vector and one or more of the zero switching vectors are selected for the appropriate fraction of the sampling period in order to synthesize the reference signal as the average of the used vectors.
 
The process of PWM conversion is non-linear and it is generally supposed that low pass filter signal recovery is imperfect for PWM. The PWM sampling theorem[8] shows that PWM conversion can be perfect. The theorem states that "Any bandlimited baseband signal within ±0.637 can be represented by a pulsewidth modulation (PWM) waveform with unit amplitude. The number of pulses in the waveform is equal to the number of Nyquist samples and the peak constraint is independent of whether the waveform is two-level or three-level."
 
Historically, a crude form of PWM has been used to play back PCM digital sound on the PC speaker, which is driven by only two voltage levels, typically 0 V and 5 V. By carefully timing the duration of the pulses, and by relying on the speaker's physical filtering properties (limited frequency response, self-inductance, etc.) it was possible to obtain an approximate playback of mono PCM samples, although at a very low quality, and with greatly varying results between implementations.
 
**David REVOY** Author, 14 march 2018, 23:38 - Reply Hi Jun, the post contains all the information in a very user friendly way ; if you browse the comments, you'll find a way to also have additional informations. 
Installing a bundle is not hard. Download the file ; unzip it, then open Krita ; and load it in the resource manager. 
I hope you'll find a way to do it :)
 
**renaud** 07 july 2016, 11:50 - Reply Merci David
Dommage pour l'instabilité, on va gentillement attendre que des personnes compétentes se chargent de régler ça.
je ferais avec car la version 3 est quand même bien meilleure (du moins pour l'utilisation que j'en ai)
par contre, désolé, car je vois que tu t'es investi dans ta réponse, mais en gros j'ai presque rien compris au jargon technique...
faut dire que je suis un utilisateur fainéant de linux; si ça marche, tant mieux, si ça marche pas, je pleure.

Autre chose: suite à notre échange au sujet du hack de game pad, j'ai fait un démontage de clavier et de la soudure et je me suis fait un mini clavier personnalisé.
dis moi si ça te tente, je peux faire un petit article et te le filer pour que tu le mettes à dispo sur ton site.

à tantôt
 
**Anista alpharo** 12 december 2016, 09:50 - Reply Thank you so much 
I'm trying to make a dry pastel brushe like in Corel Painter can you help me 

some Corel Painter samples (note mine)

 

 
**Michael Ivarsson** 01 march 2017, 22:06 - Reply not having any luck with the brush bundle installation..
Let us get this straight, is the deevad-v8-1.bundle the only file needed right?
and where is the info on where to save or extract the bundle, where does it need to be...somewhere down the line in the answers I could see some preference user folder, I do not see such folder in the krita program folder, neither in my user folders.

It looks interesting, I managed to download the zip extract the bundle file withing, but I just extracted to desktop and then located the bundle from there and installed through the manager, I can see the deevad 8.1 bundle name when checking the preset list, but there is no brushes in there.
 
**Michael Ivarsson** 01 march 2017, 22:14 - Reply Never mind, it worked out, I copied it to the krita / share/ krita folder
didn´t show up instantly, had to switch to other presets then back again and suddenly the preset brushes was there
 
**David REVOY** Author, 02 march 2017, 10:08 - Reply Ha good you figured how to make it work this way. 
The normal way is to get the \*.bundle file on your computer, open Krita, and in the "resources manager" import the file.
 
**David Revoy** Author, 08 march 2017, 17:32 - Reply Blending needs a particular attention to the direction of the stroke and preserving the edges. The stroke must follow your volumes. Here is a quick demo:

 -03-08\_blending-demo.gif

The border of this brush could be harder ( increase : Fade > Horyzontal and Vertical ) in the brush editor.
 
**David Revoy** Author, 09 march 2017, 00:36 - Reply Yes, cross hatching gently in a direction and on another angle helps to smooth.
The direction are selected to follow the plane of the surface.
Training with this type of shape ( box ) helps.
 
**ArtfulButterfly** 08 march 2017, 20:28 - Reply Thank you so much sir for these brushes!!! You are the most generous artist ever! And my favorite too! I would never have gotten back to painting if i hadn't seen your videos and website!
 
**-L0Lock-** 09 march 2017, 10:24 - Reply Coucou !
Merci pour cette nouvelle version, je vais bien m'amuser cet après-midi.
Par contre, je ne sais pas si c'est lié, mais Krita à tendance à freeze dès que je finis un coup de brush à la tablette. Je suis obligé de mettre mon stylet sur les dockers pour dégeler le canvas et dessiner à nouveau.... Parfois ça me le fait à chaque fois que je lève le stylet du canvas. :'(
 
**David REVOY** Author, 11 march 2017, 14:27 - Reply Hi Ugo, 
That's a good idea, I'll consider it for future version ( as I did for the mixbrush now merged in the main pack ) Thanks!
 
**Carro** 15 march 2017, 23:50 - Reply Hi! 
I have a question, I'm using Krita 3.1.1 and I tried to download this but when I went to download bundle I couldn't find the package in my downloaded files where I put it. I wonder if there is maybe a newer version that I have to update to, and if so, will my old unfinished art work not be able to open in the new version? I don't know if this made any sense, but it would be really nice if you could answer this. Thank you so much! And your art is really beautiful, I appreciate what you do.
 
**David REVOY** Author, 16 march 2017, 08:22 - Reply Hi Carro,

Don't worry: my brushes version 8.2 are compatible with every Krita 3.x versions. They can't broke anything. If your Krita works well for 3.1.1 on your system; just paint with it and do not update to 3.1.2 . The next version 3.1.3 can be more interesting because it will be a sort of Krita 3 final. A big "bug fix release" before Krita jumping to the big change of Krita 4 ( with new text tool, SVG ).
The team of Krita put a lot of efforts into getting compatibility across version for the Krita files format ( \*.kra ). Your old unfinished art should be safe! :-)
 
**Glenn** 19 march 2017, 15:49 - Reply Great addition to the deevad-v8 brushes, HOWEVER, both in deevad-v8 and in v8-2 your very first "brush" the Eraser Thin does not erase. It works like a brush and puts down paint UNLESS the user hits the E key to turn it into an eraser (just like all other brushes can be turned into erasers).
 
**David REVOY** Author, 19 march 2017, 16:06 - Reply Thanks.
I loaded an empty configuration vanilla Krita 3.1.2, I added the bundle 8.2 and all works correctly: I can't reproduce.
What version of Krita do you use and on what s