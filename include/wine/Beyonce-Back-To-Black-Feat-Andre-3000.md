## Beyonce – Back To Black Feat Andre 3000

 
  
 
**## Files available for download:
[Link 1](https://tlniurl.com/2tFqf6)

[Link 2](https://urlin.us/2tFqf6)

[Link 3](https://cinurl.com/2tFqf6)

**

 
 
 
 
 
"Back to Black" received universal acclaim by music critics, who generally praised its throwback sound to girl groups from the 1960s. It was included on several compiled year and decade-end lists of the best in music and was further considered to be one of Winehouse's signature songs. The single peaked at number eight on the UK Singles Chart in the United Kingdom and is Winehouse's third best-selling single in that country. Many artists recorded covers of the song; most notably, Beyoncé and André 3000 covered it for the soundtrack of the 2013 film adaptation of the novel *The Great Gatsby* (1925).
 
The music video was directed by Phil Griffin and features a funeral procession in which Winehouse mourns over a grave that reads "R.I.P. the Heart of Amy Winehouse". The shot of the headstone was edited out after the singer's death in 2011. The video was primarily shot near Gibson Gardens and Chesholm Road in Stoke Newington, London. The graveyard scenes were filmed at Abney Park Cemetery in northeast London. According to the official Winehouse website, "Amy's sultry new video for Back in Black [*sic*] is both beautifully and artistically shot in black-and-white and compares in imagery a doomed love affair with that of a funeral."[23] At the 2007 Music of Black Origin Awards (MOBO), the music video was nominated for Best Video but lost to Kanye West's "Stronger" (2007).[24][25] Myers of the Official Charts Company deemed the clip "super-sad" and felt it went further on the song's main theme of goodbye.[6] As of December 2022, the video has over 850 million views on YouTube.[26]
 
Beyoncé and André 3000's cover is slower than the original with several lyrical modifications. It features a darker production with heavier instrumentation complete with a guitar and synthesizers, chopped and screwed elements and electronic beats. Upon its release, the song received mixed reviews from music critics who noted that the original version was already perfect to be further reworked; several critics praised its reworked arrangement while others criticized the singers' vocal performance, calling it the most controversial song on the soundtrack.
 
On 4 April 2013 a new trailer for the film featured previews of three songs from the soundtrack; a thirty-second preview of the cover of "Back to Black" was among those songs.[97][98] Chris Payne of *Billboard* magazine praised the cover, which according to him was made unique with the downtempo and EDM wobble.[99] A ninety-second snippet of the song also appeared online on 21 April 2013[100][101] and was made available for streaming through the iTunes Store.[102] A writer of *Rap-Up* magazine described it as a "dark and haunting collaboration" adding that it features "[André 3000] rapping his verse, while Queen B[eyoncé] burns slow with her seductive vocals."[101] Sam Lansky of the website Idolator wrote that the cover was "a fairly sinister, gloomy affair" because of the lyrics, which according to him were connected with Amy Winehouse's death, and the film's noirish bent.[103] He further commented that "it strikes us as an unusually dark side of King Bey[oncé], who's supposed to be literally the most poised human being alive".[103] Speaking about the modification of the lyrics, he noted that it was "but a little odd" and concluded the review by saying, "It's eerie but cool, and a fitting tribute to Winehouse's legacy."[103] The full version of the song premiered on Mark Ronson's East Village Radio show on 26 April 2013.[104] Upon its release, Ronson, who co-wrote the original song, commented that it was a "wonderful take on our song" and added: "I'm flattered and honoured, I know Amy would be too."[104]
 
Upon its release, the collaboration of "Back to Black" received mixed reviews by music critics. Critics discussed that the song was already "perfect" to be further reworked.[113] A writer of the website Consequence of Sound commented that the cover was "an intriguing take on the sultry number, featuring a soft pulse of dub underneath the syrupy sing-talk of André and breathy croon a la Marilyn Monroe/Betty Boop from Queen B."[114] Paula Mejía of the same website listed the song as a highlight on the album, adding that the singers "take an unsettling postmortem stab": "Oozing womps from the latter [Beyoncé] trickle into the ear slowly, mimicking that feeling when you're walking alone at night and have the suspicion you're being followed, but you're too freaked-out and cool to look behind you."[115] Christina Lee of the website Idolator wrote in her review of the song that it was "a gloomier take on Southern hip-hop's codeine effect", comparing it with OutKast's album *Aquemini* (1998) reinterpreted for contemporary times.[116] Charley Rogulewski of *Vibe* magazine commented that the cover was "a drugged-out slow burner in comparison to the doo-wop original, which boasted Winehouse's robust vocals".[117] C. Vernon Coleman of *XXL* magazine simply described it as "dope".[118] Describing it as a shift in Beyoncé's usual approach, Ann Powers of NPR noted that "[she] gets stuck in the 1950s, sounding far more like a torch singer than a blues queen" but added that it was not a problem since "Lurhmann's [*sic*] postmodernism has plenty of room for the juxtaposition of historic and current styles."[119] She further added that André 3000 sings in a "dandyish snarl that's been off-putting to some, but his radical rearrangement of the song comes closer than anything else here to Lurhmann's [*sic*] own nearly surrealist aesthetic".[119] Describing the cover as "distinguishable", Logan Smithson of *PopMatters* further commented, "Though the cover doesn't top Winehouse's original, it is good in its own right and feels right at home on *Gatsby*. Besides, who doesn't love hearing André 3000's voice?".[109] Kelly Dearmore from the *American Songwriter* described the cover as "synthy [and] trippy", writing that the singers turn it into a "wondrously hypnotic effect, and in turn, setting the tone for the entire album by blending brash audacity with dark surrealism".[120]
 
This is so hot to me just not long enough. Beyonce voice is so so seductive and sultry on this. Yess Beyonce I love the spin you put on this. Nothing will ever sound as good as the original back to black, but this here is a good cover. It fit the movies so well. I take it these kids no nothing about The Great Gatsby.
 
De volta ao luto
Eu não perdi tempo em me arrependerFiquei na meia bombaCom a mesma velha apostaVocê com sua cabeça erguidaE suas lágrimas que já secaramSegue em frente sem seu caraE eu, eu, eu voltei ao que já conheciaTão distanteDe tudo que passamos juntosE você e você, você, você andou por uma trilha perigosaSuas chances estão sempre contraVocê voltará ao luto
Nós apenas dissemos adeus com palavrasEu morri umas cem vezesEu volta pra elaE você volta aoVocê volta ao
Eu, eu te amo tantoNão é suficienteEu amo tragar, você ama cheirarE a vida, é como um canoE eu sou uma minúscula moeda rolandoPor dentro dessas paredes
Nós apenas dissemos adeus com palavrasEu morri umas cem vezesVocê volta para ela e eu volto paraNós apenas dissemos adeus com palavrasEu morri umas cem vezesVocê volta para ela e eu volto paraEu volto para eleBack To Black (Feat. André 3000)
I, I left no time to regretKept my dick wetWith that same old betYou and your head highAnd your tears dryGet on without your guyAnd I, I, I went back to a knewSo far removedFrom all that we went throughAnd you and you you, you tread a troubled trackYour odds are always stackedYou'll go back to black
We only said goodbye with wordsI died a hundred timesI go back to herAnd you go back toYou go back to
I, I loved you muchIt's not enoughI love blow and you love puffAnd life, is like a pipeAnd I'm a tiny penny rollin'Up the walls inside
We only said goodbye with wordsI died a hundred timesYou go back to her and I go back toWe only said goodbye with wordsI died a hundred timesYou go back to her and I go back toI go back to himEncontrou algum erro na letra? Por favor, envie uma correção >Compartilhe
esta músicaComentar ÚLTIMASVer todas- Taylor Lautner revela arrependimento por não ter defendido Taylor Swift no VMA 2009
 
I left no time to regret  
Kept my dick wet with his same old safe bet  
You and your head high  
And your tears dry, get on without your guy  
I went back to what I knew  
So far removed from all that we went through  
And you tread a troubled track  
Your odds are stacked, you'll go back to black
 
We only said goodbye with words  
I died a hundred times  
You go back to her  
And I go back to  
We only said goodbye with words  
I died a hundred times  
You go back to her  
And I go back to  
I go back to black
 
#TaylorSwiftIsOverParty? As if. With the release of the "Look What You Made Me Do" video, Swift officially entered her reputation era and shifted her skewed public perception off its tilted axis and back in her favor. Yes, there were snakes serving tea, bathtubs filled with diamonds, and a Taylor or two for every era that had come before. But the true feat of the glossy, karma-fueled visual was reminding the superstar's fans, doubters and haters alike that her ability to come back from the proverbial dead with a smash single in hand will always be stronger than anything thrown at her.
 72ea393242
 
 
