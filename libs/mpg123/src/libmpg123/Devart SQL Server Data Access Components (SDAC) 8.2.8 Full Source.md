## Devart SQL Server Data Access Components (SDAC) 8.2.8 Full Source

 
  
 
**Download &gt;&gt;&gt;&gt;&gt; [https://tinurli.com/2tDU2B](https://tinurli.com/2tDU2B)**

 
 
 
 
 
*Support for more MS SQL server functionality:*

- MS SQL Server Compact Edition supported
- Multiple Active Result Sets (MARS) supported
- Support for new data types, including XML, varchar(MAX), nvarchar(MAX), varbinary(MAX) added
- Support for a new level of transaction isolation added
- Support for more server objects in TMSMetaData added
- Stored procedure parameters with default values supported

*Extensions and improvements to existing functionality:*
- General performance improved
- Master/detail functionality extensions:
    - Local master/detail relationship support added
    - Master/detail relationship in CachedUpdates mode support added
- Working with calculated and lookup fields improvements:
    - Local sorting and filtering added
    - Record location speed increased
    - Improved working with lookup fields
- Greatly increased performance of applying updates in CachedUpdates mode
- Connection pool functionality improvements:
    - Efficiency significantly improved
    - API for draining the connection pool added
- Ability to customize update commands by attaching external components to TMSUpdateSQL objects added
- Support for DefaultValues on record insertion added
- Some performance improvements achieved:
    - NUMERIC fields fetching
    - Update commands execution while editing data set
    - DataSet refreshing
    - Records refreshing after updates and inserts
- Support for selecting database name in TMSConnectDialog added

*Usability improvements:*
- Syntax highlighting in design-time editors added
- Demo projects became better organized and clearer
- FAQ added

3.80.0.34 05-Dec-06
- Fixed bug with TraceFlags in DASQLMonitor (D16224)
- Fixed bug with TDAParam.AsDate property (D16213)
- Fixed bug with wrong value of the StatementTypes variable in the BeforeUpdateExecute event
- Bug with AV failure when modifying VirtualTable data in design time fixed

3.80.0.33 31-Oct-06
- Bug with ApplyUpdates and OnUpdateError in SDAC Trial fixed
- Bug with comparing Unicode strings fixed
- Bug with BeforeDisconnect event lost in design time fixed
- Bug with debug mode in design time fixed
- Bug with RefreshRecord in CachedUpdates mode fixed
- Bug with mouse wheeling in FilterBar and Search bar of CRDBGrid fixed (15675)

3.80.0.32 29-Sep-06
- Limited support added for Windows Vista Beta 2
- Memory usage optimized for tables with many fields
- Fixed bug with SQLNCLI provider and server cursors
- Fixed bug with unquoted '@' symbol in connection string
- Design-time bug with IDE version in TMSConnectionEditor in Delphi 2005 fixed
- Fixed bug with macros and "&" operand

3.80.0.31 28-Aug-06
- Support for Professional editions of Turbo Delphi, Turbo Delphi for .NET, Turbo C++ added
- Bug with automatic detail field value generation fixed (D12886)
- Fixed bug with TrimVarChar
- Fixed bug with FindKey method when the primary key has field of type Int64 (D12558)
- Added capability to use quoted field names in IndexFieldNames property (D10091)
- Bug with canceling edit on record with BLOB fields fixed
- Bug with WideString lookup fields size fixed
- Bug with loading wide strings in TVirtualTable fixed
- Added support for macros names in which first symbol is digit
- Bugs with parsing macros fixed
- Bug with filtering DateTime fields when filter expression format differs from one in local system fixed (D12823)

3.70.3.30 07-Jul-06
- Fixed bug with wrong line number in DAScript.EndLine property (M12015)
- Fixed bug with filtering empty strings (S12329)
- Bug with using calculated and lookup fields in Master/Detail relationship fixed
- Fixed bug with design-time editor for Detail dataset in Master/Detail relationship (M12021, M11914)
- Bug with modifying FieldDefs in TVirtualTable fixed
- Performance of SaveToFile and LoadFromFile functions in TVirtualTable improved (D12435)
- Bug in TVirtualTable Editor fixed

3.70.3.29 31-May-06
- New MS SQL Server 2005 data types supported
- TMSConnection.Options.InitialFileName property added
- Modifying FieldDefs in TVirtualTable component accelerated
- Fixed bug with memory leak in TCustomDADataSet (11636)
- Bug with calling Parameter Editor from Object Inspector fixed (11671)
- Bug with Master-Detail linked by parameters fixed

3.70.2.28 18-May-06
- SDAC is now compatible with InterBase Data Access Components
- Bug with Prepare method in Delphi 5 fixed

3.70.1.27 12-May-06
- Fixed bug with loosing connection for FetchAll=False datasets (M11398)
- Added support for MIDAS TDataSet.PSExecuteStatement ResultSet parameter (M11351)
- Fixed bug with AV failure in ApplyUpdates after CommitUpdates
- Bug with local filter under CLR fixed (11291)
- Bug with using alternative memory managers fixed

3.70.1.26 14-Apr-06
- Fixed bug with positioning in grid after TCustomDADataSet.Locate (11256)
- Fixed bug with Master/Detail relations and local sorting and filtering (11254, 11255)
- TCustomDADataSet.Locate now centers position in DBGrid (10976)
- Bug with displaying query objects in objects tree in DBMonitor fixed
- Optimized using of system resources when opening query (O11270)
- Fixed bug with design-time SQL Generator
- Added OLE DB limitation workaround when preparing query with parameters in subquery

3.70.1.25 10-Mar-06
- Ability of trimming (N)VARCHAR fields with CustomDADataSet.Options.TrimVarChar property added
- Added support for MIDAS master-detail relations
- Fixed bug with fetching (N)VARCHAR(4000) fields
- Fixed bug with erroneous exception on TDAParam.LoadFromStream
- Bug with AV failure when calling CommitUpdates method of inactive dataset fixed
- Bug with error failure when assigning empty dataset to VirtualTable under CLR fixed

3.70.0.24 26-Jan-06
- Support for Delphi 2006 added
- Support for MS SQL Server 2005 added
- FastReport 3.20 support added
- Fixed bug with single MacroChar in string constants inside SQL
- Bug with invalid processing of SQL statements when using SQL keywords as macros name fixed
- Fixed MS SQL server bug with processing 'SET STATISTICS IO ON' statements
- Fixed bug with using CheckRowVersion property and RefreshRecord method

3.55.2.22 19-Dec-05
- Fixed bug with trimming VARCHAR fields
- Bug with restoring BaseSQL property fixed

3.55.2.21 07-Dec-05
- TMSConnection.Options.PersistSecurityInfo property added
- TCustomDADataSet.FindKey, TCustomDADataSet.FindNearest methods added for BDE compatibility
- Fixed bug with Locate by multiple fields
- Fixed bug with unchanging RecordCount value after CancelUpdates
- Bug with refreshing record after changing SQL property fixed
- Fixed SetWhere function bug
- Fixed bug with server cursors when RefreshOptions property contains roBeforeEdit flag

3.55.1.20 24-Oct-05
- Fixed bug with open detail TMSTable when master is not opened
- Fixed bug with design-time SQL Generator
- Fixed bug with EOLEDBError.MessageWide
- Bug with updating FilterSQL fixed
- Bug with VirtualTable registration in CBuilder fixed
- Bug with VtReg.pas in Pro version fixed
- Bug with closing dataset (SQL contains macros) when the InsertSQL property changes fixed
- Bug with incorrect sequence of calling BeforeClose, BeforeDisconnect events fixed

3.55.1.19 11-Oct-05
- Unicode error messages support added with EOLEDBError.MessageWide
- SQL Generator improved: now in design-time format is used instead of
- Fixed some bugs in design-time SQL Generator
- Fixed bug with Master/Detail relations on string keys that differ in case only
- Bug with reexecution after SQL statement changed fixed
- Bug with assertion failure with OnNewRecord fixed
- Fixed bug with detail refresh when field referenced by MasterFields property is empty
- Fixed bug with extra detail record posts after master record changed
- Bug with firing AfterScroll Event after detail dataset refresh fixed
- Bug with simultaneous use of several DAC products fixed
- Bug with changing FilterSQL of inactive dataset fixed
- Bug with column sizing with CRDBGrid.OptionsEx.dgeStretch=True at design-time fixed
- Bug with TCRColumn.SummaryMode=smLabel for string and date fields fixed

3.55.1.18 02-Sep-05
- Deferred detail dataset refresh feature with TCustomDADataSet.Options.DetailDelay property added
- Fixed bug with locating, filtering and local sorting of strings that contain '--'
- Fixed DisposeBuf bug

3.55.0.17 02-Aug-05
- Optimized macros processing
- Fixed bug with using keywords as param name
- Fixed bug with local sorting in dsEdit state
- Fixed bug with getting Null for Blob fields
- Improved behavior on parsing inline comments

3.55.0.16 05-Jul-05
- Fixed performance bug with calling Execute (ReadOnly = True)
- Fixed bug with sorting by GUID fields
- Bug with TDAParam.AsString with TDataTime parameters fixed
- Fixed bug with TMSConnection.ConnectioString design time editor

3.55.0.15 30-May-05
- Ability of automatic preparing query with TCustomDADataSet.Options.AutoPrepare property added
- Ability to synchronize position at different DataSets with TCustomDADataSet.GotoCurrent method added
- Optimized MSSQLMonitor BLOB parameters processing
- Improved behavior on editing master key on Master/Detail relation
- Removed TMSStoredProc.ParamCheck property from design-time
- Fixed bug with ignoring MSConnection.Options.WorkstationID
- Fixed bug with DADataSet.CheckBookmark on filtered datasets
- Fixed bug with Transactions on connection lost
- Fixed bug with reconnect on connection lost

3.50.0.14 29-Apr-05
- Update Pack 3 is required for Delphi 8
- Fixed bug with FetchAll = False and QueryRecCount = True for Detail queries (6759)
- MSQuery.AddWhere improved

3.50.0.13 29-Mar-05
- Fixed bug with quoting field