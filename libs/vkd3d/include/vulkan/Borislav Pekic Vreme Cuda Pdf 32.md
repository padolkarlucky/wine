## Borislav Pekic Vreme Cuda Pdf 32

 
  
 
**## Links to get files:
[Link 1](https://tweeat.com/2tBciq)

[Link 2](https://jinyurl.com/2tBciq)

[Link 3](https://urlca.com/2tBciq)

**

 
 
 
 
 
# Borislav PekiÄ's Time of Miracles: A Novel of Biblical Paradoxes
 
Borislav PekiÄ was a Serbian writer and political activist who spent five years in prison for his anti-communist views. His first novel, *Time of Miracles*, published in 1965, was a daring and original reinterpretation of the biblical stories of Jesus' miracles in Judea. The novel consists of four stories, each focusing on a different miracle and its consequences for the recipient. However, instead of bringing joy and salvation, the miracles turn out to be curses that ruin the lives of those who experience them.
 
The first story, "The Miracle on Jabneel", tells the tale of a leprous woman who is healed by Jesus, but then rejected by her husband and society. The second story, "The Miracle at Bethany", narrates the resurrection of Lazarus, who becomes a living corpse tormented by his memories of death. The third story, "The Miracle at Siloam", depicts the restoration of sight to a blind man, who is disgusted by the world he sees and blinds himself again. The fourth story, "The Miracle at Gerasa", portrays the exorcism of a demoniac, who is crucified by his fellow villagers for speaking the truth.
 
PekiÄ's novel is a brilliant and provocative exploration of the paradoxes and contradictions of faith, dogma and ideology. He challenges the traditional interpretations of the biblical texts and exposes the dark side of human nature and history. He also creates a vivid and poetic language that blends biblical archaisms with modern slang and humor. *Time of Miracles* is a masterpiece of Serbian literature and a timeless work of art that transcends its historical context.
  
The novel *Time of Miracles* can be seen as a critique of the biblical mythology and its implications for human history and destiny. PekiÄ challenges the conventional understanding of the miracles as signs of God's grace and power, and reveals them as sources of suffering and alienation. He also exposes the ideological conflicts and violence that underlie the biblical narratives, such as the clash between the Jewish and Roman authorities, the zealotry of the Pharisees and the Essenes, and the betrayal of Judas. PekiÄ does not offer a simple or straightforward interpretation of the biblical texts, but rather creates a complex and ambiguous interplay of perspectives, voices and genres. He mixes historical realism with fantasy, satire with tragedy, comedy with horror, and irony with empathy.
 
One of the main features of PekiÄ's novel is his use of parody as a literary device. Parody is a form of imitation that exaggerates or distorts the original work in order to mock or criticize it. PekiÄ parodies the biblical texts by changing their tone, style, context and meaning. He also parodies the sacred rituals and figures by presenting them as absurd, grotesque or ridiculous. For example, he depicts Jesus as a clumsy magician who performs his miracles with difficulty and uncertainty, often causing more harm than good. He also portrays the apostles as ignorant, cowardly or selfish followers who misunderstand or misuse Jesus' teachings. He also mocks the religious fanaticism and hypocrisy of some of the Jewish sects, such as the Essenes who practice self-mutilation and cannibalism.
 
However, PekiÄ's parody is not only a form of mockery or criticism, but also a form of homage and admiration. He shows a deep knowledge and respect for the biblical texts and their cultural and historical significance. He also expresses a genuine sympathy and compassion for the human condition and its struggles with faith, doubt, sin, guilt, love, death and hope. He does not deny or reject the possibility of miracles or transcendence, but rather questions their meaning and purpose in a world marked by suffering and injustice. He also explores the ethical and existential dilemmas that arise from the encounter with the miraculous and the divine. He does not offer easy answers or solutions, but rather invites the reader to reflect and question along with him.
 842aa4382a
 
 
