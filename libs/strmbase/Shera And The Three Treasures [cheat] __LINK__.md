## Shera And The Three Treasures [cheat]

 
  
 
**Download File ->->->-> [https://shurll.com/2tAQiO](https://shurll.com/2tAQiO)**

 
 
 
 
 
# Shera and the Three Treasures: How to Unlock All Endings and Achievements
 
Shera and the Three Treasures is a fantasy RPG game developed by Asakiyumemishi and published by Kagura Games. The game follows the adventures of Shera, a young girl who inherits the title of Demon Lord and must gather the three treasures of the continent to stop a war. Along the way, she meets various characters who can join her party or become her enemies.
 
The game has multiple endings and achievements that depend on the choices you make throughout the story. In this article, we will show you how to unlock all of them and get the most out of your gameplay experience.
 
## How to Unlock All Endings
 
The game has four main endings: True Ending, Good Ending, Bad Ending, and Normal Ending. Each ending has different requirements and consequences for Shera and her companions. Here is how to get each one:
 
- **True Ending:** This is the best ending for Shera and her friends. To get this ending, you need to collect all three treasures (the Sword of Light, the Shield of Darkness, and the Crown of Wisdom) and defeat the final boss with Shera's party. You also need to have a high affinity with all of your party members, which means you need to talk to them often, give them gifts, and avoid hurting them in battle. You can check your affinity level with each character by pressing F1 in the menu. The True Ending will unlock a bonus epilogue scene and an extra dungeon.
- **Good Ending:** This is a happy ending for Shera and her friends, but not as good as the True Ending. To get this ending, you need to collect all three treasures and defeat the final boss with Shera's party, but you don't need to have a high affinity with all of your party members. You can have a low or medium affinity with some of them, as long as you don't betray or abandon them. The Good Ending will unlock a bonus epilogue scene.
- **Bad Ending:** This is a tragic ending for Shera and her friends. To get this ending, you need to collect all three treasures and defeat the final boss with Shera's party, but you need to have a low affinity with all of your party members. You can achieve this by ignoring them, hurting them in battle, or choosing negative dialogue options. The Bad Ending will show you how Shera becomes corrupted by her power and loses everything she cares about.
- **Normal Ending:** This is a neutral ending for Shera and her friends. To get this ending, you need to collect all three treasures but fail to defeat the final boss with Shera's party. You can do this by losing the final battle on purpose or by choosing to escape from it. The Normal Ending will show you how Shera gives up on her quest and leaves the continent with her friends.

## How to Unlock All Achievements
 
The game has 18 achievements that you can unlock by completing various tasks or events in the game. Some of them are related to the endings, while others are hidden or optional. Here is a list of all the achievements and how to get them:

- **Demon Lord:** This achievement is unlocked automatically when you start a new game.
- **Sword of Light:** This achievement is unlocked when you obtain the Sword of Light from the Temple of Light in Luminia.
- **Shield of Darkness:** This achievement is unlocked when you obtain the Shield of Darkness from the Temple of Darkness in Nocturnia.
- **Crown of Wisdom:** This achievement is unlocked when you obtain the Crown of Wisdom from the Temple of Wisdom in Arcadia.
- **True Ending:** This achievement is unlocked when you get the True Ending as explained above.
- **Good Ending:** This achievement is unlocked when you get the Good Ending as explained above.
- **Bad Ending:** This achievement is unlocked when you get the Bad Ending as explained above.
- **Normal Ending:** This achievement is unlocked when you get the Normal Ending as explained above.
- **Loyal Friend:** This achievement is unlocked when you have a 842aa4382a




