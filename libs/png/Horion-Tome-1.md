## Horion Tome 1

 
 ![Horion Tome 1](https://m.media-amazon.com/images/I/51XhfjB7ksL._SX317_BO1,204,203,200_.jpg)
 
 
**## Links to get files:
[Link 1](https://tweeat.com/2tEBop)

[Link 2](https://jinyurl.com/2tEBop)

[Link 3](https://urlca.com/2tEBop)

**

 
 
 
 
 
La narration est propre et nous présente l'univers de manière fluide sans se trainer ni se précipiter et les dialogues ainsi que les personnages sont très bien écrits chacun à leur manière (Nyrrki est priceless). Le chapitre 7 est définitivement mon préféré : l'idée, le dialogue et les dessins sont superbes.

En parlant des dessins, ils vont de très corrects à "ah oui quand même" (le chapitre 3 en est le parfait exemple). J'ai bien aimé le côté authentique qui nous fait rappeler qu'Enaibi est Française et non Japonaise avec tout ce que cela implique, que ce soit à travers les expressions ou les détails du décor; ce n'est pas un simple copier/coller du style. Cependant un reproche à faire tout de même : de meilleurs angles auraient pu être choisis lors des combats pour donner plus de clarté aux mouvements, car j'ai souvent perdu le fil entre les différentes cases et il m'a régulièrement fallu regarder à nouveau la case précédente pour mieux comprendre le déroulement de la scène.

Tout est là pour un premier tome, c'est du très bon. J'ai déjà envie de le relire mais je vais d'abord m'empresser de précommander le second.
 
Horion est un récit d'aventure dans la plus pure tradition. Le héros ressemble à s'y méprendre au héros de Dragon Ball Z. Le concept semble être de passer des épreuves avec pour devise : "pour changer de vie, ils sont prêts à la perdre !" Autre devise dans la même veine : "Ne rien tenter par peur d'échouer, c'est ne rien faire pour réussir." Bref, c'est toujours bien d'inciter les jeunes à dépasser leurs limites mais jusqu'où ?

Après une entrée en matière assez fracassante, le soufflé retombe assez vite dans des bavardages un peu inutiles avant la reprise de l'action. Les choses avancent petit à petit. Je dois dire que j'ai perdu patience en cours de route. C'est un univers plutôt riche mais qu'il faut découvrir au fil des tomes. Moi, je suis plutôt un lecteur exigeant qu'il faut épater tout de suite.

Le dessin est soigné mais l'histoire ne m'a pas captivé dès le départ ainsi que les dialogues d'une lourdeur implacable. A noter qu'il s'agit d'un manga typiquement français mais tout en respectant les codes japonais.
 
Un très bon premier tome, il lance une aventure parsemée de nombreux personnages charismatiques, dictant des phrases intéressantes. Les dessins sont très bons avec un chara design assez original! (Notamment le personnage de Nyrrki, d'un charisme impressionnant).
 
Un très bon premier tome qui nous plonge directement dans le bain sans nous prendre par la main comme le fond énormément d'autres mangas au début. C'est assez déroutant au début mais on s'y fait et à partir de ce moment là on rentre pleinement dans le manga et on l'apprécie énormément.
 
***Horion*** est une série de mangas français d'aventure fantastique[1]. Elle est scénarisée par Aienkei[2] et dessinée par Enaibi[3]. Le premier tome est paru le 2 mai 2018 aux éditions Glénat.
 
La série est officiellement annoncée chez Glénat à l'été 2016[7]. Sa sortie était prévue pour l'année 2017 mais le premier tome sort finalement le 2 mai 2018[8],[9]. Les deux premiers volumes sont sortis à un mois d'intervalle[10]. Le troisième tome sort en juin 2019, soit une année après la sortie du 1er tome[11], et le quatrième tome, en février 2022. La version italienne éditée par Manga Senpai est publié depuis octobre 2019[12] et la version espagnole depuis 2020.
 
Horion a des allures de shonen classique, un peu violent et mature, avec des intrigues politiques a peine naissantes mais bien posées dans le premier tome. S'il fallait trouver un équivalent, on pourrait dire que ça se rapproche de *Full Metal Alchemist*. Le héros et son éternel pote ne sont pas encore rivaux/ennemis, mais on sent bien qu'il peut se passer encore beaucoup, beaucoup de choses dans cet univers fantastique qui traîne sa part d'obscurité, et le reste du casting est intéressant - certains perso sortant du lot, même lorsqu'ils semblent être tertiaires.
 
Un premier tome de 224 pages **qui pose habilement les bases d'une histoire que l'on espère longue et riche en péripéties et qui arrive à détourner habilement les codes en vigueur pour se les approprier et créer un univers totalement nouveau et fascinant à la fois**. Ici, nous sommes emportés dans un monde empreint de magie, de guerriers hors du commun et de complots politiques en germe, en suivant le périple du jeune Koza qui n'a qu'une idée en tête : retrouver son frère qui était parti rejoindre la cité interdite de Landgrave, en compagnie de son ami le prince héritier Valyu et de leur garde du corps Palk.
 
Il va sans dire que ce premier volume, en tant que tome introductif d'une saga, **atteint sans mal son objectif,** propose des pistes intéressantes et se révèle **très divertissant tout autant que prenant et drôle.** Là où on pouvait craindre qu'une oeuvre française soit trop prisonnière de ses modèles, Horion arrive à point nommé pour nous donner tort et **c'est avec un grand plaisir que l'on reconnait notre erreur.**
 72ea393242
 
 
